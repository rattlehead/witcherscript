package com.rattlehead.witcherscript.psi

import com.intellij.psi.PsiElement

interface Modifiable : PsiElement {
    val modifiers: WSModifiers
    val modifiersString: String
}
