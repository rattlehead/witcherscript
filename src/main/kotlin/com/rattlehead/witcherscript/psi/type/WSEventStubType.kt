package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSCallableIndex
import com.rattlehead.witcherscript.psi.WSEvent
import com.rattlehead.witcherscript.psi.impl.WSEventImpl
import com.rattlehead.witcherscript.psi.stub.WSEventStub

class WSEventStubType(
    typeName: String
) : AbstractStubType<WSEventStub, WSEvent>(typeName, ::WSEventImpl, WSCallableIndex) {
    override fun createStub(psi: WSEvent, parentStub: StubElement<out PsiElement>): WSEventStub {
        return WSEventStub(parentStub, psi.name, psi.isAbstract)
    }

    override fun serialize(stub: WSEventStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
        stream.writeBoolean(stub.isAbstract)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSEventStub {
        return WSEventStub(parentStub, stream.readNameString(), stream.readBoolean())
    }
}
