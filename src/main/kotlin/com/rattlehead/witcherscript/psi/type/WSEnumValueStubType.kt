package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSEnumValueIndex
import com.rattlehead.witcherscript.psi.WSEnumValue
import com.rattlehead.witcherscript.psi.impl.WSEnumValueImpl
import com.rattlehead.witcherscript.psi.stub.WSEnumValueStub

class WSEnumValueStubType(
    typeName: String
) : AbstractStubType<WSEnumValueStub, WSEnumValue>(typeName, ::WSEnumValueImpl, WSEnumValueIndex) {
    override fun createStub(psi: WSEnumValue, parentStub: StubElement<out PsiElement>): WSEnumValueStub {
        return WSEnumValueStub(parentStub, psi.name, psi.type.toString())
    }

    override fun serialize(stub: WSEnumValueStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
        stream.writeUTF(stub.typeString)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSEnumValueStub {
        return WSEnumValueStub(parentStub, stream.readNameString(), stream.readUTF())
    }
}
