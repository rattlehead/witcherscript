package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSConstructIndex
import com.rattlehead.witcherscript.psi.WSStruct
import com.rattlehead.witcherscript.psi.impl.WSStructImpl
import com.rattlehead.witcherscript.psi.stub.WSStructStub

class WSStructStubType(
    typeName: String
) : AbstractStubType<WSStructStub, WSStruct>(typeName, ::WSStructImpl, WSConstructIndex) {
    override fun createStub(psi: WSStruct, parentStub: StubElement<out PsiElement>): WSStructStub {
        return WSStructStub(parentStub, psi.name, psi.modifiersString)
    }

    override fun serialize(stub: WSStructStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
        stream.writeUTF(stub.modifiersString)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSStructStub {
        return WSStructStub(parentStub, stream.readNameString(), stream.readUTF())
    }
}
