package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.IStubElementType
import com.intellij.psi.stubs.IndexSink
import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.WSLanguage
import com.rattlehead.witcherscript.index.Index

abstract class AbstractStubType<S : StubElement<in T>, T : PsiElement>(
    private val typeName: String,
    private val psiConstructor: (S, IStubElementType<*, *>) -> T,
    private val index: Index<in S>
) : IStubElementType<S, T>(typeName, WSLanguage.INSTANCE) {
    final override fun createPsi(stub: S): T = psiConstructor.invoke(stub, this)

    final override fun getExternalId() = "${language.id}.${typeName}"

    final override fun indexStub(stub: S, indexSink: IndexSink) = index.indexStub(stub, indexSink)
}
