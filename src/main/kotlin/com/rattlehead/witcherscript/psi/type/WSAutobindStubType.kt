package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSPropertyIndex
import com.rattlehead.witcherscript.psi.WSAutobind
import com.rattlehead.witcherscript.psi.impl.WSAutobindImpl
import com.rattlehead.witcherscript.psi.stub.WSPropertyStub

class WSAutobindStubType(
    typeName: String
) : AbstractStubType<WSPropertyStub, WSAutobind>(typeName, ::WSAutobindImpl, WSPropertyIndex) {
    override fun createStub(psi: WSAutobind, parentStub: StubElement<out PsiElement>): WSPropertyStub {
        return WSPropertyStub(parentStub, this, psi.name, psi.modifiersString, psi.type.toString())
    }

    override fun serialize(stub: WSPropertyStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
        stream.writeUTF(stub.modifiersString)
        stream.writeUTF(stub.typeString)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSPropertyStub {
        return WSPropertyStub(parentStub, this, stream.readNameString(), stream.readUTF(), stream.readUTF())
    }
}