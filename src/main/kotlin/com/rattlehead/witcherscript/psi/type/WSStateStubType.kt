package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSConstructIndex
import com.rattlehead.witcherscript.psi.WSState
import com.rattlehead.witcherscript.psi.impl.WSStateImpl
import com.rattlehead.witcherscript.psi.stub.WSStateStub

class WSStateStubType(
    typeName: String
) : AbstractStubType<WSStateStub, WSState>(typeName, ::WSStateImpl, WSConstructIndex) {
    override fun createStub(psi: WSState, parentStub: StubElement<out PsiElement>): WSStateStub {
        return WSStateStub(parentStub, psi.name, psi.modifiersString, psi.parentName, psi.ownerName)
    }

    override fun serialize(stub: WSStateStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
        stream.writeUTF(stub.modifiersString)
        stream.writeName(stub.parentName)
        stream.writeName(stub.ownerName)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSStateStub {
        return WSStateStub(parentStub, stream.readNameString(), stream.readUTF(),
                stream.readNameString(), stream.readNameString())
    }
}
