package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSConstructIndex
import com.rattlehead.witcherscript.psi.WSClass
import com.rattlehead.witcherscript.psi.impl.WSClassImpl
import com.rattlehead.witcherscript.psi.stub.WSClassStub

class WSClassStubType(
    typeName: String
) : AbstractStubType<WSClassStub, WSClass>(typeName, ::WSClassImpl, WSConstructIndex) {
    override fun createStub(psi: WSClass, parentStub: StubElement<out PsiElement>): WSClassStub {
        return WSClassStub(parentStub, psi.name, psi.modifiersString, psi.parentName)
    }

    override fun serialize(stub: WSClassStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
        stream.writeUTF(stub.modifiersString)
        stream.writeName(stub.parentName)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSClassStub {
        return WSClassStub(parentStub, stream.readNameString(), stream.readUTF(), stream.readNameString())
    }
}
