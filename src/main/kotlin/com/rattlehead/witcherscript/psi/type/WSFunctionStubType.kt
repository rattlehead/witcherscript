package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSCallableIndex
import com.rattlehead.witcherscript.psi.WSFunction
import com.rattlehead.witcherscript.psi.impl.WSFunctionImpl
import com.rattlehead.witcherscript.psi.stub.WSFunctionStub

class WSFunctionStubType(
    typeName: String
) : AbstractStubType<WSFunctionStub, WSFunction>(typeName, ::WSFunctionImpl, WSCallableIndex) {
    override fun createStub(psi: WSFunction, parentStub: StubElement<out PsiElement>): WSFunctionStub {
        return WSFunctionStub(parentStub, this, psi.name, psi.isAbstract, psi.modifiersString, psi.type.toString())
    }

    override fun serialize(stub: WSFunctionStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
        stream.writeBoolean(stub.isAbstract)
        stream.writeUTF(stub.modifiersString)
        stream.writeUTF(stub.typeString)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSFunctionStub {
        return WSFunctionStub(parentStub, this, stream.readNameString(),
            stream.readBoolean(), stream.readUTF(), stream.readUTF())
    }
}
