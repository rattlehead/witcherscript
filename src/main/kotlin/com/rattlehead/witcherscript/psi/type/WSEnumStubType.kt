package com.rattlehead.witcherscript.psi.type

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement
import com.intellij.psi.stubs.StubInputStream
import com.intellij.psi.stubs.StubOutputStream
import com.rattlehead.witcherscript.index.WSConstructIndex
import com.rattlehead.witcherscript.psi.WSEnum
import com.rattlehead.witcherscript.psi.impl.WSEnumImpl
import com.rattlehead.witcherscript.psi.stub.WSEnumStub

class WSEnumStubType(
    typeName: String
) : AbstractStubType<WSEnumStub, WSEnum>(typeName, ::WSEnumImpl, WSConstructIndex) {
    override fun createStub(psi: WSEnum, parentStub: StubElement<out PsiElement>): WSEnumStub {
        return WSEnumStub(parentStub, psi.name)
    }

    override fun serialize(stub: WSEnumStub, stream: StubOutputStream) {
        stream.writeName(stub.name)
    }

    override fun deserialize(stream: StubInputStream, parentStub: StubElement<*>): WSEnumStub {
        return WSEnumStub(parentStub, stream.readNameString())
    }
}
