package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.type.WSStubTypes

class WSEnumStub(
    parent: StubElement<*>?,
    name: String?
) : WSConstructStub(parent, WSStubTypes.ENUM, name)
