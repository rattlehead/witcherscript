package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.StubBase
import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.WSEnumValue
import com.rattlehead.witcherscript.psi.type.WSStubTypes

class WSEnumValueStub(
    parent: StubElement<*>?,
    override val name: String?,
    override val typeString: String
) : StubBase<WSEnumValue?>(parent, WSStubTypes.ENUM_VALUE), NamedStub, TypedStub
