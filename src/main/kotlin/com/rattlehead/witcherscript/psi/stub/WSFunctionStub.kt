package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.IStubElementType
import com.intellij.psi.stubs.StubElement

class WSFunctionStub(
    parent: StubElement<*>?,
    elementType: IStubElementType<*, *>?,
    name: String?,
    hasBody: Boolean,
    override val modifiersString: String,
    override val typeString: String
) : WSCallableStub(parent, elementType, name, hasBody), ModifiableStub, TypedStub