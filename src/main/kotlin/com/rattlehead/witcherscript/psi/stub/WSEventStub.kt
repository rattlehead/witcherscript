package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.type.WSStubTypes

class WSEventStub(
    parent: StubElement<*>?,
    name: String?,
    hasBody: Boolean
) : WSCallableStub(parent, WSStubTypes.EVENT, name, hasBody)