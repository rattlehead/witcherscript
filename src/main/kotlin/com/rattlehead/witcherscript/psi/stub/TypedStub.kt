package com.rattlehead.witcherscript.psi.stub

interface TypedStub {
    val typeString: String
}