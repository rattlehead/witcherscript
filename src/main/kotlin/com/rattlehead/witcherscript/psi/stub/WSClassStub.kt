package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.type.WSStubTypes

class WSClassStub(
    parent: StubElement<*>?,
    name: String?,
    override val modifiersString: String,
    val parentName: String?
) : WSConstructStub(parent, WSStubTypes.CLASS, name), ModifiableStub
