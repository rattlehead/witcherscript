package com.rattlehead.witcherscript.psi.stub

interface ModifiableStub {
    val modifiersString: String
}