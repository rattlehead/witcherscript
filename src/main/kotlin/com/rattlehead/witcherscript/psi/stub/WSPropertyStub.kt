package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.IStubElementType
import com.intellij.psi.stubs.StubBase
import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.WSProperty

class WSPropertyStub(
    parent: StubElement<*>?,
    elementType: IStubElementType<*, *>?,
    override val name: String?,
    override val modifiersString: String,
    override val typeString: String
) : StubBase<WSProperty?>(parent, elementType), NamedStub, ModifiableStub, TypedStub
