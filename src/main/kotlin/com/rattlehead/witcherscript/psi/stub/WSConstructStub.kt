package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.IStubElementType
import com.intellij.psi.stubs.StubBase
import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.WSConstruct

abstract class WSConstructStub protected constructor(
    parent: StubElement<*>?,
    elementType: IStubElementType<*, *>?,
    override val name: String?
) : StubBase<WSConstruct?>(parent, elementType), NamedStub {
    open val id: String?
        get() = name
}
