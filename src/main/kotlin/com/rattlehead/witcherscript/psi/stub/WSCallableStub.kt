package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.IStubElementType
import com.intellij.psi.stubs.StubBase
import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.WSCallable

abstract class WSCallableStub(
    parent: StubElement<*>?,
    elementType: IStubElementType<*, *>?,
    override val name: String?,
    val isAbstract: Boolean
) : StubBase<WSCallable?>(parent, elementType), NamedStub
