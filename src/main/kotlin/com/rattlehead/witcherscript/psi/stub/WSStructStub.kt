package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.type.WSStubTypes

class WSStructStub(
    parent: StubElement<*>?,
    name: String?,
    override val modifiersString: String
) : WSConstructStub(parent, WSStubTypes.STRUCT, name), ModifiableStub
