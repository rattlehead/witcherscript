package com.rattlehead.witcherscript.psi.stub

import com.intellij.psi.stubs.StubElement
import com.rattlehead.witcherscript.psi.type.WSStubTypes

class WSStateStub(
    parent: StubElement<*>?,
    name: String?,
    override val modifiersString: String,
    val parentName: String?,
    val ownerName: String?
) : WSConstructStub(parent, WSStubTypes.STATE, name), ModifiableStub {
    override val id: String?
        get() {
            return "${ownerName ?: return null}State${name ?: return null}"
        }
}
