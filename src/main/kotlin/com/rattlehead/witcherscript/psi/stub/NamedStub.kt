package com.rattlehead.witcherscript.psi.stub

interface NamedStub {
    val name: String?
}