package com.rattlehead.witcherscript.psi

import com.intellij.psi.PsiElement
import com.rattlehead.witcherscript.common.Type

interface Typed : PsiElement {
    val type: Type?
}