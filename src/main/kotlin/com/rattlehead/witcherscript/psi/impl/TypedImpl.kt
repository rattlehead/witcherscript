@file:JvmMultifileClass @file:JvmName("WSPsiImplUtil")

package com.rattlehead.witcherscript.psi.impl

import com.intellij.psi.util.PsiTreeUtil
import com.rattlehead.witcherscript.common.ArrayType
import com.rattlehead.witcherscript.common.CustomType
import com.rattlehead.witcherscript.common.Type
import com.rattlehead.witcherscript.common.Type.Companion.BOOL
import com.rattlehead.witcherscript.common.Type.Companion.FLOAT
import com.rattlehead.witcherscript.common.Type.Companion.INT
import com.rattlehead.witcherscript.common.Type.Companion.NAME
import com.rattlehead.witcherscript.common.Type.Companion.NULL
import com.rattlehead.witcherscript.common.Type.Companion.STRING
import com.rattlehead.witcherscript.common.Type.Companion.VOID
import com.rattlehead.witcherscript.psi.*
import com.rattlehead.witcherscript.psi.stub.WSFunctionStub
import com.rattlehead.witcherscript.service.ConstructService
import com.rattlehead.witcherscript.service.TypeService

fun getType(element: WSEnumValue): Type {
    element.stub?.let { return Type.fromString(it.typeString, element.project) }
    return CustomType(ConstructService.getEnum(element)?.id!!, element.project)
}

fun getType(element: WSCallable): Type {
    (element.stub as? WSFunctionStub)?.let { return Type.fromString(it.typeString, element.project) }
    return when (element) {
        is WSEvent -> BOOL
        is WSFunction -> element.typeElement?.toType() ?: VOID
        else -> error("Unknown callable type: $element")
    }
}

fun getType(element: WSProperty): Type {
    element.stub?.let { return Type.fromString(it.typeString, element.project) }
    return when (element) {
        is WSField -> PsiTreeUtil.getRequiredChildOfType(element.parent, WSType::class.java).toType()
        is WSAutobind -> element.typeElement.toType()
        else -> error("Unknown property type: $element")
    }
}

fun getType(element: WSParameter): Type {
    return PsiTreeUtil.getRequiredChildOfType(element.parent, WSType::class.java).toType()
}

fun getType(element: WSVariable): Type {
    return PsiTreeUtil.getRequiredChildOfType(element.parent, WSType::class.java).toType()
}

fun getType(element: WSTernaryExpression): Type? {
    return element.falseClause?.type
}

fun getType(element: WSBinaryExpression): Type? {
    return TypeService.getBinaryType(element.operator.text, element.left.type, element.right?.type)
}

fun getType(element: WSUnaryExpression): Type? {
    return TypeService.getUnaryType(element.operator.text, element.expression?.type)
}

fun getType(element: WSNewExpression): CustomType {
    return element.typeElement.toType() as CustomType
}

fun getType(element: WSTypeCastExpression): Type {
    return element.typeElement.toType()
}

fun getType(element: WSCallExpression): Type? {
    return when (val resolved = element.referenceIdentifier.reference?.resolve()) {
        is WSStruct -> CustomType(resolved.id ?: return null, element.project)
        is WSCallable -> resolved.type
        else -> null
    }
}

fun getType(element: WSSuperCallExpression): Type? {
    return (element.referenceIdentifier.reference?.resolve() as? Typed)?.type
}

fun getType(element: WSMemberCallExpression): Type? {
    val type = (element.referenceIdentifier.reference?.resolve() as? WSCallable)?.type
    if (type?.id == "__element") {
        return (element.owner.type as? ArrayType)?.enclosedType
    }
    return type
}

fun getType(element: WSIndexExpression): Type? {
    return (element.owner.type as? ArrayType)?.enclosedType
}

fun getType(element: WSPropertyExpression): Type? {
    return (element.referenceIdentifier.reference?.resolve() as? Typed)?.type
}

fun getType(element: WSThisExpression): Type? {
    return CustomType(ConstructService.get(element)?.id ?: return null, element.project)
}

fun getType(element: WSParentExpression): Type? {
    return CustomType(ConstructService.getState(element)?.ownerName ?: return null, element.project)
}

fun getType(element: WSLiteralExpression): Type? {
    return when {
        element.intLiteral != null -> INT
        element.floatLiteral != null -> FLOAT
        element.boolLiteral != null -> BOOL
        element.nameLiteral != null -> NAME
        element.stringLiteral != null -> STRING
        element.nullLiteral != null -> NULL
        else -> null
    }
}

fun getType(element: WSVariableExpression): Type? {
    return (element.referenceIdentifier.reference?.resolve() as? Typed)?.type
}

fun getType(element: WSGlobalVariableExpression): Type {
    val typeString = when (element.text) {
        "theGame" -> "CR4Game"
        "thePlayer" -> "CR4Player"
        "theCamera" -> "CR4CameraDirector"
        "theSound" -> "CScriptSoundSystem"
        "theDebug" -> "CDebugAttributesManager"
        "theTimer" -> "CTimerScriptKeyword"
        "theInput" -> "CInputManager"
        "theTelemetry" -> "CR4TelemetryScriptProxy"
        else -> error("Unknown global variable ${element.text}")
    }
    return CustomType(typeString, element.project)
}

fun getType(element: WSParenthesesExpression): Type? {
    return element.expression?.type
}