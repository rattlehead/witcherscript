@file:JvmMultifileClass @file:JvmName("WSPsiImplUtil")

package com.rattlehead.witcherscript.psi.impl

import com.intellij.openapi.command.WriteCommandAction
import com.intellij.psi.PsiElement
import com.intellij.psi.StubBasedPsiElement
import com.rattlehead.witcherscript.psi.Named
import com.rattlehead.witcherscript.service.ElementFactory
import com.rattlehead.witcherscript.psi.stub.NamedStub

fun getName(named: Named): String? {
    ((named as? StubBasedPsiElement<*>)?.stub as? NamedStub)?.let { return it.name }
    return named.nameIdentifier?.text
}

fun setName(named: Named, newName: String): PsiElement {
    return WriteCommandAction.runWriteCommandAction<PsiElement>(named.project) {
        named.nameIdentifier!!.replace(ElementFactory.createNameIdentifier(named.project, newName))
    }
}

fun getTextOffset(named: Named): Int {
    return named.nameIdentifier?.textOffset ?: named.node.startOffset
}