@file:JvmMultifileClass @file:JvmName("WSPsiImplUtil")

package com.rattlehead.witcherscript.psi.impl

import com.intellij.extapi.psi.StubBasedPsiElementBase
import com.intellij.psi.util.PsiTreeUtil
import com.rattlehead.witcherscript.common.AccessModifier
import com.rattlehead.witcherscript.common.FunctionModifier
import com.rattlehead.witcherscript.psi.*
import com.rattlehead.witcherscript.psi.stub.ModifiableStub

fun getModifiersString(modifiable: Modifiable): String {
    ((modifiable as? StubBasedPsiElementBase<*>)?.stub as? ModifiableStub)?.let { return it.modifiersString }
    return modifiable.modifiers.text
}

fun getModifiers(parameter: WSParameter): WSModifiers {
    return PsiTreeUtil.getRequiredChildOfType(parameter.parent, WSModifiers::class.java)
}

fun getModifiers(property: WSProperty): WSModifiers {
    return PsiTreeUtil.getRequiredChildOfType(property.parent, WSModifiers::class.java)
}

fun getFunctionModifier(function: WSFunction): FunctionModifier {
    return with(function.modifiersString) { when {
        contains("exec") -> FunctionModifier.EXEC
        contains("quest") -> FunctionModifier.QUEST
        contains("storyscene") -> FunctionModifier.STORYSCENE
        contains("reward") -> FunctionModifier.REWARD
        contains("timer") -> FunctionModifier.TIMER
        contains("entry") -> FunctionModifier.ENTRY
        contains("cleanup") -> FunctionModifier.CLEANUP
        else -> FunctionModifier.NONE
    }}
}

fun getAccessModifier(modifiable: Modifiable): AccessModifier? {
    return with(modifiable.modifiersString) { when {
        contains("private") -> AccessModifier.PRIVATE
        contains("protected") -> AccessModifier.PROTECTED
        contains("public") -> AccessModifier.PUBLIC
        else -> null
    }}
}

fun isImport(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("import")
}

fun isAbstract(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("abstract")
}

fun isStatemachine(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("statemachine")
}

fun isConst(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("const")
}

fun isEditable(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("editable")
}

fun isInlined(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("inlined")
}

fun isSaved(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("saved")
}

fun isOptional(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("optional")
}

fun isFinal(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("final")
}

fun isLatent(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("latent")
}

fun isOut(modifiable: Modifiable): Boolean {
    return modifiable.modifiersString.contains("out")
}