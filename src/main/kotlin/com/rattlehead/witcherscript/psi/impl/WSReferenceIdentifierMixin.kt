package com.rattlehead.witcherscript.psi.impl

import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReference
import com.intellij.psi.PsiReferenceBase
import com.rattlehead.witcherscript.common.ArrayType
import com.rattlehead.witcherscript.common.CustomType
import com.rattlehead.witcherscript.psi.*
import com.rattlehead.witcherscript.service.*
import java.util.function.BiFunction

abstract class WSReferenceIdentifierMixin(node: ASTNode) : ASTWrapperPsiElement(node) {
    override fun getReference(): PsiReference? {
        return when (parent) {
            is WSExtendsClause -> createReference { _, parent: WSExtendsClause ->
                ConstructService.findSuper(parent.parent as WSConstruct)
            }
            is WSInClause -> createReference { id: String, parent: WSInClause ->
                ConstructService.findClass(parent.project, id)
            }
            is WSCustomType -> createReference { _, parent: WSCustomType ->
                (parent.toType() as CustomType).resolved
            }
            is WSCallExpression -> createReference { id: String, parent: WSCallExpression ->
                CallableService.findMember(parent, id)
                    ?: CallableService.findGlobal(parent.project, id)
                    ?: ConstructService.findStruct(parent.project, id)
            }
            is WSSuperCallExpression -> createReference { id: String, parent: WSSuperCallExpression ->
                CallableService.findSuper(parent, id)
            }
            is WSDefaultInline, is WSHint -> createReference { id: String, parent: PsiElement ->
                VariableService.findMember(parent, id)
            }
            is WSVariableExpression -> createReference { id: String, parent: WSVariableExpression ->
                VariableService.findLocal(parent, id)
                    ?: VariableService.findMember(parent, id)
                    ?: VariableService.findEnumValue(parent.project, id)
            }
            is WSMemberCallExpression -> createReference { id: String, parent: WSMemberCallExpression ->
                with(parent.owner.type) { when (this) {
                    is ArrayType -> CallableService.findArrayFunction(parent.project, id)
                    is CustomType -> resolved?.let { CallableService.findMember(it, id) }
                    else -> null
                }}
            }
            is WSPropertyExpression -> createReference { id: String, parent: WSPropertyExpression ->
                (parent.owner.type as? CustomType)?.resolved?.let { VariableService.findMember(it, id) }
            }
            else -> null
        }
    }

    private fun <P : PsiElement> createReference(resolve: BiFunction<String, P, PsiElement?>): PsiReference {
        return object : PsiReferenceBase<PsiElement?>(this, TextRange.from(0, textLength)) {
            override fun handleElementRename(newName: String): PsiElement {
                return WriteCommandAction.runWriteCommandAction<PsiElement>(element.project) {
                    element.replace(ElementFactory.createReferenceIdentifier(element.project, newName))
                }
            }

            override fun resolve(): PsiElement? {
                @Suppress("UNCHECKED_CAST")
                return resolve.apply(element.text, element.parent as P)
            }
        }
    }
}
