@file:JvmMultifileClass @file:JvmName("WSPsiImplUtil")

package com.rattlehead.witcherscript.psi.impl

import com.rattlehead.witcherscript.common.ArrayType
import com.rattlehead.witcherscript.common.BuiltinType
import com.rattlehead.witcherscript.common.CustomType
import com.rattlehead.witcherscript.common.Type
import com.rattlehead.witcherscript.common.Type.Companion.BOOL
import com.rattlehead.witcherscript.common.Type.Companion.BYTE
import com.rattlehead.witcherscript.common.Type.Companion.FLOAT
import com.rattlehead.witcherscript.common.Type.Companion.INT
import com.rattlehead.witcherscript.common.Type.Companion.NAME
import com.rattlehead.witcherscript.common.Type.Companion.STRING
import com.rattlehead.witcherscript.psi.*
import com.rattlehead.witcherscript.psi.stub.WSClassStub
import com.rattlehead.witcherscript.psi.stub.WSStateStub

fun getId(construct: WSConstruct): String? {
    return when (construct) {
        is WSClass, is WSStruct, is WSEnum -> construct.name
        is WSState -> "${construct.ownerName ?: return null}State${construct.name ?: return null}"
        else -> error("Unknown construct $construct")
    }
}

fun getParentName(clazz: WSClass): String? {
    (clazz.stub as? WSClassStub)?.let { return it.parentName }
    return clazz.extendsClause?.referenceIdentifier?.text
}

fun getParentName(state: WSState): String? {
    (state.stub as? WSStateStub)?.let { return it.parentName }
    return state.extendsClause?.referenceIdentifier?.text
}

fun getOwnerName(state: WSState): String? {
    (state.stub as? WSStateStub)?.let { return it.ownerName }
    return state.inClause.referenceIdentifier.text
}

fun isAbstract(callable: WSCallable): Boolean {
    callable.stub?.let { return it.isAbstract }
    return callable.body == null
}

fun toType(element: WSType): Type {
    when (element) {
        is WSPrimitiveType -> when (element.text) {
            "byte" -> return BYTE
            "int" -> return INT
            "float" -> return FLOAT
            "bool" -> return BOOL
            "name" -> return NAME
            "string" -> return STRING
        }
        is WSBuiltinType -> return BuiltinType(element.text)
        is WSCustomType -> return CustomType(element.text, element.project)
        is WSArrayType -> return ArrayType(element.enclosedType.toType())
    }
    error("Unknown type: ${element.text}")
}