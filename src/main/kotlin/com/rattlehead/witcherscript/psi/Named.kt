package com.rattlehead.witcherscript.psi

import com.intellij.psi.PsiNameIdentifierOwner

interface Named : PsiNameIdentifierOwner
