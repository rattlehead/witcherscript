package com.rattlehead.witcherscript.annotator

import com.intellij.psi.PsiElement
import com.rattlehead.witcherscript.annotator.AnnotationRange.*
import com.rattlehead.witcherscript.psi.*
import com.rattlehead.witcherscript.common.FunctionModifier.*
import com.rattlehead.witcherscript.common.Type.Companion.BOOL
import com.rattlehead.witcherscript.common.Type.Companion.INT
import com.rattlehead.witcherscript.common.Type.Companion.NAME
import com.rattlehead.witcherscript.common.Type.Companion.VOID
import com.rattlehead.witcherscript.common.isEnum
import com.rattlehead.witcherscript.common.isHandle

fun DSLRuleset<PsiElement, PsiElement>.annotatorDSL() {
    rulesFor<WSConstruct> {
        whenever {
            duplicates.isNotEmpty()
        } show error(IDENTIFIER) { "Duplicate construct: $id" }

        rulesWhen({ isImport }) {
            whenever {
                native == null
            } show error(DECLARATION) { "The native $constructType $id is not found" }

            whenever {
                native != null && isAbstract
            } show warning(MODIFIERS) { "Unnecessary 'abstract' qualifier for imported type" }

            whenever {
                native != null && parentConstruct isNot native!!.parentConstruct
            } show error(DECLARATION) { "Cannot import native $constructType with different super type (${parentConstruct?.id} instead of ${native!!.parentConstruct?.id})" }
        }

        rulesFor<WSClass> {
            whenever {
                parentName == "IReferencable" || parentConstruct?.native?.isAbstract == true
            } unless {
                isImport
            } show error(DECLARATION) { "Cannot extend a native abstract construct ${parentConstruct!!.id}" }

            whenever {
                parentName == "CObject"
            } show warning(DECLARATION) { "Explicitly extending CObject is unnecessary" }
        }

        rulesFor<WSStruct> {
            whenever {
                body.fieldListList.isEmpty()
            } unless {
                isImport
            } show error { "Cannot declare a struct with no properties" }
        }

        rulesFor<WSState> {
            whenever {
                ownerClass?.isStatemachine == false
            } show warning(DECLARATION) { "The owner class is not statemachine" }
        }
    }

    rulesFor<WSCallable> {
        whenever {
            duplicates.isNotEmpty()
        } show error(IDENTIFIER) { "Duplicate callable: $name" }

        whenever {
            superCallable != null && parameters notMatch superCallable!!.parameters
        } show error(PARAMETERS) { "Cannot override a function with different parameters" }

        rulesFor<WSEvent> {
            whenever {
                name notMatches "On.*"
            } show error(IDENTIFIER) { "An event name must start with \"On\"" }

            whenever {
                superCallable != null && superCallable !is WSEvent
            } show error(DECLARATION) { "The base function is not an event" }

            whenever {
                isAbstract
            } unless {
                construct!!.isAbstract || construct!!.isImport
            } show error(DECLARATION) { "An abstract event must be declared in an abstract class or state" }
        }

        rulesFor<WSFunction> {
            rulesWhen({ isImport }) {
                whenever {
                    native == null
                } show warning(DECLARATION) { "The native function $name is not found" }

                whenever {
                    isImport && native != null && type != native!!.type
                } show warning(RETURN_TYPE) { "The native function $name has different return type" }

                whenever {
                    isImport && native != null && parameters notMatch native!!.parameters
                } show warning(PARAMETERS) { "The native function $name has different parameters" }
            }

            whenever {
                !isImport && native != null
            } show warning(DECLARATION) { "The function overshadows a native function $name" }

            whenever {
                isAbstract && construct == null
            } unless {
                isImport
            } show warning { "The global function has no body" }

            whenever {
                isAbstract && construct != null
            } unless {
                isImport || construct!!.isAbstract || construct!!.isImport
            } show error(DECLARATION) { "An abstract function must be declared in an abstract class or state" }

            whenever {
                isFinal && construct == null
            } show error(MODIFIERS) { "A global function cannot be final" }

            whenever {
                accessModifier != null && construct == null
            } show error(MODIFIERS) { "A global function cannot have an explicit access modifier" }

            whenever {
                superCallable != null && (superCallable is WSEvent || functionModifier != superFunction!!.functionModifier)
            } show error(DECLARATION) { "Cannot override a function with different type modifier" }

            rulesWhen({ superFunction != null }) {
                whenever {
                    superFunction!!.isFinal
                } show error(DECLARATION) { "Cannot override a final function" }

                whenever {
                    superFunction!!.functionModifier == TIMER
                } show error(DECLARATION) { "Cannot override a timer function" }

                whenever {
                    functionModifier == ENTRY && construct is WSState && superFunction!!.construct is WSState
                            && (construct as WSState).ownerName == (superFunction!!.construct as WSState).ownerName
                } show error(DECLARATION) { "Cannot override an entry function within the same statemachine class" }

                whenever {
                    accessModifier weakerThan superFunction!!.accessModifier
                } show error(MODIFIERS) { "Cannot override a function with weaker access modifier" }

                whenever {
                    isLatent && !superFunction!!.isLatent
                } show error(MODIFIERS) { "The base function is not latent" }

                whenever {
                    !isLatent && superFunction!!.isLatent
                } show error(MODIFIERS) { "The base function is latent" }

                whenever {
                    type != superFunction!!.type
                } show error(RETURN_TYPE) { "Cannot override a function with a different return type" }
            }

            rulesWhen({ functionModifier == CLEANUP }) {
                whenever {
                    parameters notMatch emptyList()
                } show error(PARAMETERS) { "A cleanup function must not accept any parameters" }

                whenever {
                    type != VOID
                } show error(RETURN_TYPE) { "A cleanup function cannot return anything" }

                whenever {
                    construct !is WSState
                } show error(DECLARATION) { "A cleanup function cannot be declared outside of state" }
            }

            rulesWhen({ functionModifier == TIMER }) {
                whenever {
                    isImport
                } show error(MODIFIERS) { "An imported function cannot be timed" }

                whenever {
                    construct == null
                } show error(MODIFIERS) { "A timer function cannot be global" }

                whenever {
                    parameters notMatch listOf("Float|GameTime", "Int32")
                } show error(PARAMETERS) { "A timer function must accept parameters (float or GameTime, int)" }

                whenever {
                    isLatent
                } show error(MODIFIERS) { "A timer function cannot be latent" }

                whenever {
                    type != VOID
                } show error(RETURN_TYPE) { "A timer function cannot return anything" }
            }

            rulesWhen({ functionModifier == STORYSCENE }) {
                whenever {
                    parameters notMatch listOf("CStoryScenePlayer", "...")
                } show error(PARAMETERS) { "A storyscene function must accept CStoryScenePlayer as its first parameter" }
            }

            rulesWhen({ functionModifier == ENTRY }) {
                whenever {
                    isLatent
                } show error(MODIFIERS) { "An entry function cannot be latent" }

                whenever {
                    type != VOID
                } show error(RETURN_TYPE) { "An entry function cannot return anything" }

                whenever {
                    construct !is WSState
                } show error(DECLARATION) { "An entry function cannot be declared outside of state" }
            }

            rulesWhen({ functionModifier == EXEC }) {
                whenever {
                    construct != null
                } show error(DECLARATION) { "An exec function can only be declared globally" }

                whenever {
                    isLatent
                } show error(MODIFIERS) { "An exec function cannot be latent" }
            }
        }
    }

    rulesFor<WSParameter> {
        whenever {
            duplicates.isNotEmpty()
        } show error { "Duplicate local variable: $name" }
    }

    rulesFor<WSVariable> {
        whenever {
            duplicates.isNotEmpty()
        } show error { "Duplicate local variable: $name" }
    }

    rulesFor<WSEnumValue> {
        whenever {
            duplicates.isNotEmpty()
        } show error { "Duplicate enum value: $name" }
    }

    rulesFor<WSProperty> {
        whenever {
            duplicates.isNotEmpty()
        } show error { "Duplicate property: $name" }

        rulesFor<WSField> {
            rulesWhen({ isImport }) {
                whenever {
                    native == null
                } show error { "The native field $name is not found" }

                whenever {
                    native != null && type != native!!.type
                } show error { "The native field $name has different type" }

                whenever {
                    native != null && construct!!.id != native!!.construct!!.id
                } show warning { "The native field $name is declared in a supertype: ${native!!.construct!!.id}" }
            }

            whenever {
                construct is WSStruct && accessModifier != null
            } show error(MODIFIERS) { "Cannot specify an explicit access modifier for a struct field" }
        }
    }

    rulesFor<WSStatement> {
        rulesFor<WSBlockStatement> {
            whenever {
                this `is` (parent as? WSCallableBody)?.statementList?.first()
            } show error { "(Compiler oversight) The first statement of a function cannot be a block statement" }
        }

        rulesFor<WSExpressionStatement> {
            always() unless {
                expression is WSCallExpression || expression is WSMemberCallExpression || expression is WSSuperCallExpression
                        || (expression as? WSBinaryExpression)?.operator?.text in listOf("=", "+=", "-=", "*=", "/=", "|=", "&=")
            } show warning { "The result of this expression is ignored" }
        }

        rulesFor<WSReturnStatement> {
            whenever {
                expression != null && expression!!.type notAssignableTo callable.type
            } show error { "Incorrect return type: ${expression!!.type}" }

            whenever {
                expression == null && callable.type != VOID
            } show error { "A return value expected" }
        }
    }

    rulesFor<WSExpression> {
        whenever {
            parent is WSIfClause || parent is WSElseIfClause || parent is WSWhileStatement || parent is WSDoWhileStatement
                    || this == (parent as? WSTernaryExpression)?.condition || parent == (parent.parent as? WSForStatement)?.condition
        } unless {
            type == NAME || type assignableTo BOOL
        } show error { "Expected boolean expression" }

        whenever {
            this == (parent as? WSIndexExpression)?.index
        } unless {
            type assignableTo INT
        } show error { "Expected integer expression" }

        rulesFor<WSTernaryExpression> {
            always() show warning { "(Compiler oversight) Ternary operator does not work as expected" }
        }

        rulesFor<WSBinaryExpression> {
            whenever {
                operator.text in listOf("=", "+=", "-=", "*=", "/=", "|=", "&=")
            } unless {
                left is WSVariableExpression || left is WSPropertyExpression || left is WSIndexExpression
            } show error { "A value can only be assigned to a variable, property or an array element" }

            whenever {
                operator.text == "="
            } unless {
                parent is WSExpressionStatement || (parent is WSForSection && parent != (parent.parent as WSForStatement).condition)
            } show warning { "(Compiler oversight) The assignment operator does not return a meaningful value" }

            whenever {
                left.type != null && right?.type != null && type == null
            } show error { "Cannot infer the expression type: ${left.type} ${operator.text} ${right!!.type}" }
        }

        rulesFor<WSUnaryExpression> {
            whenever {
                expression?.type != null && type == null
            } show error { "Cannot infer the expression type: ${operator.text} ${expression!!.type}" }
        }

        rulesFor<WSTypeCastExpression> {
            always() unless {
                typeElement is WSPrimitiveType || type.isHandle() || type.isEnum()
            } show error { "Type cast is only possible with a primitive, enum, or handle type" }
        }

        rulesForWhen<WSNewExpression>({ type.resolved != null }) {
            whenever {
                type.resolved notExtends "IScriptable"
            } show error { "Operator \"new\" is only applicable to scriptable classes and states" }

            whenever {
                expression == null && type.resolved isOrExtends "CObject"
            } unless {
                type.id in listOf("CCreateEntityHelper", "CR4CreateEntityHelper", "CInGameConfigWrapper", "CScriptBatchQueryAccessor")
            } show error { "Types extending CObject must be instantiated via \"new ... in ...\" syntax" }

            whenever {
                type.resolved!!.isNative
            } show error { "The ${type.resolved!!.constructType} $type must be imported" }

            whenever {
                type.resolved!!.native?.isAbstract == true || (type.resolved!!.isAbstract && type.resolved!!.native == null)
            } show error { "Cannot instantiate an abstract type" }
        }

        rulesFor<WSCallExpression> {
            whenever {
                callable != null && arguments notMatch callable!!.parameters
            } show error(PARAMETERS) { "Parameter types mismatch" }
        }

        rulesFor<WSSuperCallExpression> {
            whenever {
                callable != null && arguments notMatch callable!!.parameters
            } show error(PARAMETERS) { "Parameter types mismatch" }
        }

        rulesFor<WSMemberCallExpression> {
            whenever {
                callable != null && arguments notMatch callable!!.parameters
            } show error(PARAMETERS) { "Parameter types mismatch" }
        }
    }

    rulesFor<WSReferenceIdentifier> {
        whenever {
            target == null
        } unless {
            parent is WSHint || (parent is WSDefaultInline && text == "autoState")
        } show unresolved()

        whenever {
            target is Modifiable && !hasAccess
        } unless {
            parent is WSHint || parent is WSDefaultInline
        } show error { "Insufficient access: $text" }

        whenever {
            parent is WSHint && target == null
        } show warning { "The target property $text is missing" }
    }

    rulesFor<WSVariableList> {
        whenever {
            expression != null && expression!!.type notAssignableTo type.toType()
        } show error { "Incorrect expression type: ${expression!!.type}" }
    }
}