package com.rattlehead.witcherscript.annotator

import com.intellij.codeInspection.ProblemHighlightType
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import com.rattlehead.witcherscript.common.AccessModifier
import com.rattlehead.witcherscript.common.ArrayType
import com.rattlehead.witcherscript.common.Type
import com.rattlehead.witcherscript.psi.*
import com.rattlehead.witcherscript.service.CallableService
import com.rattlehead.witcherscript.service.ConstructService
import com.rattlehead.witcherscript.service.TypeService
import com.rattlehead.witcherscript.service.VariableService

@DslMarker
@Target(AnnotationTarget.CLASS)
annotation class AnnotatorDSL

@Suppress("unused", "MemberVisibilityCanBePrivate")
@AnnotatorDSL
abstract class AbstractDSLRule {
    abstract fun evaluate(element: PsiElement, holder: AnnotationHolder)

    val WSConstruct.duplicates: List<WSConstruct>
        get() = ConstructService.findDuplicates(this)

    val WSConstruct.native: WSConstruct?
        get() = ConstructService.findNative(this)

    val WSConstruct.parentConstruct: WSConstruct?
        get() = ConstructService.findSuper(this)

    val WSConstruct.constructType: String
        get() = when (this) {
            is WSClass -> "class"
            is WSState -> "state"
            is WSStruct -> "struct"
            is WSEnum -> "enum"
            else -> error("Unknown construct type: $javaClass")
        }

    val WSConstruct.isAbstract: Boolean
        get() = when (this) {
            is WSClass -> isAbstract
            is WSState -> isAbstract
            else -> false
        }

    val WSConstruct.isImport: Boolean
        get() = when (this) {
            is WSClass -> isImport
            is WSState -> isImport
            is WSStruct -> isImport
            else -> false
        }

    val WSConstruct.isNative: Boolean
        get() = this == native

    val WSState.ownerClass: WSClass?
        get() = ownerName?.let { ConstructService.findClass(project, it) }

    val WSCallable.duplicates: List<WSCallable>
        get() = CallableService.findDuplicates(this)

    val WSCallable.superCallable: WSCallable?
        get() = name?.let { CallableService.findSuper(this, it) }

    val WSCallable.construct: WSConstruct?
        get() = ConstructService.get(this)

    val WSEvent.superEvent: WSEvent?
        get() = superCallable as? WSEvent

    val WSFunction.superFunction: WSFunction?
        get() = superCallable as? WSFunction

    val WSFunction.native: WSFunction?
        get() = CallableService.findNative(this)

    val WSParameter.duplicates: List<Named>
        get() = VariableService.findDuplicates(this)

    val WSVariable.duplicates: List<Named>
        get() = VariableService.findDuplicates(this)

    val WSEnumValue.duplicates: List<WSEnumValue>
        get() = VariableService.findDuplicates(this)

    val WSProperty.duplicates: List<WSProperty>
        get() = VariableService.findDuplicates(this)

    val WSProperty.construct: WSConstruct?
        get() = ConstructService.get(this)

    val WSField.native: WSField?
        get() = VariableService.findNative(this)

    val WSField.isNative: Boolean
        get() = this == native

    val WSReferenceIdentifier.target: Named?
        get() = reference?.resolve() as? Named

    val WSReferenceIdentifier.hasAccess: Boolean
        get() = ConstructService.isSufficientAccess(this, target as Modifiable)

    val WSCallExpression.callable: WSCallable?
        get() = referenceIdentifier.reference?.resolve() as? WSCallable

    val WSSuperCallExpression.callable: WSCallable?
        get() = referenceIdentifier.reference?.resolve() as? WSCallable

    val WSMemberCallExpression.callable: WSCallable?
        get() = referenceIdentifier.reference?.resolve() as? WSCallable

    val WSStatement.callable: WSCallable
        get() = CallableService.get(this)!!

    val WSDefaultInline.referencedField: WSProperty?
        get() = referenceIdentifier.reference?.resolve() as? WSProperty

    infix fun PsiElement?.`is`(other: PsiElement?): Boolean = when {
        this == null -> other == null
        else -> isEquivalentTo(other)
    }

    infix fun PsiElement?.isNot(other: PsiElement?): Boolean = !`is`(other)

    infix fun AccessModifier?.weakerThan(other: AccessModifier?): Boolean = (this?.ordinal ?: 2) > (other?.ordinal ?: 2)

    infix fun String?.matches(pattern: String): Boolean {
        return this?.matches(pattern.toRegex()) ?: false
    }

    infix fun String?.notMatches(pattern: String): Boolean = !matches(pattern)

    infix fun WSParameters.match(expected: List<String>): Boolean {
        val types = CallableService.getParameters(this.parent as WSCallable).map { it.type.id }
        val template = expected.toMutableList()
        val wildcardIndex = expected.indexOf("...")
        if (wildcardIndex != -1) {
            template.removeAt(wildcardIndex)
            while (template.size < types.size) {
                template.add(wildcardIndex, ".*")
            }
        }
        return types.size == template.size && types.zip(template).all { it.first.matches(it.second.toRegex()) }
    }

    infix fun WSParameters.notMatch(expected: List<String>): Boolean = !match(expected)

    infix fun WSParameters.match(other: WSParameters): Boolean {
        val types = CallableService.getParameters(this.parent as WSCallable).map { it.type.id }
        val otherTypes = CallableService.getParameters(other.parent as WSCallable).map { it.type.id }
        return types == otherTypes
    }

    infix fun WSParameters.notMatch(other: WSParameters): Boolean = !match(other)

    infix fun WSConstruct?.isOrExtends(supposedParent: String): Boolean {
        this ?: return false
        val foundParent = ConstructService.find(project, supposedParent)
        return ConstructService.isOrExtends(this, foundParent ?: return false)
    }

    infix fun WSConstruct?.extends(supposedParent: String): Boolean {
        return isOrExtends(supposedParent) && supposedParent != this!!.id
    }

    infix fun WSConstruct?.notExtends(supposedParent: String): Boolean = !extends(supposedParent)

    infix fun Type?.assignableTo(other: Type?): Boolean = TypeService.isAssignable(this, other)

    infix fun Type?.notAssignableTo(other: Type?): Boolean = !assignableTo(other)

    infix fun WSArguments.match(parameters: WSParameters): Boolean {
        val params = CallableService.getParameters(parameters.parent as WSCallable)
        val elementType = ((parent as? WSMemberCallExpression)?.owner?.type as? ArrayType)?.enclosedType
        for (i in params.indices) {
            val expression = argumentList.getOrNull(i)?.expression
            val paramType = if (params[i].type.id == "__element") elementType else params[i].type
            if (expression == null && !params[i].isOptional
                || expression != null && expression.type notAssignableTo paramType) {
                return false
            }
        }
        return true
    }

    infix fun WSArguments.notMatch(parameters: WSParameters): Boolean = !match(parameters)
}

@Suppress("unused", "MemberVisibilityCanBePrivate")
class DSLRuleset<Inner : Outer, Outer : PsiElement>(
    private val condition: Outer.() -> Boolean
) : AbstractDSLRule() {
    private val rules: MutableList<AbstractDSLRule> = mutableListOf()

    @Suppress("UNCHECKED_CAST")
    override fun evaluate(element: PsiElement, holder: AnnotationHolder) {
        if ((element as Outer).condition()) {
            rules.forEach { it.evaluate(element, holder) }
        }
    }

    inline fun <reified T : Inner> rulesFor(noinline rulesInit: DSLRuleset<T, Inner>.() -> Unit) {
        rulesForWhen(T::class.java, { true }, rulesInit)
    }

    inline fun <reified T : Inner> rulesForWhen(noinline condition: T.() -> Boolean,
                                                noinline rulesInit: DSLRuleset<T, Inner>.() -> Unit) {
        rulesForWhen(T::class.java, condition, rulesInit)
    }

    fun <T : Inner> rulesForWhen(clazz: Class<T>, condition: T.() -> Boolean,
                                 rulesInit: DSLRuleset<T, Inner>.() -> Unit) {
        val ruleset = DSLRuleset<T, Inner> { clazz.isAssignableFrom(javaClass) && clazz.cast(this).condition() }
        ruleset.rulesInit()
        rules.add(ruleset)
    }

    fun rulesWhen(condition: Inner.() -> Boolean, rulesInit: DSLRuleset<Inner, Inner>.() -> Unit) {
        val ruleset = DSLRuleset(condition)
        ruleset.rulesInit()
        rules.add(ruleset)
    }

    fun always(): DSLRule<Inner> {
        return whenever { true }
    }

    fun whenever(condition: Inner.() -> Boolean): DSLRule<Inner> {
        val rule = DSLRule(condition)
        rules.add(rule)
        return rule
    }

    fun error(range: AnnotationRange, message: Inner.() -> String): AnnotationProducer<Inner> {
        return AnnotationProducer(HighlightSeverity.ERROR, range, message)
    }

    fun error(message: Inner.() -> String): AnnotationProducer<Inner> {
        return error(AnnotationRange.WHOLE, message)
    }

    fun warning(range: AnnotationRange, message: Inner.() -> String): AnnotationProducer<Inner> {
        return AnnotationProducer(HighlightSeverity.WARNING, range, message)
    }

    fun warning(message: Inner.() -> String): AnnotationProducer<Inner> {
        return warning(AnnotationRange.WHOLE, message)
    }

    fun unresolved(): AnnotationProducer<Inner> {
        return AnnotationProducer(HighlightSeverity.ERROR, AnnotationRange.WHOLE,
            { "Unresolved reference: $text" }, ProblemHighlightType.LIKE_UNKNOWN_SYMBOL)
    }
}

class DSLRule<T : PsiElement>(
    private val condition: T.() -> Boolean
) : AbstractDSLRule() {
    private var unless: (T.() -> Boolean)? = null
    private var annotation: AnnotationProducer<T>? = null

    @Suppress("UNCHECKED_CAST")
    override fun evaluate(element: PsiElement, holder: AnnotationHolder) {
        if ((element as T).condition() && unless?.invoke(element) != true) {
            annotation!!.produce(element, holder)
        }
    }

    infix fun unless(condition: T.() -> Boolean): DSLRule<T> {
        if (unless != null) {
            error("'Unless' condition is already specified")
        }
        unless = condition
        return this
    }

    infix fun show(annotation: AnnotationProducer<T>) {
        this.annotation = annotation
    }
}

class AnnotationProducer<T : PsiElement>(
    private val severity: HighlightSeverity,
    private val range: AnnotationRange,
    private val message: T.() -> String,
    private val type: ProblemHighlightType
) {
    constructor(
        severity: HighlightSeverity,
        range: AnnotationRange,
        message: (T) -> String
    ) : this(severity, range, message, ProblemHighlightType.GENERIC_ERROR_OR_WARNING)

    @Suppress("UNCHECKED_CAST")
    fun produce(element: PsiElement, holder: AnnotationHolder) {
        holder.newAnnotation(severity, (element as T).message())
            .highlightType(type)
            .range(range.get(element))
            .create()
    }
}

enum class AnnotationRange(
    val get: (PsiElement) -> TextRange
) {
    WHOLE({
        it.textRange
    }),
    IDENTIFIER({ when (it) {
        is Named -> it.nameIdentifier?.textRange ?: it.textRange
        else -> error("NAME range is not defined for ${it.javaClass.simpleName}")
    }}),
    DECLARATION({ when (it) {
        is WSClass -> TextRange.create(it.startOffset, (it.extendsClause ?: it.nameIdentifier).endOffset)
        is WSStruct, is WSEnum -> TextRange.create(it.startOffset, ((it as Named).nameIdentifier ?: it).endOffset)
        is WSState -> TextRange.create(it.startOffset, (it.extendsClause ?: it.inClause).endOffset)
        is WSCallable -> TextRange.create(it.startOffset, it.parameters.endOffset)
        else -> error("DECLARATION range is not defined for ${it.javaClass.simpleName}")
    }}),
    MODIFIERS({ when (it) {
        is Modifiable -> it.modifiers.textRange
        else -> error("MODIFIERS range is not defined for ${it.javaClass.simpleName}")
    }}),
    PARAMETERS({ when (it) {
        is WSCallable -> it.parameters.textRange
        is WSCallExpression -> it.arguments.textRange
        is WSSuperCallExpression -> it.arguments.textRange
        is WSMemberCallExpression -> it.arguments.textRange
        else -> error("PARAMETERS range is not defined for ${it.javaClass.simpleName}")
    }}),
    RETURN_TYPE({ when (it) {
        is WSFunction -> it.typeElement?.textRange ?: it.textRange
        else -> error("RETURN_TYPE range is not defined for ${it.javaClass.simpleName}")
    }})
}
