package com.rattlehead.witcherscript.annotator

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.psi.PsiElement
import com.rattlehead.witcherscript.WSNativeLibrary

object WSAnnotator {
    val DSL_ROOT: AbstractDSLRule by lazy {
        DSLRuleset<PsiElement, PsiElement> { !WSNativeLibrary.isNative(this) }.apply { annotatorDSL() }
    }

    class Extension : Annotator {
        override fun annotate(element: PsiElement, holder: AnnotationHolder) = DSL_ROOT.evaluate(element, holder)
    }
}