package com.rattlehead.witcherscript

import com.intellij.lexer.Lexer
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.tree.IElementType
import com.rattlehead.witcherscript.lexer.WSLexerAdapter
import com.rattlehead.witcherscript.psi.type.WSTypes.*
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors as Colors

object WSSyntaxHighlighter : SyntaxHighlighterBase() {
    private val WS_COMMENT = createTextAttributesKey("WS_COMMENT", Colors.BLOCK_COMMENT)
    private val WS_STRING_LITERAL = createTextAttributesKey("WS_STRING_LITERAL", Colors.STRING)
    private val WS_OTHER_LITERAL = createTextAttributesKey("WS_OTHER_LITERAL", Colors.NUMBER)
    private val WS_KEYWORD = createTextAttributesKey("WS_KEYWORD", Colors.KEYWORD)

    private val COMMENTS: List<IElementType> = listOf(BLOCK_COMMENT, LINE_COMMENT)
    private val STRING_LITERALS: List<IElementType> = listOf(STRING_LITERAL, NAME_LITERAL)
    private val OTHER_LITERALS: List<IElementType> = listOf(INT_LITERAL, FLOAT_LITERAL, BOOL_LITERAL, NULL_LITERAL)
    private val KEYWORDS: List<IElementType> = listOf(KW_CLASS, KW_STRUCT, KW_STATE, KW_ENUM, KW_FUNCTION, KW_EVENT,
        KW_VAR, KW_BYTE, KW_INT, KW_FLOAT, KW_BOOL, KW_STRING, KW_NAME, KW_ARRAY, KW_IMPORT, KW_PUBLIC, KW_PROTECTED,
        KW_PRIVATE, KW_ABSTRACT, KW_STATEMACHINE, KW_EXTENDS, KW_IN, KW_CONST, KW_INLINED, KW_SAVED, KW_EDITABLE,
        KW_DEFAULT, KW_DEFAULTS, KW_AUTOBIND, KW_SINGLE, KW_ANY, KW_HINT, KW_FINAL, KW_LATENT, KW_EXEC, KW_STORYSCENE,
        KW_REWARD, KW_CLEANUP, KW_QUEST, KW_ENTRY, KW_VOID, KW_IF, KW_ELSE, KW_DO, KW_FOR, KW_WHILE, KW_SWITCH, KW_CASE,
        KW_BREAK, KW_TIMER, KW_RETURN, KW_THIS, KW_PARENT, KW_SUPER, KW_OPTIONAL, KW_OUT, KW_CONTINUE, KW_NEW, KW_DELETE
    )

    override fun getHighlightingLexer(): Lexer = WSLexerAdapter()

    override fun getTokenHighlights(tokenType: IElementType?): Array<TextAttributesKey> {
        return when (tokenType) {
            in COMMENTS -> arrayOf(WS_COMMENT)
            in STRING_LITERALS -> arrayOf(WS_STRING_LITERAL)
            in OTHER_LITERALS -> arrayOf(WS_OTHER_LITERAL)
            in KEYWORDS -> arrayOf(WS_KEYWORD)
            else -> emptyArray()
        }
    }

    class Factory : SyntaxHighlighterFactory() {
        override fun getSyntaxHighlighter(project: Project?,
                                          virtualFile: VirtualFile?): SyntaxHighlighter = WSSyntaxHighlighter
    }
}