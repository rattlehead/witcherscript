package com.rattlehead.witcherscript.common

import com.intellij.openapi.project.Project
import com.rattlehead.witcherscript.psi.WSClass
import com.rattlehead.witcherscript.psi.WSConstruct
import com.rattlehead.witcherscript.psi.WSEnum
import com.rattlehead.witcherscript.psi.WSState
import com.rattlehead.witcherscript.service.ConstructService
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

sealed class Type(
    val id: String
) {
    companion object {
        val VOID = BuiltinType("void")
        val BYTE = BuiltinType("Uint8")
        val INT = BuiltinType("Int32")
        val FLOAT = BuiltinType("Float")
        val UINT64 = BuiltinType("Uint64")
        val BOOL = BuiltinType("Bool")
        val NAME = BuiltinType("CName")
        val STRING = BuiltinType("String")
        val NULL = BuiltinType("NULL")

        fun fromString(string: String, project: Project): Type {
            val parts = string.split("::", limit = 2)
            return when (parts[0]) {
                ArrayType::class.simpleName -> ArrayType(fromString(parts[1], project))
                BuiltinType::class.simpleName -> BuiltinType(parts[1])
                CustomType::class.simpleName -> CustomType(parts[1], project)
                else -> error("Unknown type ${parts[0]}")
            }
        }
    }

    override fun equals(other: Any?): Boolean = javaClass == other?.javaClass && id == (other as Type).id
    override fun hashCode(): Int = id.hashCode()
}

class ArrayType(
    val enclosedType: Type
) : Type("array<${enclosedType.id}>") {
    override fun toString(): String = "${ArrayType::class.simpleName}::$enclosedType"
}

class BuiltinType(
    id: String
) : Type(id) {
    override fun toString(): String = "${BuiltinType::class.simpleName}::$id"
}

class CustomType(
    id: String,
    project: Project
) : Type(id) {
    val resolved: WSConstruct? by SmartReference(project) { ConstructService.find(project, id) }

    override fun toString(): String = "${CustomType::class.simpleName}::$id"
}

@OptIn(ExperimentalContracts::class)
fun Type?.isHandle(): Boolean {
    contract {
        returns(true) implies (this@isHandle is CustomType)
    }
    return this is CustomType && (resolved is WSClass || resolved is WSState)
}

@OptIn(ExperimentalContracts::class)
fun Type?.isEnum(): Boolean {
    contract {
        returns(true) implies (this@isEnum is CustomType)
    }
    return this is CustomType && resolved is WSEnum
}