package com.rattlehead.witcherscript.common

enum class AccessModifier {
    PRIVATE, PROTECTED, PUBLIC
}