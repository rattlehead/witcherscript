package com.rattlehead.witcherscript.common

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.SmartPointerManager
import com.intellij.psi.SmartPsiElementPointer
import kotlin.reflect.KProperty

class SmartReference<T : PsiElement>(
    private val project: Project,
    private val supplier: () -> T?
) {
    private var pointer: SmartPsiElementPointer<T>? = null

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        val supplied by lazy { supplier() }
        if (pointer == null && supplied != null) {
            pointer = SmartPointerManager.getInstance(project).createSmartPsiElementPointer(supplied!!)
        }
        return pointer?.element
    }
}