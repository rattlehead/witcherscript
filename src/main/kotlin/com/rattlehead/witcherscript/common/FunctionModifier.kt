package com.rattlehead.witcherscript.common

enum class FunctionModifier {
    EXEC, QUEST, STORYSCENE, REWARD, CLEANUP, TIMER, ENTRY, NONE
}
