package com.rattlehead.witcherscript.index

import com.intellij.openapi.project.Project
import com.intellij.psi.search.GlobalSearchScope.allScope
import com.intellij.psi.stubs.IndexSink
import com.intellij.psi.stubs.StringStubIndexExtension
import com.intellij.psi.stubs.StubIndex
import com.intellij.psi.stubs.StubIndexKey
import com.rattlehead.witcherscript.psi.WSEnumValue
import com.rattlehead.witcherscript.psi.stub.WSEnumValueStub

object WSEnumValueIndex : Index<WSEnumValueStub> {
    val KEY: StubIndexKey<String, WSEnumValue> = StubIndexKey.createIndexKey("witcherscript.enumvalue")

    override fun indexStub(stub: WSEnumValueStub, indexSink: IndexSink) {
        indexSink.occurrence(KEY, stub.name ?: return)
    }

    fun findAll(project: Project, name: String): Collection<WSEnumValue> {
        return StubIndex.getElements(KEY, name, project, allScope(project), WSEnumValue::class.java)
    }

    class Extension : StringStubIndexExtension<WSEnumValue>() {
        override fun getKey(): StubIndexKey<String, WSEnumValue> = KEY
    }
}
