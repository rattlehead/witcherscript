package com.rattlehead.witcherscript.index

import com.intellij.openapi.project.Project
import com.intellij.psi.search.GlobalSearchScope.allScope
import com.intellij.psi.stubs.IndexSink
import com.intellij.psi.stubs.StringStubIndexExtension
import com.intellij.psi.stubs.StubIndex.getElements
import com.intellij.psi.stubs.StubIndexKey
import com.rattlehead.witcherscript.psi.WSProperty
import com.rattlehead.witcherscript.psi.stub.WSConstructStub
import com.rattlehead.witcherscript.psi.stub.WSPropertyStub

object WSPropertyIndex : Index<WSPropertyStub> {
    val KEY: StubIndexKey<String, WSProperty> = StubIndexKey.createIndexKey("witcherscript.property")

    override fun indexStub(stub: WSPropertyStub, indexSink: IndexSink) {
        (stub.parentStub as? WSConstructStub)?.let {
            indexSink.occurrence(KEY, "${it.id ?: return}.${stub.name ?: return}")
        }
    }

    fun findAll(project: Project, name: String, constructId: String): Collection<WSProperty> {
        return getElements(KEY, "$constructId.$name", project, allScope(project), WSProperty::class.java)
    }

    class Extension : StringStubIndexExtension<WSProperty>() {
        override fun getKey(): StubIndexKey<String, WSProperty> = KEY
    }
}