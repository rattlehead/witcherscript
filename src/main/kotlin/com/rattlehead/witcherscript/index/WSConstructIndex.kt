package com.rattlehead.witcherscript.index

import com.intellij.openapi.project.Project
import com.intellij.psi.search.GlobalSearchScope.allScope
import com.intellij.psi.stubs.IndexSink
import com.intellij.psi.stubs.StringStubIndexExtension
import com.intellij.psi.stubs.StubIndex.getElements
import com.intellij.psi.stubs.StubIndexKey
import com.rattlehead.witcherscript.psi.WSConstruct
import com.rattlehead.witcherscript.psi.stub.WSConstructStub

object WSConstructIndex : Index<WSConstructStub> {
    val KEY: StubIndexKey<String, WSConstruct> = StubIndexKey.createIndexKey("witcherscript.construct")

    override fun indexStub(stub: WSConstructStub, indexSink: IndexSink) {
        indexSink.occurrence(KEY, stub.id ?: return)
    }

    fun findAll(project: Project, id: String): Collection<WSConstruct> {
        return getElements(KEY, id, project, allScope(project), WSConstruct::class.java)
    }

    class Extension : StringStubIndexExtension<WSConstruct>() {
        override fun getKey(): StubIndexKey<String, WSConstruct> = KEY
    }
}