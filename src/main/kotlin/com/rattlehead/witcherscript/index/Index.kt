package com.rattlehead.witcherscript.index

import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.IndexSink
import com.intellij.psi.stubs.StubElement

interface Index<T : StubElement<out PsiElement?>> {
    fun indexStub(stub: T, indexSink: IndexSink)
}