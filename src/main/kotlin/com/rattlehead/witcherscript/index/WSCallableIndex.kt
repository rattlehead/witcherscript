package com.rattlehead.witcherscript.index

import com.intellij.openapi.project.Project
import com.intellij.psi.search.GlobalSearchScope.allScope
import com.intellij.psi.stubs.IndexSink
import com.intellij.psi.stubs.StringStubIndexExtension
import com.intellij.psi.stubs.StubIndex.getElements
import com.intellij.psi.stubs.StubIndexKey
import com.rattlehead.witcherscript.psi.WSCallable
import com.rattlehead.witcherscript.psi.stub.WSCallableStub
import com.rattlehead.witcherscript.psi.stub.WSConstructStub

object WSCallableIndex : Index<WSCallableStub> {
    val KEY: StubIndexKey<String, WSCallable> = StubIndexKey.createIndexKey("witcherscript.callable")

    override fun indexStub(stub: WSCallableStub, indexSink: IndexSink) {
        (stub.parentStub as? WSConstructStub)?.let {
            indexSink.occurrence(KEY, "${it.id ?: return}.${stub.name ?: return}")
        } ?: indexSink.occurrence(KEY, stub.name ?: return)
    }

    fun findAll(project: Project, name: String): Collection<WSCallable> {
        return getElements(KEY, name, project, allScope(project), WSCallable::class.java)
    }

    fun findAll(project: Project, name: String, constructId: String): Collection<WSCallable> {
        return getElements(KEY, "$constructId.$name", project, allScope(project), WSCallable::class.java)
    }

    class Extension : StringStubIndexExtension<WSCallable>() {
        override fun getKey(): StubIndexKey<String, WSCallable> = KEY
    }
}