package com.rattlehead.witcherscript

import com.intellij.openapi.project.Project
import com.intellij.openapi.roots.AdditionalLibraryRootsProvider
import com.intellij.openapi.roots.SyntheticLibrary
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiElement
import com.intellij.psi.stubs.StubElement

object WSNativeLibrary {
    private val LIBRARY = object : SyntheticLibrary() {
        val FILE = VfsUtil.findFileByURL(javaClass.getResource("/native")!!)!!
        override fun equals(other: Any?): Boolean = this === other
        override fun hashCode(): Int = 0
        override fun getSourceRoots(): Collection<VirtualFile> = listOf(FILE)
    }

    fun isNative(element: PsiElement): Boolean {
        return LIBRARY.contains(element.containingFile.virtualFile)
    }

    fun isNative(stub: StubElement<*>): Boolean {
        return isNative(stub.containingFileStub?.psi ?: return false)
    }

    class Extension : AdditionalLibraryRootsProvider() {
        override fun getAdditionalProjectLibraries(project: Project): Collection<SyntheticLibrary> = listOf(LIBRARY)
    }
}