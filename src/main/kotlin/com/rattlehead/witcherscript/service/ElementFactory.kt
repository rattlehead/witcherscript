package com.rattlehead.witcherscript.service

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFileFactory
import com.rattlehead.witcherscript.WSLanguage
import com.rattlehead.witcherscript.psi.*
import com.rattlehead.witcherscript.psi.impl.WSFile

object ElementFactory {
    fun createNameIdentifier(project: Project?, name: String): WSNameIdentifier {
        val file = PsiFileFactory.getInstance(project)
            .createFileFromText(WSLanguage.INSTANCE, "class $name {}") as WSFile
        return (file.firstChild as WSClass).nameIdentifier
    }

    fun createReferenceIdentifier(project: Project?, name: String): WSReferenceIdentifier {
        val file = PsiFileFactory.getInstance(project)
            .createFileFromText(WSLanguage.INSTANCE, "class Test extends $name {}") as WSFile
        return (file.firstChild as WSClass).extendsClause!!.referenceIdentifier
    }
}
