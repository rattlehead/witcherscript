package com.rattlehead.witcherscript.service

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.rattlehead.witcherscript.WSNativeLibrary
import com.rattlehead.witcherscript.index.WSEnumValueIndex
import com.rattlehead.witcherscript.index.WSPropertyIndex
import com.rattlehead.witcherscript.psi.*

@Suppress("unused", "MemberVisibilityCanBePrivate")
object VariableService {
    fun findLocal(context: PsiElement, name: String): Named? {
        return findAllLocal(context, name)
            .firstOrNull()
    }

    fun findMember(context: PsiElement, name: String): WSProperty? {
        return findAllMembers(context, name)
            .firstOrNull()
    }

    fun findEnumValue(project: Project, name: String): WSEnumValue? {
        return findAllEnumValues(project, name)
            .firstOrNull()
    }

    fun findDuplicates(element: WSParameter): List<Named> {
        return findAllLocal(element, element.name ?: return emptyList())
            .filter { !it.isEquivalentTo(element) }
            .toList()
    }

    fun findDuplicates(element: WSVariable): List<Named> {
        return findAllLocal(element, element.name ?: return emptyList())
            .filter { !it.isEquivalentTo(element) }
            .toList()
    }

    fun findDuplicates(element: WSProperty): List<WSProperty> {
        return findAllMembers(element, element.name ?: return emptyList())
            .filter { !(element is WSField && element.isImport && WSNativeLibrary.isNative(it)) }
            .filter { !it.isEquivalentTo(element) }
            .toList()
    }

    fun findDuplicates(element: WSEnumValue): List<WSEnumValue> {
        return findAllEnumValues(element.project, element.name ?: return emptyList())
            .filter { !it.isEquivalentTo(element) }
            .toList()
    }

    fun findNative(field: WSField): WSField? {
        return findAllMembers(field, field.name ?: return null)
            .find(WSNativeLibrary::isNative) as? WSField
    }

    private fun findAllLocal(context: PsiElement, name: String): Sequence<Named> {
        val callable = CallableService.get(context) ?: return emptySequence()
        val variables = callable.body?.variableListList?.flatMap { it.variableList } ?: emptyList()
        val parameters = CallableService.getParameters(callable)
        return (variables + parameters).asSequence().filter { it.name == name }
    }

    private fun findAllMembers(context: PsiElement, name: String): Sequence<WSProperty> {
        return generateSequence(ConstructService.get(context), ConstructService::findSuper)
            .flatMap {
                WSPropertyIndex.findAll(context.project, name, it.id ?: return@flatMap emptyList())
                    .sortedBy(WSNativeLibrary::isNative)
            }
    }

    private fun findAllEnumValues(project: Project, name: String): Sequence<WSEnumValue> {
        return WSEnumValueIndex.findAll(project, name).asSequence()
    }
}