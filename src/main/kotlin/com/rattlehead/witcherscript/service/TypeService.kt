package com.rattlehead.witcherscript.service

import com.rattlehead.witcherscript.common.Type
import com.rattlehead.witcherscript.common.Type.Companion.BOOL
import com.rattlehead.witcherscript.common.Type.Companion.BYTE
import com.rattlehead.witcherscript.common.Type.Companion.FLOAT
import com.rattlehead.witcherscript.common.Type.Companion.INT
import com.rattlehead.witcherscript.common.Type.Companion.NAME
import com.rattlehead.witcherscript.common.Type.Companion.NULL
import com.rattlehead.witcherscript.common.Type.Companion.STRING
import com.rattlehead.witcherscript.common.Type.Companion.UINT64
import com.rattlehead.witcherscript.common.isEnum
import com.rattlehead.witcherscript.common.isHandle
import com.rattlehead.witcherscript.service.ConstructService.isOrExtends

object TypeService {
    fun isAssignable(from: Type?, to: Type?): Boolean {
        return when {
            from == null || to == null -> false
            from == to -> true

            from(NULL) -> to.isHandle()
            from(BYTE) -> to(INT, FLOAT, BOOL, STRING)
            from(INT) -> to(BYTE, FLOAT, BOOL, STRING) || to.isEnum()
            from(FLOAT) -> to(BOOL, STRING)
            from(BOOL) -> to(STRING)
            from(NAME) -> to(STRING)
            from(STRING) -> to(BOOL)
            from.isEnum() -> to(INT, STRING)
            from.isHandle() -> to(BOOL, STRING) || to.isHandle() && isOrExtends(from.resolved!!, to.resolved!!)
            else -> false
        }
    }

    fun getBinaryType(operator: String, left: Type?, right: Type?): Type? {
        return when {
            left?.id == "Vector" && (right == left || right(BYTE, INT, FLOAT)) ->
                operator("+", "-", "*", "/", "+=", "-=", "*=", "/=") then left
            left(BYTE, INT, FLOAT) && right?.id == "Vector" ->
                operator("*") then right

            left?.id == "SAbilityAttributeValue" && right == left ->
                operator("+", "-", "+=", "-=") then left
            left?.id == "SAbilityAttributeValue" && right(BYTE, INT, FLOAT) ->
                operator("*") then left

            left?.id == "GameTime" && right == left ->
                (operator("+", "-", "+=", "-=") then left)
                    ?: (operator("<", "<=", ">", ">=") then BOOL)
            left?.id == "GameTime" && right(BYTE, INT) ->
                operator("+", "-", "*", "/", "+=", "-=", "*=", "/=") then left
            left?.id == "GameTime" && right(FLOAT) ->
                operator("*", "/", "*=", "/=") then left
            left?.id == "GameTime" && right.isEnum() ->
                operator("+", "-", "+=", "-=") then left

            left?.id == "EngineTime" && right == left ->
                (operator("+", "-", "+=", "-=") then left)
                    ?: (operator("<", "<=", ">", ">=") then BOOL)
            left?.id == "EngineTime" && right(BYTE, INT, FLOAT) ->
                (operator("+", "-", "*", "/", "+=", "-=", "*=", "/=") then left)
                    ?: (operator("<", "<=", ">", ">=") then BOOL)

            left?.id == "Matrix" && right == left ->
                operator("*", "*=") then left

            else -> null
        } ?: when (operator) {
            "=" -> isAssignable(right, left) then left
            "==", "!=" -> with(casts(left, right, CastType.INT_CAST, CastType.FLOAT_CAST)) {
                (leftCast == rightCast || leftCast.isHandle() && rightCast.isHandle()) then BOOL
            }
            "<", "<=", ">", ">=" -> with(casts(left, right, CastType.INT_CAST, CastType.FLOAT_CAST)) {
                both(BYTE, INT, UINT64, FLOAT) then BOOL
            }
            "+" -> with(casts(left, right, CastType.INT_CAST, CastType.FLOAT_CAST, CastType.STRING_CAST)) {
                both(BYTE, INT, UINT64, FLOAT, STRING) then leftCast
            }
            "-", "*", "/" -> with(casts(left, right, CastType.INT_CAST, CastType.FLOAT_CAST)) {
                both(BYTE, INT, UINT64, FLOAT) then leftCast
            }
            "%" -> (left(BYTE, INT) && isAssignable(right, INT)) then BYTE
            "|", "^", "&" -> with(casts(left, right, CastType.INT_CAST)) {
                both(BYTE, INT, UINT64) then leftCast
            }
            "||", "&&" -> (isAssignable(left, BOOL) && isAssignable(right, BOOL)) then BOOL
            "+=" -> (left(BYTE, INT, UINT64, FLOAT, STRING)
                    && isAssignable(getBinaryType("+", left, right), left)) then left
            "-=", "*=", "/=" -> (left(BYTE, INT, UINT64, FLOAT)
                    && isAssignable(getBinaryType("-", left, right), left)) then left
            "|=", "&=" -> (left(BYTE, INT, UINT64)
                    && isAssignable(getBinaryType("|", left, right), left)) then left
            else -> error("Unknown operator: $operator")
        }
    }

    fun getUnaryType(operator: String, operand: Type?): Type? {
        return if (operand?.id in listOf("Vector", "GameTime") && operator("-")) operand
        else when (operator) {
            "-" -> (operand(FLOAT) then FLOAT) ?: (isAssignable(operand, INT) then INT)
            "~" -> isAssignable(operand, INT) then INT
            "!" -> isAssignable(operand, BOOL) then BOOL
            else -> error("Unknown operator: $operator")
        }
    }

    private fun casts(left: Type?, right: Type?, vararg types: CastType): TypePair {
        if (left == null || right == null) {
            return TypePair(left, right)
        }
        if (types.contains(CastType.INT_CAST)) {
            when {
                left(BYTE) && (right(INT) || right.isEnum()) -> return TypePair(BYTE, BYTE)
                left(INT) && (right(BYTE) || right.isEnum()) -> return TypePair(INT, INT)
                left.isEnum() && (right(BYTE, INT) || right == left) -> return TypePair(INT, INT)
            }
        }
        if (types.contains(CastType.FLOAT_CAST)) {
            when {
                left(FLOAT) && isAssignable(right, FLOAT) -> return TypePair(FLOAT, FLOAT)
                isAssignable(left, FLOAT) && right(FLOAT) -> return TypePair(FLOAT, FLOAT)
            }
        }
        if (types.contains(CastType.STRING_CAST)) {
            when {
                left(NAME) && right(NAME) -> return TypePair(STRING, STRING)
                left.isHandle() && right.isHandle() && (isOrExtends(left.resolved!!, right.resolved!!)
                        || isOrExtends(right.resolved!!, left.resolved!!)) -> return TypePair(STRING, STRING)
                left(BOOL) && isAssignable(right, BOOL) -> return TypePair(STRING, STRING)
                isAssignable(left, BOOL) && right(BOOL) -> return TypePair(STRING, STRING)
                left(STRING) && isAssignable(right, STRING) -> return TypePair(STRING, STRING)
                isAssignable(left, STRING) && right(STRING) -> return TypePair(STRING, STRING)
            }
        }
        return TypePair(left, right)
    }

    private infix fun <T> Boolean.then(trueValue: T): T? = if (this) trueValue else null

    private operator fun Type?.invoke(vararg types: Type): Boolean = this in types

    private operator fun String?.invoke(vararg options: String): Boolean = this in options

    private data class TypePair(
        val leftCast: Type?,
        val rightCast: Type?
    ) {
        fun both(vararg types: Type): Boolean = leftCast == rightCast && leftCast in types
    }

    private enum class CastType {
        INT_CAST, FLOAT_CAST, STRING_CAST
    }
}
