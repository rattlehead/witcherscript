package com.rattlehead.witcherscript.service

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil
import com.rattlehead.witcherscript.WSNativeLibrary
import com.rattlehead.witcherscript.common.AccessModifier
import com.rattlehead.witcherscript.index.WSConstructIndex
import com.rattlehead.witcherscript.psi.*

@Suppress("unused", "MemberVisibilityCanBePrivate")
object ConstructService {
    fun find(project: Project, id: String): WSConstruct? {
        return WSConstructIndex.findAll(project, id)
            .sortedBy(WSNativeLibrary::isNative)
            .firstOrNull()
    }

    fun findClass(project: Project, id: String): WSClass? = find(project, id) as? WSClass

    fun findState(project: Project, id: String): WSState? = find(project, id) as? WSState

    fun findStruct(project: Project, id: String): WSStruct? = find(project, id) as? WSStruct

    fun findEnum(project: Project, id: String): WSEnum? = find(project, id) as? WSEnum

    fun get(context: PsiElement): WSConstruct? {
        return PsiTreeUtil.getNonStrictParentOfType(context, WSConstruct::class.java)
    }

    fun getClass(context: PsiElement): WSClass? = get(context) as? WSClass

    fun getState(context: PsiElement): WSState? = get(context) as? WSState

    fun getStruct(context: PsiElement): WSStruct? = get(context) as? WSStruct

    fun getEnum(context: PsiElement): WSEnum? = get(context) as? WSEnum

    fun findSuper(construct: WSConstruct): WSConstruct? {
        return with(construct) { when (this) {
            is WSClass -> findClass(project, parentName ?: "CObject")
                ?: findStruct(project, parentName!!)
            is WSState -> when (parentName) {
                null -> findClass(project, "CScriptableState")
                else -> generateSequence(findClass(project, ownerName ?: return null), ::findSuper)
                    .mapNotNull { findState(project, "${it.id}State${parentName}") }
                    .firstOrNull()
            }
            else -> null
        }}
    }

    fun findDuplicates(construct: WSConstruct): List<WSConstruct> {
        val id = construct.id ?: return emptyList()
        val native = findNative(construct)
        val isImport = when (construct) {
            is WSClass -> construct.isImport
            is WSState -> construct.isImport
            is WSStruct -> construct.isImport
            else -> false
        }
        return WSConstructIndex.findAll(construct.project, id)
            .filter { !it.isEquivalentTo(construct) && !(it.isEquivalentTo(native) && isImport) }
    }

    fun findNative(construct: WSConstruct): WSConstruct? {
        val id = construct.id ?: return null
        return WSConstructIndex.findAll(construct.project, id)
            .find(WSNativeLibrary::isNative)
            ?.also {
                if (construct.javaClass != it.javaClass) {
                    return null
                }
            }
    }

    fun isOrExtends(supposedChild: WSConstruct, supposedParent: WSConstruct): Boolean {
        return generateSequence(supposedChild, ::findSuper)
            .dropWhile { !it.isEquivalentTo(supposedParent) }
            .any()
    }

    fun isStateOfClass(supposedState: WSConstruct, supposedClass: WSConstruct): Boolean {
        return supposedState is WSState && supposedClass is WSClass
                && findClass(supposedState.project, supposedState.ownerName ?: return false)
                    ?.let { clazz -> isOrExtends(clazz, supposedClass) } ?: false
    }

    fun isSufficientAccess(context: PsiElement, target: Modifiable): Boolean {
        val modifier = when (target) {
            is WSProperty -> target.accessModifier
            is WSFunction -> target.accessModifier
            else -> return true
        }
        val construct = get(context) ?: return true
        val tarConstruct = get(target) ?: return true
        return when (modifier) {
            AccessModifier.PUBLIC, null -> true
            AccessModifier.PROTECTED -> isOrExtends(construct, tarConstruct) || isStateOfClass(construct, tarConstruct)
            AccessModifier.PRIVATE -> construct.isEquivalentTo(tarConstruct)
        }
    }
}