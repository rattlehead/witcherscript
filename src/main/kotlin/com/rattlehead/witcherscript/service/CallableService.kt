package com.rattlehead.witcherscript.service

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil
import com.rattlehead.witcherscript.WSNativeLibrary
import com.rattlehead.witcherscript.index.WSCallableIndex
import com.rattlehead.witcherscript.psi.*

@Suppress("unused", "MemberVisibilityCanBePrivate")
object CallableService {
    fun findGlobal(project: Project, name: String): WSFunction? {
        return findAllGlobal(project, name)
            .firstOrNull()
    }

    fun findMember(context: PsiElement, name: String): WSCallable? {
        return findAllMember(context, name)
            .firstOrNull()
    }

    fun findSuper(context: PsiElement, name: String): WSCallable? {
        return ConstructService.findSuper(ConstructService.get(context) ?: return null)
            ?.let { findMember(it, name) }
    }

    fun findArrayFunction(project: Project, name: String): WSCallable? {
        return WSCallableIndex.findAll(project, name, "__array_methods")
            .firstOrNull()
    }

    fun findDuplicates(callable: WSCallable): List<WSCallable> {
        val name = callable.name ?: return emptyList()
        return when (val construct = ConstructService.get(callable)) {
            null -> findAllGlobal(callable.project, name)
            else -> WSCallableIndex.findAll(callable.project, name, construct.id ?: return emptyList()).asSequence()
        }.filter { !it.isEquivalentTo(callable) && !(callable is WSFunction && WSNativeLibrary.isNative(it)) }.toList()
    }

    fun findNative(function: WSFunction): WSFunction? {
        val name = function.name ?: return null
        return when (val construct = ConstructService.get(function)) {
            null -> findAllGlobal(function.project, name)
            else -> findAllMember(construct, name)
        }.find(WSNativeLibrary::isNative) as? WSFunction
    }

    fun get(context: PsiElement): WSCallable? {
        return PsiTreeUtil.getNonStrictParentOfType(context, WSCallable::class.java)
    }

    fun getParameters(callable: WSCallable): List<WSParameter> {
        return callable.parameters.parameterListList.flatMap { it.parameterList }
    }

    private fun findAllGlobal(project: Project, name: String): Sequence<WSFunction> {
        return WSCallableIndex.findAll(project, name)
            .map { it as WSFunction }
            .sortedBy(WSNativeLibrary::isNative)
            .asSequence()
    }

    private fun findAllMember(context: PsiElement, name: String): Sequence<WSCallable> {
        return generateSequence(ConstructService.get(context), ConstructService::findSuper)
            .flatMap {
                WSCallableIndex.findAll(context.project, name, it.id ?: return@flatMap emptyList())
                    .sortedBy(WSNativeLibrary::isNative)
            }
    }
}