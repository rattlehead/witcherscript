package com.rattlehead.witcherscript;

import com.intellij.lang.Language;

public class WSLanguage extends Language {

    public static final WSLanguage INSTANCE = new WSLanguage();

    private WSLanguage() {
        super("WitcherScript");
    }
}
