package com.rattlehead.witcherscript;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NotNull;

import javax.swing.Icon;

public class WSLanguageFileType extends LanguageFileType {

    public static final WSLanguageFileType INSTANCE = new WSLanguageFileType();

    private static final Icon ICON = IconLoader.getIcon("/icons/witcherscript.svg", WSLanguageFileType.class);

    private WSLanguageFileType() {
        super(WSLanguage.INSTANCE);
    }

    @Override
    public @NotNull String getName() {
        return "WitcherScript File";
    }

    @Override
    public @NotNull String getDescription() {
        return "Witcher 3 script file";
    }

    @Override
    public @NotNull String getDefaultExtension() {
        return "ws";
    }

    @Override
    public Icon getIcon() {
        return ICON;
    }
}
