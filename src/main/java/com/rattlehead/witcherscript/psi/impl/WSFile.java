package com.rattlehead.witcherscript.psi.impl;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.rattlehead.witcherscript.WSLanguage;
import com.rattlehead.witcherscript.WSLanguageFileType;
import org.jetbrains.annotations.NotNull;

public class WSFile extends PsiFileBase {

    public WSFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, WSLanguage.INSTANCE);
    }

    @Override
    public @NotNull FileType getFileType() {
        return WSLanguageFileType.INSTANCE;
    }
}
