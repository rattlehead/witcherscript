package com.rattlehead.witcherscript.psi.type;

import com.intellij.psi.tree.IElementType;
import com.rattlehead.witcherscript.WSLanguage;
import org.jetbrains.annotations.NotNull;

public class WSElementType extends IElementType {

    public WSElementType(@NotNull String debugName) {
        super(debugName, WSLanguage.INSTANCE);
    }
}
