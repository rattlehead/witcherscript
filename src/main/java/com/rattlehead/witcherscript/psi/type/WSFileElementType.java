package com.rattlehead.witcherscript.psi.type;

import com.intellij.psi.stubs.PsiFileStub;
import com.intellij.psi.tree.IStubFileElementType;
import com.rattlehead.witcherscript.WSLanguage;
import com.rattlehead.witcherscript.psi.impl.WSFile;
import org.jetbrains.annotations.NotNull;

public class WSFileElementType extends IStubFileElementType<PsiFileStub<WSFile>> {

    public static final WSFileElementType INSTANCE = new WSFileElementType();

    public WSFileElementType() {
        super(WSLanguage.INSTANCE);
    }

    @Override
    public @NotNull String getExternalId() {
        return "WitcherScript.FILE";
    }
}
