package com.rattlehead.witcherscript.psi.type;

import com.intellij.psi.stubs.IStubElementType;

public interface WSStubTypes {
    IStubElementType<?, ?> CLASS = (IStubElementType<?, ?>) WSTypes.CLASS;
    IStubElementType<?, ?> STATE = (IStubElementType<?, ?>) WSTypes.STATE;
    IStubElementType<?, ?> STRUCT = (IStubElementType<?, ?>) WSTypes.STRUCT;
    IStubElementType<?, ?> ENUM = (IStubElementType<?, ?>) WSTypes.ENUM;
    IStubElementType<?, ?> FUNCTION = (IStubElementType<?, ?>) WSTypes.FUNCTION;
    IStubElementType<?, ?> EVENT = (IStubElementType<?, ?>) WSTypes.EVENT;
    IStubElementType<?, ?> FIELD = (IStubElementType<?, ?>) WSTypes.FIELD;
    IStubElementType<?, ?> AUTOBIND = (IStubElementType<?, ?>) WSTypes.AUTOBIND;
    IStubElementType<?, ?> ENUM_VALUE = (IStubElementType<?, ?>) WSTypes.ENUM_VALUE;
}
