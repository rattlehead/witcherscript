package com.rattlehead.witcherscript.parser;

import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import com.rattlehead.witcherscript.lexer.WSLexerAdapter;
import com.rattlehead.witcherscript.psi.impl.WSFile;
import com.rattlehead.witcherscript.psi.type.WSTypes;
import com.rattlehead.witcherscript.psi.type.WSFileElementType;
import org.jetbrains.annotations.NotNull;

import static com.rattlehead.witcherscript.psi.type.WSTypes.*;

@SuppressWarnings("TokenSetInParserDefinition")
public class WSParserDefinition implements ParserDefinition {

    private static final TokenSet COMMENTS = TokenSet.create(BLOCK_COMMENT, LINE_COMMENT);
    private static final TokenSet STRING_LITERALS = TokenSet.create(STRING_LITERAL, NAME_LITERAL);

    @Override
    public @NotNull Lexer createLexer(Project project) {
        return new WSLexerAdapter();
    }

    @Override
    public @NotNull PsiParser createParser(Project project) {
        return new WSParser();
    }

    @Override
    public @NotNull IFileElementType getFileNodeType() {
        return WSFileElementType.INSTANCE;
    }

    @Override
    public @NotNull TokenSet getCommentTokens() {
        return COMMENTS;
    }

    @Override
    public @NotNull TokenSet getStringLiteralElements() {
        return STRING_LITERALS;
    }

    @Override
    public @NotNull PsiElement createElement(ASTNode astNode) {
        return WSTypes.Factory.createElement(astNode);
    }

    @Override
    public @NotNull PsiFile createFile(@NotNull FileViewProvider fileViewProvider) {
        return new WSFile(fileViewProvider);
    }
}
