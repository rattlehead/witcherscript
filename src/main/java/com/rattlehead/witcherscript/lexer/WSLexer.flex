package com.rattlehead.witcherscript.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static com.rattlehead.witcherscript.psi.type.WSTypes.*;

%%

%{
  public WSLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class WSLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

INT_LITERAL=[0-9]+|0x[0-9A-Fa-f]+
FLOAT_LITERAL=[0-9]+\.[0-9]*f?|\.[0-9]+f?
BOOL_LITERAL=true|false
STRING_LITERAL=\"([^\"]|\\\")*\"
NAME_LITERAL='([^']|\\')*'
WHITE_SPACE=[ \t\n\x0B\f\r]+
LINE_COMMENT="//".*
BLOCK_COMMENT="/"\*([^*]|\*[^/])*?\*"/"
ID=[a-zA-Z_][a-zA-Z_0-9]*

%%
<YYINITIAL> {
  "Int8"                     { return T_INT8; }
  "Int16"                    { return T_INT16; }
  "Int32"                    { return T_INT32; }
  "Uint8"                    { return T_UINT8; }
  "Uint16"                   { return T_UINT16; }
  "Uint32"                   { return T_UINT32; }
  "Uint64"                   { return T_UINT64; }
  "Float"                    { return T_FLOAT; }
  "Bool"                     { return T_BOOL; }
  "CName"                    { return T_NAME; }
  "String"                   { return T_STRING; }
  "EngineQsTransform"        { return T_ENGINE_QS_TRANSFORM; }
  "EntityHandle"             { return T_ENTITY_HANDLE; }
  "TagList"                  { return T_TAG_LIST; }
  "CGUID"                    { return T_GUID; }
  "CDateTime"                { return T_DATE_TIME; }
  "CPhysicalCollision"       { return T_PHYSICAL_COLLISION; }
  "EngineTransform"          { return T_ENGINE_TRANSFORM; }
  "IdTag"                    { return T_ID_TAG; }
  "LocalizedString"          { return T_LOCALIZED_STRING; }
  "SharedDataBuffer"         { return T_SHARED_DATA_BUFFER; }
  "DeferredDataBuffer"       { return T_DEFERRED_DATA_BUFFER; }
  "StringAnsi"               { return T_STRING_ANSI; }
  "theGame"                  { return VAR_GAME; }
  "thePlayer"                { return VAR_PLAYER; }
  "theCamera"                { return VAR_CAMERA; }
  "theSound"                 { return VAR_SOUND; }
  "theDebug"                 { return VAR_DEBUG; }
  "theTimer"                 { return VAR_TIMER; }
  "theInput"                 { return VAR_INPUT; }
  "theTelemetry"             { return VAR_TELEMETRY; }
  "class"                    { return KW_CLASS; }
  "struct"                   { return KW_STRUCT; }
  "state"                    { return KW_STATE; }
  "enum"                     { return KW_ENUM; }
  "function"                 { return KW_FUNCTION; }
  "event"                    { return KW_EVENT; }
  "var"                      { return KW_VAR; }
  "autobind"                 { return KW_AUTOBIND; }
  "hint"                     { return KW_HINT; }
  "defaults"                 { return KW_DEFAULTS; }
  "byte"                     { return KW_BYTE; }
  "int"                      { return KW_INT; }
  "float"                    { return KW_FLOAT; }
  "bool"                     { return KW_BOOL; }
  "string"                   { return KW_STRING; }
  "name"                     { return KW_NAME; }
  "array"                    { return KW_ARRAY; }
  "void"                     { return KW_VOID; }
  "import"                   { return KW_IMPORT; }
  "public"                   { return KW_PUBLIC; }
  "protected"                { return KW_PROTECTED; }
  "private"                  { return KW_PRIVATE; }
  "abstract"                 { return KW_ABSTRACT; }
  "statemachine"             { return KW_STATEMACHINE; }
  "final"                    { return KW_FINAL; }
  "latent"                   { return KW_LATENT; }
  "exec"                     { return KW_EXEC; }
  "storyscene"               { return KW_STORYSCENE; }
  "reward"                   { return KW_REWARD; }
  "cleanup"                  { return KW_CLEANUP; }
  "quest"                    { return KW_QUEST; }
  "entry"                    { return KW_ENTRY; }
  "timer"                    { return KW_TIMER; }
  "const"                    { return KW_CONST; }
  "inlined"                  { return KW_INLINED; }
  "editable"                 { return KW_EDITABLE; }
  "saved"                    { return KW_SAVED; }
  "optional"                 { return KW_OPTIONAL; }
  "out"                      { return KW_OUT; }
  "extends"                  { return KW_EXTENDS; }
  "in"                       { return KW_IN; }
  "any"                      { return KW_ANY; }
  "single"                   { return KW_SINGLE; }
  "this"                     { return KW_THIS; }
  "virtual_parent"           { return EXT_VIRTUAL_PARENT; }
  "parent"                   { return KW_PARENT; }
  "super"                    { return KW_SUPER; }
  "if"                       { return KW_IF; }
  "else"                     { return KW_ELSE; }
  "for"                      { return KW_FOR; }
  "do"                       { return KW_DO; }
  "while"                    { return KW_WHILE; }
  "switch"                   { return KW_SWITCH; }
  "case"                     { return KW_CASE; }
  "default"                  { return KW_DEFAULT; }
  "break"                    { return KW_BREAK; }
  "continue"                 { return KW_CONTINUE; }
  "delete"                   { return KW_DELETE; }
  "return"                   { return KW_RETURN; }
  "new"                      { return KW_NEW; }
  "."                        { return DOT; }
  ","                        { return COMMA; }
  ":"                        { return COLON; }
  ";"                        { return SEMICOLON; }
  "?"                        { return QUESTION; }
  "("                        { return L_PARENTHESIS; }
  ")"                        { return R_PARENTHESIS; }
  "{"                        { return L_BRACE; }
  "}"                        { return RL_BRACE; }
  "["                        { return L_BRACKET; }
  "]"                        { return R_BRACKET; }
  "+="                       { return ASSIGN_PLUS; }
  "-="                       { return ASSIGN_MINUS; }
  "*="                       { return ASSIGN_MULTIPLY; }
  "/="                       { return ASSIGN_DIVIDE; }
  "&="                       { return ASSIGN_AND; }
  "|="                       { return ASSIGN_OR; }
  "&&"                       { return AND; }
  "||"                       { return OR; }
  "=="                       { return EQUALS; }
  "!="                       { return NOT_EQUALS; }
  "<="                       { return LESS_EQUALS; }
  ">="                       { return MORE_EQUALS; }
  "<"                        { return LESS; }
  ">"                        { return MORE; }
  "+"                        { return PLUS; }
  "-"                        { return MINUS; }
  "*"                        { return MULTIPLY; }
  "/"                        { return DIVIDE; }
  "%"                        { return MOD; }
  "!"                        { return NOT; }
  "&"                        { return B_AND; }
  "|"                        { return B_OR; }
  "^"                        { return B_XOR; }
  "~"                        { return B_NOT; }
  "="                        { return ASSIGN; }
  "NULL"                     { return NULL_LITERAL; }

  {INT_LITERAL}              { return INT_LITERAL; }
  {FLOAT_LITERAL}            { return FLOAT_LITERAL; }
  {BOOL_LITERAL}             { return BOOL_LITERAL; }
  {STRING_LITERAL}           { return STRING_LITERAL; }
  {NAME_LITERAL}             { return NAME_LITERAL; }
  {WHITE_SPACE}              { return WHITE_SPACE; }
  {LINE_COMMENT}             { return LINE_COMMENT; }
  {BLOCK_COMMENT}            { return BLOCK_COMMENT; }
  {ID}                       { return ID; }

}

[^] { return BAD_CHARACTER; }
