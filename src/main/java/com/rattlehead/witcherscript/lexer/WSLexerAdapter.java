package com.rattlehead.witcherscript.lexer;

import com.intellij.lexer.FlexAdapter;

public class WSLexerAdapter extends FlexAdapter {

    public WSLexerAdapter() {
        super(new WSLexer());
    }
}
