package com.rattlehead.witcherscript;

import com.intellij.openapi.util.Condition;
import com.intellij.openapi.vfs.VirtualFile;

public class WSProblemFileHighlightFilter implements Condition<VirtualFile> {

    @Override
    public boolean value(VirtualFile virtualFile) {
        return virtualFile.getFileType() == WSLanguageFileType.INSTANCE;
    }
}
