enum EAIAttitude
{
    AIA_Friendly, AIA_Neutral, AIA_Hostile
}
enum EActorActionType
{
    ActorAction_None, ActorAction_Moving, ActorAction_Rotating, ActorAction_Animation = 4, ActorAction_RaiseEvent = 8,
    ActorAction_Sliding = 16, ActorAction_Working = 32, ActorAction_ChangeEmotion = 64, ActorAction_Exploration = 128,
    ActorAction_UseDevice = 256, ActorAction_DynamicMoving = 512, ActorAction_MovingOnCurve = 1024,
    ActorAction_CustomSteer = 2048, ActorAction_R4Reserved_PC = 4096, ActorAction_MovingOutNavdata = 8192
}
enum EMoveType
{
    MT_Walk, MT_Run, MT_FastRun, MT_Sprint, MT_AbsSpeed
}
enum EMountType
{
    MT_normal = 1, MT_instant,MT_fromScript = 1024
}
enum EBTNodeStatus
{
    BTNS_Active, BTNS_Failed, BTNS_Completed
}
enum EBaseCharacterStats
{
    BCS_Vitality, BCS_Essence, BCS_Stamina, BCS_Toxicity, BCS_Focus, BCS_Morale, BCS_Air, BCS_Panic, BCS_PanicStatic,
    BCS_SwimmingStamina, BCS_Undefined
}
enum ECharacterDefenseStats
{
    CDS_None, CDS_PhysicalRes, CDS_BleedingRes, CDS_PoisonRes, CDS_FireRes, CDS_FrostRes, CDS_ShockRes, CDS_ForceRes,
    CDS_FreezeRes, CDS_WillRes, CDS_BurningRes, CDS_SlashingRes, CDS_PiercingRes, CDS_BludgeoningRes, CDS_RendingRes,
    CDS_ElementalRes, CDS_DoTBurningDamageRes, CDS_DoTPoisonDamageRes, CDS_DoTBleedingDamageRes
}
enum ELookAtMode
{
    LM_Dialog = 1, LM_Cutscene, LM_MiniGame = 4, LM_GameplayLock = 8
}
enum EAttackDirection
{
    AD_Front, AD_Left, AD_Right, AD_Back
}
enum EAttackDistance
{
    ADIST_Small, ADIST_Medium, ADIST_Large, ADIST_None = 4
}
enum EWoundTypeFlags
{
    WTF_None, WTF_Cut, WTF_Explosion, WTF_Frost = 4, WTF_All = 7
}
enum ETickGroup
{
    TICK_PrePhysics, TICK_PrePhysicsPost, TICK_Main, TICK_PostPhysics, TICK_PostPhysicsPost, TICK_PostUpdateTransform
}
enum EPersistanceMode
{
    PM_DontPersist, PM_SaveStateOnly, PM_Persist
}
enum EShowFlags
{
    SHOW_Meshes, SHOW_AI, SHOW_AISenses, SHOW_AIRanges, SHOW_Camera, SHOW_HierarchicalGrid, SHOW_NavMesh, SHOW_Shadows,
    SHOW_ForceAllShadows, SHOW_Lights, SHOW_Terrain, SHOW_Foliage, SHOW_PostProcess, SHOW_Selection, SHOW_Grid,
    SHOW_Sprites, SHOW_Particles, SHOW_Wireframe, SHOW_WalkSurfaces, SHOW_Areas, SHOW_Locomotion, SHOW_MovableRep,
    SHOW_Logic, SHOW_Profilers, SHOW_Paths, SHOW_Stickers, SHOW_Steering, SHOW_Bboxes, SHOW_Exploration, SHOW_Behavior,
    SHOW_Emissive, SHOW_Spawnset, SHOW_Collision, SHOW_GUI, SHOW_VisualDebug, SHOW_Decals, SHOW_Lighting,
    SHOW_TSLighting, SHOW_Refraction, SHOW_Encounter, SHOW_BboxesParticles, SHOW_BboxesTerrain, SHOW_NavRoads,
    SHOW_Brushes, SHOW_BuildingBrush, SHOW_Sound, SHOW_Gizmo, SHOW_Flares, SHOW_FlaresData, SHOW_GUIFallback,
    SHOW_OnScreenMessages, SHOW_DepthOfField, SHOW_LightsBBoxes, SHOW_ErrorState, SHOW_Devices, SHOW_SelectionContacts,
    SHOW_PEObstacles, SHOW_Bg, SHOW_TriangleStats, SHOW_Formations, SHOW_Skirt, SHOW_Wind, SHOW_NonClimbable,
    SHOW_MergedMeshes, SHOW_UseVisibilityMask, SHOW_ShadowMeshDebugMesh, SHOW_ShadowMeshDebugText,
    SHOW_ShadowsFromMeshes, SHOW_ShadowsFromMerged, SHOW_NavTerrain, SHOW_NavGraph, SHOW_NavObstacles, SHOW_Apex,
    SHOW_SpeedTreeShadows, SHOW_ShadowStats, SHOW_ShadowPreviewDynamic, SHOW_ShadowPreviewStatic,
    SHOW_PhysXVisualization, SHOW_PhysXTraceVisualization, SHOW_PhysXPlatforms, SHOW_PhysXMaterials, SHOW_BboxesDecals,
    SHOW_BboxesSmallestHoleOverride, SHOW_SpritesDecals, SHOW_HiResEntityShadows, SHOW_TAATransparencyMask,
    __SHOW_Apex_Enable_Visualizations, SHOW_ApexClothCollisionSolid, SHOW_ApexClothSkeleton, SHOW_ApexClothBackstop,
    SHOW_ApexClothMaxDistance, SHOW_ApexClothVelocity, SHOW_ApexClothSkinnedPosition, SHOW_ApexClothActiveTethers,
    SHOW_ApexClothLength, SHOW_ApexClothCrossSection, SHOW_ApexClothBending, SHOW_ApexClothShearing,
    SHOW_ApexClothZeroStretch, SHOW_ApexClothSelfCollision, SHOW_ApexClothVirtualCollision,
    SHOW_ApexClothPhysicsMeshSolid, SHOW_ApexClothPhysicsMeshWire, SHOW_ApexClothWind, SHOW_ApexClothBackstopPrecise,
    SHOW_ApexClothCollisionWire, SHOW_ApexClothLocalSpace, SHOW_ApexDestructSupport, SHOW_ApexStats, SHOW_DistantLights,
    SHOW_CollisionIfNotVisible, SHOW_Projectile, SHOW_EnvProbesInstances, SHOW_TerrainHoles,
    SHOW_CollisionSoundOcclusion, SHOW_Dimmers, SHOW_DimmersBBoxes, SHOW_PhysActorDampers, SHOW_PhysActorVelocities,
    SHOW_PhysActorMasses, SHOW_TBN, SHOW_TonemappingCurve, SHOW_ApexFractureRatio, SHOW_AnimatedProperties,
    SHOW_PhysMotionIntensity, SHOW_AreaShapes, SHOW_TriggerBounds, SHOW_TriggerActivators, SHOW_TriggerTree,
    SHOW_SoundReverb, SHOW_CurveAnimations, SHOW_AnimDangles, SHOW_Wetness, SHOW_CameraVisibility, SHOW_StreamingTree0,
    SHOW_StreamingTree1, SHOW_StreamingTree2, SHOW_StreamingTree3, SHOW_StreamingTree4, SHOW_StreamingTree5,
    SHOW_StreamingTree6, SHOW_StreamingTree7, SHOW_EnvProbeStats, SHOW_FocusMode, SHOW_AIBehTree, SHOW_NavMeshTriangles,
    SHOW_NavGraphNoOcclusion, SHOW_PhantomShapes, SHOW_Crowd, SHOW_SoundEmitters, SHOW_SoundAmbients, SHOW_GenericGrass,
    SHOW_ApexFracturePoints, SHOW_ApexThresoldLeft, SHOW_Underwater, SHOW_Scenes, SHOW_BoatSailing, SHOW_OcclusionStats,
    SHOW_UmbraFrustum, SHOW_UmbraObjectBounds, SHOW_UmbraPortals, SHOW_UmbraVisibleVolume, SHOW_UmbraViewCell,
    SHOW_UmbraVisibilityLines, SHOW_UmbraStatistics, SHOW_UmbraOcclusionBuffer, SHOW_UmbraCullShadows,
    SHOW_UmbraCullSpeedTreeShadows, SHOW_UmbraShowOccludedNonStaticGeometry, SHOW_UmbraStreamingVisualization,
    SHOW_UmbraCullTerrain, SHOW_UmbraCullFoliage, SHOW_UmbraShowFoliageCells, SHOW_DynamicDecals, SHOW_Swarms,
    SHOW_TerrainStats, SHOW_EnvProbesOverlay, SHOW_Interactions, SHOW_AIActions, SHOW_Stripes, SHOW_Waypoints,
    SHOW_RoadFollowing, SHOW_MapTracking, SHOW_QuestMapPins, SHOW_FlareOcclusionShapes, SHOW_ReversedProjection,
    SHOW_CascadesStabilizedRotation, SHOW_DynamicCollector, SHOW_DynamicComponent, SHOW_MeshComponent, SHOW_LodInfo,
    SHOW_NavMeshOverlay, SHOW_ActorLodInfo, SHOW_LocalReflections, SHOW_EnvProbesBigOverlay, SHOW_VideoOutputLimited,
    SHOW_BoatHedgehog, SHOW_BoatDestruction, SHOW_BoatBuoyancy, SHOW_BoatPathFollowing, SHOW_Histogram,
    SHOW_VolumetricPaths, SHOW_PigmentMap, SHOW_PreferTemporalAA, SHOW_AllowDebugPreviewTemporalAA,
    SHOW_AllowApexShadows, SHOW_NodeTags, SHOW_HairAndFur, SHOW_ForwardPass, SHOW_GeometrySkinned, SHOW_GeometryStatic,
    SHOW_GeometryProxies, SHOW_NavGraphRegions, SHOW_Containers, SHOW_RenderTargetBackground, SHOW_AITickets,
    SHOW_EffectAreas, SHOW_PathSpeedValues, SHOW_PhysActorIterations, SHOW_BoatInput, SHOW_Dismemberment,
    SHOW_EntityVisibility, SHOW_AIBehaviorDebug, SHOW_XB1SafeArea, SHOW_PS4SafeArea, SHOW_Skybox, SHOW_CameraLights,
    SHOW_AntiLightbleed, SHOW_BoatWaterProbbing, SHOW_CommunityAgents, SHOW_RenderDeepTerrain,
    SHOW_CullTerrainWithFullHeight, SHOW_CameraInteriorFactor, SHOW_BrowseDebugPreviews, SHOW_BBoxesCloth,
    SHOW_BBoxesFur, SHOW_BBoxesDestruction, SHOW_PhysActorFloatingRatio, SHOW_GameplayPostFx, SHOW_SimpleBuoyancy,
    SHOW_ScaleformMemoryInfo, SHOW_GameplayLightComponent, SHOW_TeleportDetector, SHOW_SoundListener,
    SHOW_NavTerrainHeight, SHOW_StreamingCollisionBoxes, SHOW_RenderGPUProfiler, SHOW_TemplateLoadBalancer,
    SHOW_DistantLightsDebug, SHOW_GrassHeightMap, SHOW_GrassGenPoints
}
enum ECheats
{
    CHEAT_Console, CHEAT_FreeCamera, CHEAT_DebugPages, CHEAT_InstantKill, CHEAT_Teleport, CHEAT_MovementOnPhysics,
    CHEAT_TimeScaling
}
enum EEntityGameplayEffectFlags
{
    EGEF_FocusModeHighlight = 1, EGEF_CatViewHiglight
}
enum EInputKey
{
    IK_None, IK_LeftMouse, IK_RightMouse, IK_MiddleMouse, IK_Unknown04, IK_Unknown05, IK_Unknown06, IK_Unknown07,
    IK_Backspace, IK_Tab, IK_Unknown0A, IK_Unknown0B, IK_Unknown0C, IK_Enter, IK_Unknown0E, IK_Unknown0F, IK_Shift,
    IK_Ctrl, IK_Alt, IK_Pause, IK_CapsLock, IK_Unknown15, IK_Unknown16, IK_Unknown17, IK_Unknown18, IK_Unknown19,
    IK_Unknown1A, IK_Escape, IK_Unknown1C, IK_Unknown1D, IK_Unknown1E, IK_Unknown1F, IK_Space, IK_PageUp, IK_PageDown,
    IK_End, IK_Home, IK_Left, IK_Up, IK_Right, IK_Down, IK_Select, IK_Print, IK_Execute, IK_PrintScrn, IK_Insert,
    IK_Delete, IK_Help, IK_0, IK_1, IK_2, IK_3, IK_4, IK_5, IK_6, IK_7, IK_8, IK_9, IK_Unknown3A, IK_Unknown3B,
    IK_Unknown3C, IK_Unknown3D, IK_Unknown3E, IK_Unknown3F, IK_Unknown40, IK_A, IK_B, IK_C, IK_D, IK_E, IK_F, IK_G,
    IK_H, IK_I, IK_J, IK_K, IK_L, IK_M, IK_N, IK_O, IK_P, IK_Q, IK_R, IK_S, IK_T, IK_U, IK_V, IK_W, IK_X, IK_Y, IK_Z,
    IK_Unknown5B, IK_Unknown5C, IK_Unknown5D, IK_Unknown5E, IK_Unknown5F, IK_NumPad0, IK_NumPad1, IK_NumPad2,
    IK_NumPad3, IK_NumPad4, IK_NumPad5, IK_NumPad6, IK_NumPad7, IK_NumPad8, IK_NumPad9, IK_NumStar, IK_NumPlus,
    IK_Separator, IK_NumMinus, IK_NumPeriod, IK_NumSlash, IK_F1, IK_F2, IK_F3, IK_F4, IK_F5, IK_F6, IK_F7, IK_F8, IK_F9,
    IK_F10, IK_F11, IK_F12, IK_F13, IK_F14, IK_F15, IK_F16, IK_F17, IK_F18, IK_F19, IK_F20, IK_F21, IK_F22, IK_F23,
    IK_F24, IK_Pad_A_CROSS, IK_Pad_B_CIRCLE, IK_Pad_X_SQUARE, IK_Pad_Y_TRIANGLE, IK_Pad_Start, IK_Pad_Back_Select,
    IK_Pad_DigitUp, IK_Pad_DigitDown, IK_Pad_DigitLeft, IK_Pad_DigitRight, IK_Pad_LeftThumb, IK_Pad_RightThumb,
    IK_Pad_LeftShoulder, IK_Pad_RightShoulder, IK_Pad_LeftTrigger, IK_Pad_RightTrigger, IK_Pad_LeftAxisX,
    IK_Pad_LeftAxisY, IK_Pad_RightAxisX, IK_Pad_RightAxisY, IK_NumLock, IK_ScrollLock, IK_Unknown9E, IK_Unknown9F,
    IK_LShift, IK_RShift, IK_LControl, IK_RControl, IK_UnknownA4, IK_UnknownA5, IK_UnknownA6, IK_UnknownA7,
    IK_UnknownA8, IK_UnknownA9, IK_UnknownAA, IK_UnknownAB, IK_UnknownAC, IK_UnknownAD, IK_UnknownAE, IK_UnknownAF,
    IK_UnknownB0, IK_UnknownB1, IK_UnknownB2, IK_UnknownB3, IK_UnknownB4, IK_UnknownB5, IK_UnknownB6, IK_UnknownB7,
    IK_UnknownB8, IK_Unicode, IK_Semicolon, IK_Equals, IK_Comma, IK_Minus, IK_Period, IK_Slash, IK_Tilde, IK_Mouse4,
    IK_Mouse5, IK_Mouse6, IK_Mouse7, IK_Mouse8, IK_UnknownC6, IK_UnknownC7, IK_Joy1, IK_Joy2, IK_Joy3, IK_Joy4, IK_Joy5,
    IK_Joy6, IK_Joy7, IK_Joy8, IK_Joy9, IK_Joy10, IK_Joy11, IK_Joy12, IK_Joy13, IK_Joy14, IK_Joy15, IK_Joy16,
    IK_UnknownD8, IK_UnknownD9, IK_UnknownDA, IK_LeftBracket, IK_Backslash, IK_RightBracket, IK_SingleQuote,
    IK_UnknownDF, IK_UnknownE0, IK_UnknownE1, IK_UnknownE2, IK_UnknownE3, IK_MouseX, IK_MouseY, IK_MouseZ, IK_MouseW,
    IK_JoyU, IK_JoyV, IK_JoySlider1, IK_JoySlider2, IK_MouseWheelUp, IK_MouseWheelDown, IK_UnknownEE, IK_UnknownEF,
    IK_JoyX, IK_JoyY, IK_JoyZ, IK_JoyR, IK_UnknownF4, IK_UnknownF5, IK_Attn, IK_ClearSel, IK_ExSel, IK_ErEof, IK_Play,
    IK_Zoom, IK_NoName, IK_UnknownFD, IK_UnknownFE, IK_PS4_OPTIONS, IK_PS4_TOUCH_PRESS, IK_Last, IK_Count
}
enum EBatchQueryState
{
    BQS_NotFound, BQS_NotReady, BQS_Processed
}
enum ER4TelemetryEvents
{
    TE_STATE_HORSE_RIDING, TE_STATE_SAILING, TE_STATE_AIM_THROW, TE_STATE_COMBAT, TE_STATE_EXPLORING, TE_STATE_DIALOG,
    TE_STATE_SWIMMING, TE_HERO_FAST_TRAVEL, TE_HERO_LEVEL_UP, TE_HERO_EXP_EARNED, TE_HERO_SKILL_POINT_EARNED,
    TE_HERO_SKILL_UP, TE_HERO_CASH_CHANGED, TE_HERO_SPAWNED, TE_HERO_FOCUS_ON, TE_HERO_FOCUS_OFF, TE_HERO_MUTAGEN_USED,
    TE_HERO_ACHIEVEMENT_UNLOCKED, TE_HERO_GWENT_MATCH_STARTED, TE_HERO_GWENT_MATCH_ENDED, TE_HERO_HEALTH_SEGMENT_LOST,
    TE_HERO_HEALTH_SEGMENT_REGAINED, TE_FIGHT_PLAYER_DIES, TE_FIGHT_PLAYER_ATTACKS, TE_FIGHT_PLAYER_USE_SIGN,
    TE_FIGHT_ENEMY_DIES, TE_FIGHT_ENEMY_GETS_HIT, TE_FIGHT_HERO_GETS_HIT, TE_FIGHT_HERO_THROWS_BOMB, TE_ITEM_COOKED,
    TE_ELIXIR_USED, TE_INV_ITEM_EQUIPPED, TE_INV_ITEM_UNEQUIPPED, TE_INV_ITEM_PICKED, TE_INV_ITEM_DROPPED,
    TE_INV_ITEM_SOLD, TE_INV_ITEM_BOUGHT, TE_INV_QUEST_COMPLETED, TE_HERO_MOVEMENT, TE_HERO_POSITION,
    TE_SYS_END_SESISON, TE_SYS_GAME_LOADED, TE_SYS_GAME_SAVED, TE_SYS_GAME_LAUNCHED, TE_SYS_GAME_PAUSE,
    TE_SYS_GAME_UNPAUSE, TE_SYS_GAME_PROGRESS, TE_QUEST_ACTIVATED, TE_QUEST_FINISHED, TE_QUEST_FAILED, TE_UNKNOWN
}
enum ER4CommonStats
{
    CS_VITALITY, CS_TOXICITY, CS_VIGOR, CS_SKILLPOINTS, CS_POSITION_X, CS_POSITION_Y, CS_POSITION_Z, CS_DIFFICULTY_LVL,
    CS_GAME_PROGRESS, CS_MEMORY, CS_FPS, CS_WORLD_ID, CS_GAME_TIME, CS_UNKNOWN
}
enum EMoveFailureAction
{
    MFA_REPLAN, MFA_EXIT
}
enum ESlideRotation
{
    SR_Nearest, SR_Right, SR_Left
}
enum ESkeletonType
{
    Man, Woman, Witcher, Dwarf, Elf, Child, Monster
}
enum EAsyncCheckResult
{
    ASR_InProgress, ASR_ReadyTrue, ASR_ReadyFalse, ASR_Failed
}
enum EArbitratorPriorities
{
    BTAP_Unavailable = -1, BTAP_BelowIdle = 16, BTAP_Idle = 20, BTAP_AboveIdle = 26, BTAP_AboveIdle2 = 31,
    BTAP_Emergency = 50, BTAP_AboveEmergency = 61, BTAP_AboveEmergency2 = 66, BTAP_Combat = 75, BTAP_AboveCombat = 86,
    BTAP_AboveCombat2 = 91, BTAP_FullCutscene = 95
}
enum EPushingDirection
{
    EPD_Front, EPD_Back
}
enum EInteractionPriority
{
    IP_Max_Unpushable = -2, IP_NotSet, IP_Prio_0, IP_Prio_1, IP_Prio_2, IP_Prio_3, IP_Prio_4, IP_Prio_5, IP_Prio_6,
    IP_Prio_7, IP_Prio_8, IP_Prio_9, IP_Prio_10, IP_Prio_11, IP_Prio_12, IP_Prio_13, IP_Prio_14
}
enum EFocusModeVisibility
{
    FMV_None, FMV_Interactive, FMV_Clue
}
enum EGameplayInfoCacheType
{
    GICT_IsInteractive, GICT_HasDrawableComponents, GICT_Custom0, GICT_Custom1, GICT_Custom2, GICT_Custom3, GICT_Custom4
}
enum EPropertyCurveMode
{
    PCM_Forward, PCM_Backward
}
enum EAnimationEventType
{
    AET_Tick, AET_DurationStart, AET_DurationStartInTheMiddle, AET_DurationEnd, AET_Duration
}
enum EVehicleMountType
{
    VMT_None, VMT_ApproachAndMount, VMT_MountIfPossible, VMT_TeleportAndMount, VMT_ImmediateUse
}
enum EVehicleSlot
{
    EVS_driver_slot, EVS_passenger_slot
}
enum ENPCGroupType
{
    ENGT_Enemy, ENGT_Commoner, ENGT_Quest, ENGT_Guard
}
enum EDifficultyMode
{
    EDM_NotSet, EDM_Easy, EDM_Medium, EDM_Hard, EDM_Hardcore
}
enum EVehicleMountStatus
{
    VMS_mountInProgress, VMS_mounted, VMS_dismountInProgress, VMS_dismounted
}
enum ELoadGameResult
{
    LOAD_NotInitialized, LOAD_Initializing, LOAD_ReadyToLoad, LOAD_Loading, LOAD_Error, LOAD_MissingContent
}
enum ESaveGameType
{
    SGT_AutoSave = 1, SGT_QuickSave, SGT_Manual, SGT_ForcedCheckPoint, SGT_CheckPoint
}
enum EQuestManageFastTravelOperation
{
    QMFT_EnableAndShow, QMFT_EnableOnly, QMFT_ShowOnly
}
enum EUsableItemType
{
    UI_Torch, UI_Horn, UI_Bell, UI_OilLamp, UI_Mask, UI_FiendLure, UI_Meteor, UI_None, UI_Censer, UI_Apple, UI_Cookie,
    UI_Basket
}
enum EFinisherSide
{
    FinisherLeft, FinisherRight
}
enum EGlobalEventCategory
{
    GEC_Empty, GEC_Trigger, GEC_Tag, GEC_Fact, GEC_ScriptsCustom0, GEC_ScriptsCustom1, GEC_ScriptsCustom2,
    GEC_ScriptsCustom3, GEC_ScriptsCustom4, GEC_ScriptsCustom5, GEC_ScriptsCustom6, GEC_ScriptsCustom7,
    GEC_ScriptsCustom8, GEC_ScriptsCustom9, GEC_Last
}
enum EGlobalEventType
{
    GET_Unknown, GET_TriggerCreated, GET_TriggerRemoved, GET_TriggerActivatorCreated, GET_TriggerActivatorRemoved,
    GET_TagAdded, GET_TagRemoved, GET_StubTagAdded, GET_StubTagRemoved, GET_FactAdded, GET_FactRemoved,
    GET_ScriptsCustom0, GET_ScriptsCustom1, GET_ScriptsCustom2, GET_ScriptsCustom3
}
enum eQuestObjectiveType
{
    Manual, __Kill_count, __Inventory_Count, __Hunting_List
}
enum eQuestType
{
    Story, Chapter, Side, MonsterHunt, TreasureHunt
}
enum EJournalContentType
{
    EJCT_Vanilla, EJCT_EP1, EJCT_EP2
}
enum ECharacterImportance
{
    __Main_Character, __Side_Character
}
enum EJournalStatus
{
    JS_Inactive, JS_Active, JS_Success, JS_Failed
}
enum ESaveComboStatus
{
    SCO_Deleting = 10, SCO_Local = 20, SCO_Cloud = 30, SCO_Synced = 40, SCO_Conflict = 50, SCO_Uploading = 60,
    SCO_Downloading = 80
}
enum ENewGamePlusStatus
{
    NGP_Success, NGP_Invalid, NGP_CantLoad, NGP_TooOld, NGP_RequirementsNotMet, NGP_InternalError, NGP_ContentRequired,
    NGP_WrongGameVersion
}
enum EDismountType
{
    DT_normal = 1, DT_shakeOff, DT_ragdoll = 4, DT_instant = 8, DT_fromScript = 1024
}
enum eGwintFaction
{
    GwintFaction_Neutral, GwintFaction_NoMansLand, GwintFaction_Nilfgaard, GwintFaction_NothernKingdom,
    GwintFaction_Scoiatael, GwintFaction_Skellige
}
enum EMinigameState
{
    EMS_None = 2, EMS_Init = 4, EMS_Started = 8, EMS_End_PlayerWon = 16, EMS_End_PlayerLost = 32, EMS_End_Error = 64,
    EMS_Wait_PlayerLost = 128, EMS_End_PlayerForfeited = 256, EMS_End = 368
}
enum EGwintAggressionMode
{
    EGAM_Defensive, EGAM_Normal, EGAM_Aggressive, EGAM_VeryAggressive, EGAM_AllIHave
}
enum EGwintDifficultyMode
{
    EGDM_Easy, EGDM_Medium, EGDM_Hard
}
enum EAnimationTrajectorySelectorType
{
    ATST_None, ATST_IK, ATST_Blend2, ATST_Blend3, ATST_Blend2Direction
}
enum EActionMoveAnimationSyncType
{
    AMAST_None, AMAST_CrossBlendIn, AMAST_CrossBlendOut
}
enum ERidingManagerTask
{
    RMT_None, RMT_MountHorse, RMT_DismountHorse, RMT_MountBoat, RMT_DismountBoat
}
enum EExplorationType
{
    ET_Jump, ET_Ladder, ET_Horse_LF, ET_Horse_LB, ET_Horse_L, ET_Horse_R, ET_Horse_RF, ET_Horse_RB, ET_Horse_B,
    ET_Boat_B, ET_Boat_P, ET_Boat_Enter_From_Beach, ET_Fence, ET_Fence_OneSided, ET_Ledge, ET_Boat_Passenger_B
}
enum EAIAreaSelectionMode
{
    EAIASM_Encounter, EAIASM_GuardArea, EAIASM_ByTag, EAIASM_ByTagInEncounter, EAIASM_None
}
enum ECombatTargetSelectionSkipTarget
{
    CTSST_SKIP_ALWAYS, CTSST_SKIP_IF_THERE_ARE_OTHER_TARGETS, CTSST_DONT_SKIP
}
enum ENpcStance
{
    NS_Normal, NS_Strafe, NS_Retreat, NS_Guarded, NS_Wounded, NS_Fly, NS_Swim
}
enum ECombatActionType
{
    CAT_Attack, CAT_SpecialAttack, CAT_Dodge, CAT_Roll, CAT_ItemThrow, CAT_LayItem, CAT_CastSign, CAT_Pirouette,
    CAT_PreAttack, CAT_Parry, CAT_Crossbow, CAT_None2, CAT_CiriDodge
}
enum EInventoryEventType
{
    IET_Empty, IET_ItemAdded, IET_ItemRemoved, IET_ItemQuantityChanged, IET_ItemTagChanged, IET_InventoryRebalanced
}
enum EInventoryItemClass
{
    InventoryItemClass_Common = 1, InventoryItemClass_Magic, InventoryItemClass_Rare, InventoryItemClass_Epic
}
enum ECollisionSides
{
    CS_FRONT, CS_RIGHT, CS_BACK, CS_LEFT, CS_FRONT_LEFT, CS_FRONT_RIGHT, CS_BACK_RIGHT, CS_BACK_LEFT, CS_CENTER
}
enum ECharacterPhysicsState
{
    CPS_Simulated, CPS_Animated, CPS_Falling, CPS_Swimming, CPS_Ragdoll, CPS_Count
}
enum EDestroyWay
{
    Random, Timed, OnContact, OnDistance
}
enum EVehicleType
{
    EVT_Horse, EVT_Boat, EVT_Undefined
}
enum EWorkPlacementImportance
{
    WPI_Anywhere, WPI_Nearby, WPI_Precise
}
enum EBufferActionType
{
    EBAT_EMPTY, EBAT_LightAttack, EBAT_HeavyAttack, EBAT_CastSign, EBAT_ItemUse, EBAT_Parry, EBAT_Dodge,
    EBAT_SpecialAttack_Light, EBAT_SpecialAttack_Heavy, EBAT_Roll, EBAT_Ciri_SpecialAttack,
    EBAT_Ciri_SpecialAttack_Heavy, EBAT_Ciri_Counter, EBAT_Ciri_Dodge, EBAT_Draw_Steel, EBAT_Draw_Silver,
    EBAT_Sheathe_Sword
}
enum EOrientationTarget
{
    OT_Player, OT_Actor, OT_CustomHeading, OT_Camera, OT_CameraOffset, OT_None
}
enum ESpawnTreeSpawnVisibility
{
    STSV_SPAWN_HIDEN, STSV_SPAWN_ALWAYS, STSV_SPAWN_ONLY_VISIBLE
}
enum EDialogActionIcon
{
    DialogAction_NONE = 1, DialogAction_AXII, DialogAction_CONTENT_MISSING = 4, DialogAction_BRIBE = 8,
    DialogAction_HOUSE = 16, DialogAction_PERSUASION = 32, DialogAction_GETBACK = 64, DialogAction_GAME_DICES = 128,
    DialogAction_GAME_FIGHT = 256, DialogAction_GAME_WRESTLE = 512, DialogAction_CRAFTING = 1024,
    DialogAction_SHOPPING = 2048, DialogAction_TimedChoice = 4096, DialogAction_EXIT = 8192,
    DialogAction_HAIRCUT = 16384, DialogAction_MONSTERCONTRACT = 32768, DialogAction_BET = 65536,
    DialogAction_STORAGE = 131072, DialogAction_GIFT = 262144, DialogAction_GAME_DRINK = 524288,
    DialogAction_GAME_DAGGER = 1048576, DialogAction_SMITH = 2097152, DialogAction_ARMORER = 4194304,
    DialogAction_RUNESMITH = 8388608, DialogAction_TEACHER = 16777216, DialogAction_FAST_TRAVEL = 33554432,
    DialogAction_GAME_CARDS = 67108864, DialogAction_SHAVING = 134217728, DialogAction_AUCTION = 268435456,
    DialogAction_LEVELUP1 = 536870912, DialogAction_LEVELUP2 = 1073741824, DialogAction_LEVELUP3 = -2147483648
}
enum EComboAttackType
{
    ComboAT_Normal,  ComboAT_Directional,  ComboAT_Restart,  ComboAT_Stop
}
enum eGwintEffect
{
    GwintEffect_None, GwintEffect_Bin2 = 5, GwintEffect_MeleeScorch = 7, GwintEffect_11thCard, GwintEffect_ClearWeather,
    GwintEffect_PickWeatherCard, GwintEffect_PickRainCard, GwintEffect_PickFogCard, GwintEffect_PickFrostCard,
    GwintEffect_View3EnemyCard, GwintEffect_ResurectCard, GwintEffect_ResurectFromEnemy, GwintEffect_Bin2Pick1,
    GwintEffect_MeleeHorn, GwintEffect_RangedHorn, GwintEffect_SiegeHorn, GwintEffect_SiegeScorch,
    GwintEffect_CounterKingAbility, GwintEffect_Melee, GwintEffect_Ranged, GwintEffect_Siege, GwintEffect_UnsummonDummy,
    GwintEffect_Horn, GwintEffect_Draw, GwintEffect_Scorch, GwintEffect_ClearSky, GwintEffect_SummonClones,
    GwintEffect_ImproveNeightbours, GwintEffect_Nurse, GwintEffect_Draw2, GwintEffect_SameTypeMorale,
    GwintEffect_Mushroom = 41, GwintEffect_Morph, GwintEffect_WeatherResistant, GwintEffect_GraveyardShuffle
}
enum EStorySceneSignalType
{
    SSST_Accept, SSST_Highlight, SSST_Skip
}
enum EMovementAdjustmentNotify
{
    MAN_None, MAN_LocationAdjustmentReachedDestination, MAN_RotationAdjustmentReachedDestination, MAN_AdjustmentEnded,
    MAN_AdjustmentCancelled
}
enum ENavigationReachabilityTestType
{
    ENavigationReachability_Any, ENavigationReachability_All, ENavigationReachability_FullTest
}
enum EAsyncTestResult
{
    EAsyncTastResult_Failure, EAsyncTastResult_Success, EAsyncTastResult_Pending, EAsyncTastResult_Invalidated
}
enum EEnvManagerModifier
{
    EMM_None, EMM_RayTracingColor, EMM_RayTracingBufferColor, EMM_RayTracingBufferNormalized,
    EMM_RayTracingBufferMetadataRed, EMM_RayTracingBufferMetadataGreen, EMM_RayTracingBufferMetadataBlue,
    EMM_RayTracingBufferMetadataAlpha, EMM_ScreenSpaceAmbientOcclusionMask, EMM_GBuffAlbedo, EMM_GBuffAlbedoNormalized,
    EMM_GBuffNormalsWorldSpace, EMM_GBuffNormalsViewSpace, EMM_GBuffSpecularity, EMM_GBuffRoughness,
    EMM_GBuffTranslucency, EMM_GBuffVelocity, EMM_Depth, EMM_LinearAll, EMM_DecomposedAmbient, EMM_DecomposedDiffuse,
    EMM_DecomposedSpecular, EMM_DecomposedReflection, EMM_DecomposedLightingAmbient, EMM_DecomposedLightingReflection,
    EMM_DimmersSurface, EMM_DimmersVolume, EMM_ComplexityEnvProbes, EMM_MaskShadow, EMM_MaskSSAO, EMM_MaskInterior,
    EMM_MaskDimmers, EMM_InteriorsVolume, EMM_InteriorsFactor, EMM_Bloom, EMM_StencilMix, EMM_Stencil0, EMM_Stencil1,
    EMM_Stencil2, EMM_Stencil3, EMM_Stencil4, EMM_Stencil5, EMM_Stencil6, EMM_Stencil7, EMM_LocalReflections,
    EMM_LightsOverlay, EMM_LightsOverlayDensity, EMM_SSSR, EMM_HBIL, EMM_HitProxies, EMM_MAX
}
enum EDebugPostProcess
{
    EDPP_Gamma, EDPP_EnableInstantAdaptation, EDPP_EnableGlobalLightingTrajectory, EDPP_AllowEnvProbeUpdate = 4,
    EDPP_AllowBloom, EDPP_AllowColorMod, EDPP_AllowAntialiasing, EDPP_AllowGlobalFog, EDPP_AllowDOF, EDPP_AllowSSAO,
    EDPP_AllowCloudsShadow, EDPP_AllowVignette, EDPP_DisableTonemapping, EDPP_ForceCutsceneDofMode,
    EDPP_AllowWaterShader, EDPP_DisplayMode
}
enum ESessionRestoreResult
{
    RESTORE_Success, RESTORE_DataCorrupted, RESTORE_DLCRequired, RESTORE_MissingContent, RESTORE_InternalError,
    RESTORE_NoGameDefinition, RESTORE_WrongGameVersion
}
enum ESwitchOperation
{
    SO_TurnOn, SO_TurnOff, SO_Toggle, SO_Reset, SO_Enable, SO_Disable, SO_Lock, SO_Unlock
}
enum EPropertyAnimationOperation
{
    PAO_Play, PAO_Stop, PAO_Rewind, PAO_Pause, PAO_Unpause
}
enum EAnimationManualSyncType
{
    AMST_SyncBeginning, AMST_SyncEnd, AMST_SyncMatchEvents
}
enum ESyncRotationUsingRefBoneType
{
    SRT_TowardsOtherEntity, SRT_ToMatchOthersRotation
}
enum EWitcherSwordType
{
    WST_Silver, WST_Steel
}
enum EMeshShadowImportanceBias
{
    MSIB_EvenLessImportant = -2, MSIB_LessImportant, MSIB_Default, MSIB_MoreImportant, MSIB_EvenMoreImportant
}
enum EPathLibCollision
{
    PLC_Disabled, PLC_Static, PLC_StaticWalkable, PLC_StaticMetaobstacle, PLC_Dynamic, PLC_Walkable, PLC_Immediate
}
enum EJournalVisibilityAction
{
    JVA_Nothing, JVA_Show, JVA_Hide
}
enum EJournalMapPinType
{
    EJMPT_Default, EJMPT_QuestReturn, EJMPT_HorseRace, EJMPT_BoatRace, EJMPT_QuestBelgard, EJMPT_QuestCoronata,
    EJMPT_QuestVermentino
}
enum ETopLevelAIPriorities
{
    AIP_Unavailable = -1, AIP_BelowIdle = 15, AIP_AboveIdle = 25, AIP_AboveIdle2 = 30, AIP_AboveEmergency = 60,
    AIP_AboveEmergency2 = 65, AIP_AboveCombat = 85, AIP_AboveCombat2 = 90
}
enum ERenderingPlane
{
    RPl_Scene, RPl_Background
}
enum EDestructionPreset
{
    CUSTOM_PRESET_D
}
enum ETriggerShape
{
    TS_None, TS_Sphere, TS_Box
}
enum EMotionType
{
    MT_Dynamic, MT_KeyFramed
}
enum EAreaTerrainSide
{
    ATS_AboveAndBelowTerrain, ATS_OnlyAboveTerrain, ATS_OnlyBelowTerrain
}
enum EAreaClippingMode
{
    ACM_NoClipping, ACM_ClipToNegativeAreas
}
enum ESoundParameterCurveType
{
    ESPCT_Log3, ESPCT_Sine, ESPCT_Log1, ESPCT_InversedSCurve, ESPCT_Linear, ESPCT_SCurve, ESPCT_Exp1,
    ESPCT_ReciprocalOfSineCurve, ESPCT_Exp3
}
enum ESoundAmbientDynamicParameter
{
    ESADP_None
}
enum EDoorState
{
    Door_Closed, Door_Open
}
enum EZoneAcceptor
{
    ZA_LairAreaOnly, ZA_BothTriggersAndLairArea
}
enum EPhantomShape
{
    PS_Sphere, PS_Box, PS_EntityBounds
}
enum ESpawnTreeType
{
    STT_default, STT_gameplay
}
enum EPathEngineAgentType
{
    PEAT_Player, PEAT_TallNPCs, PEAT_ShortNPCs, PEAT_Monsters, PEAT_Ghost
}
enum ECameraSolver
{
    CS_None, CS_LookAt, CS_Focus
}
enum EJobMovementMode
{
    JMM_Walk, JMM_Run, JMM_CustomSpeed
}
enum EInputSensitivityPreset
{
    ISP_Normal, ISP_Aiming
}
enum ECurveBaseType
{
    CT_Linear, CT_Smooth, CT_Segmented
}
enum EScriptQueryFlags
{
    FLAG_ExcludePlayer = 1, FLAG_OnlyActors, FLAG_OnlyAliveActors = 4, FLAG_WindEmitters = 8, FLAG_Vehicles = 16,
    FLAG_ExcludeTarget = 32, FLAG_Attitude_Neutral = 64, FLAG_Attitude_Friendly = 128, FLAG_Attitude_Hostile = 256,
    FLAG_PathLibTest = 4096, FLAG_NotVehicles = 8192, FLAG_TestLineOfSight = 16384
}
enum PhotomodeParameterId
{
    PM_Camera_Preset, PM_Camera_FOV, PM_Camera_Tilt, PM_DOF_Enable, PM_DOF_Autofocus, PM_DOF_Aperture,
    PM_DOF_FocusDistance, PM_Exposure, PM_Contrast, PM_Highlights, PM_Temperature, PM_Saturation,
    PM_ChromaticAberration, PM_Grain, PM_Filter, PM_Mask, PM_Vignette, PM_Logo
}
enum eGwintType
{
    GwintType_None, GwintType_Melee, GwintType_Ranged, GwintType_Siege = 4, GwintType_Creature = 8,
    GwintType_Weather = 16, GwintType_Spell = 32, GwintType_RowModifier = 64, GwintType_Hero = 128, GwintType_Spy = 256,
    GwintType_FriendlyEffect = 512, GwintType_OffensiveEffect = 1024, GwintType_GlobalEffect = 2048
}
enum GamepadTriggerEffectMode
{
    GTFX_Off, GTFX_Feedback, GTFX_SlopeFeedback, GTFX_MultiFeedback, GTFX_Vibration, GTFX_MultiVibration, GTFX_Weapon
}
enum EBatchQueryQueryFlag
{
    EQQF_IMPACT = 1, EQQF_NORMAL, EQQF_DISTANCE = 4, EQQF_UV = 8, EQQF_NO_INITIAL_OVERLAP = 16, EQQF_TOUCHING_HIT = 32,
    EQQF_BLOCKING_HIT = 64, EQQF_MESH_BOTH_SIDES = 128, EQQF_PRECISE_SWEEP = 256
}
enum ETextureRawFormat
{
    TRF_TrueColor, TRF_Grayscale, TRF_HDR, TRF_AlphaGrayscale, TRF_HDRGrayscale
}
enum ETextureCompression
{
    TCM_None, TCM_DXTNoAlpha, TCM_DXTAlpha, TCM_RGBE, TCM_Normals, TCM_NormalsHigh, TCM_NormalsGloss,
    TCM_DXTAlphaLinear = 8, TCM_QualityR, TCM_QualityRG, TCM_QualityColor
}
enum ECurveType
{
    ECurveType_Uninitialized, ECurveType_Float, ECurveType_Vector, ECurveType_EngineTransform, ECurveType_EulerAngles
}
enum ECurveInterpolationMode
{
    ECurveInterpolationMode_Linear, ECurveInterpolationMode_Automatic, ECurveInterpolationMode_Manual
}
enum ECurveManualMode
{
    ECurveManualMode_Bezier, ECurveManualMode_BezierSymmetricDirection, ECurveManualMode_BezierSymmetric
}
enum ECurveRelativeMode
{
    ECurveRelativeMode_None, ECurveRelativeMode_InitialTransform, ECurveRelativeMode_CurrentTransform
}
enum ESimpleCurveType
{
    SCT_Float, SCT_Vector, SCT_Color, SCT_ColorScaled
}
enum EApertureValue
{
    __f_1_0, __f_1_4, __f_2_0, __f_2_8, __f_4_0, __f_5_6, __f_8_0, __f_11_0, __f_16_0, __f_22_0, __f_32_0,
}
enum ECameraPlane
{
    None, Wide, Medium, Semicloseup, Closeup, Supercloseup
}
enum EMeshVertexType
{
    MVT_StaticMesh, MVT_SkinnedMesh, MVT_DestructionMesh
}