class __array_methods {
    function PushBack(value : __element): int;
    function Size() : int;
    function Contains(value : __element) : bool;
    function Clear();
    function Erase(index : int);
    function EraseFast(index : int);
    function FindFirst(value : __element) : int;
    function Insert(index : int, value : __element);
    function Remove(value : __element): bool;
    function Grow(size : int): int;
    function Resize(size : int): int;
    function PopBack(): __element;
}