struct ApertureDofParams
{
    var aperture: EApertureValue;
    var focalLength: float;
    var distance: float;
    var enabled: bool;
}
struct Box
{
    var Min: Vector;
    var Max: Vector;
}
struct CAreaEnvironmentParams
{
}
struct CBehTreeValBool
{
    var varName: name;
    var value: bool;
}
struct CBehTreeValCName
{
    var varName: name;
    var value: name;
}
struct CBehTreeValFloat
{
    var varName: name;
    var value: float;
}
struct CBehTreeValInt
{
    var varName: name;
    var value: int;
}
struct CBehTreeValString
{
    var varName: name;
    var value: string;
}
struct CBuffImmunity
{
    var immunityTo: array<int>;
    var potion: bool;
    var positive: bool;
    var negative: bool;
    var neutral: bool;
    var immobilize: bool;
    var confuse: bool;
    var damage: bool;
}
struct CColorShift
{
    var hue: Uint16;
    var saturation: Int8;
    var luminance: Int8;
}
struct CComponentReference
{
    //var `name`: ?;
    var className: name;
}
struct CEntityAppearance
{
    //var `name`: ?;
    var voicetag: name;
    //var appearanceParams: ?;
    var useVertexCollapse: bool;
    var usesRobe: bool;
    var wasIncluded: bool;
    var includedTemplates: array<CEntityTemplate>;
    var collapsedComponents: array<name>;
}
struct CEntityBodyPart
{
    //var `name`: ?;
    var states: array<CEntityBodyPartState>;
    var wasIncluded: bool;
}
struct CEntityBodyPartState
{
    //var `name`: ?;
    var componentsInUse: array<CComponentReference>;
}
struct CEntityTemplateCookedEffect
{
    //var `name`: ?;
    var animName: name;
    var buffer: SharedDataBuffer;
}
struct CEventGeneratorCameraParams
{
    var cameraPlane: ECameraPlane;
    var tags: TagList;
    var targetSlot: int;
    var sourceSlot: int;
    var usableForGenerator: bool;
}
struct CFlyingGroupId
{
}
struct CFlyingSwarmGroup
{
    var groupId: CFlyingGroupId;
    var groupCenter: Vector;
    var targetPosition: Vector;
    var currentGroupState: name;
    var boidCount: int;
    var toSpawnCount: int;
    var spawnPoiType: name;
    var toDespawnCount: int;
    var despawnPoiType: name;
    var changeGroupState: name;
}
struct CGlobalLightingTrajectory
{
    var yawDegrees: float;
    var yawDegreesSunOffset: float;
    var yawDegreesMoonOffset: float;
    var sunCurveShiftFactor: float;
    var moonCurveShiftFactor: float;
    var sunSqueeze: float;
    var moonSqueeze: float;
    var sunHeight: SSimpleCurve;
    var moonHeight: SSimpleCurve;
    var lightHeight: SSimpleCurve;
    var lightDirChoice: SSimpleCurve;
    var skyDayAmount: SSimpleCurve;
    var moonShaftsBeginHour: float;
    var moonShaftsEndHour: float;
}
struct CPreAttackEventData
{
    var attackName: name;
    var weaponSlot: name;
    var hitReactionType: int;
    var rangeName: name;
    var Damage_Friendly: bool;
    var Damage_Neutral: bool;
    var Damage_Hostile: bool;
    var Can_Parry_Attack: bool;
    var hitFX: name;
    var hitBackFX: name;
    var hitParriedFX: name;
    var hitBackParriedFX: name;
    var swingType: int;
    var swingDir: int;
    var soundAttackType: name;
    var canBeDodged: bool;
    var cameraAnimOnMissedHit: name;
}
struct CSpawnTreeActionPointSpawner
{
    var visibility: ESpawnTreeSpawnVisibility;
    var spawnpointDelay: float;
    var tags: TagList;
    var categories: array<name>;
}
struct CSpawnTreeWaypointSpawner
{
    var visibility: ESpawnTreeSpawnVisibility;
    var spawnpointDelay: float;
    var tags: TagList;
    var useLocationTest: bool;
}
struct CTextureArrayEntry
{
    //var m_texture: ?;
}
struct CTicketSourceConfiguration
{
    //var `name`: ?;
    var ticketsPoolSize: Uint16;
    var minimalImportance: float;
}
struct CWorldShadowConfig
{
    var numCascades: int;
    var cascadeRange1: float;
    var cascadeRange2: float;
    var cascadeRange3: float;
    var cascadeRange4: float;
    var cascadeFilterSize1: float;
    var cascadeFilterSize2: float;
    var cascadeFilterSize3: float;
    var cascadeFilterSize4: float;
    var shadowEdgeFade1: float;
    var shadowEdgeFade2: float;
    var shadowEdgeFade3: float;
    var shadowEdgeFade4: float;
    var shadowBiasOffsetSlopeMul: float;
    var shadowBiasOffsetConst: float;
    var shadowBiasOffsetConstCascade1: float;
    var shadowBiasOffsetConstCascade2: float;
    var shadowBiasOffsetConstCascade3: float;
    var shadowBiasOffsetConstCascade4: float;
    var shadowBiasCascadeMultiplier: float;
    var speedTreeShadowFilterSize1: float;
    var speedTreeShadowFilterSize2: float;
    var speedTreeShadowFilterSize3: float;
    var speedTreeShadowFilterSize4: float;
    var speedTreeShadowGradient: float;
    var hiResShadowBiasOffsetSlopeMul: float;
    var hiResShadowBiasOffsetConst: float;
    var hiResShadowTexelRadius: float;
    var useTerrainShadows: bool;
    var terrainShadowsDistance: float;
    var terrainShadowsFadeRange: float;
    var terrainShadowsBaseSmoothing: float;
    var terrainShadowsTerrainDistanceSoftness: float;
    var terrainShadowsMeshDistanceSoftness: float;
    var terrainMeshShadowDistance: float;
    var terrainMeshShadowFadeRange: float;
}
struct CollectionCard
{
    var cardID: int;
    var numCopies: int;
}
struct Color
{
    var Red: byte;
    var Green: byte;
    var Blue: byte;
    var Alpha: byte;
}
struct EngineTime
{
}
struct EntitySlot
{
    var wasIncluded: bool;
    //var `name`: ?;
    var componentName: name;
    var boneName: name;
    var transform: EngineTransform;
    var freePositionAxisX: bool;
    var freePositionAxisY: bool;
    var freePositionAxisZ: bool;
    var freeRotation: bool;
}
struct EulerAngles
{
    var Pitch: float;
    var Yaw: float;
    var Roll: float;
}
struct GameTime
{
    var m_seconds: int;
}
struct GameTimeInterval
{
    var begin: GameTime;
    var end: GameTime;
}
struct GameTimeWrapper
{
    var gameTime: GameTime;
}
struct IReferencable
{
}
struct Matrix
{
    var X: Vector;
    var Y: Vector;
    var Z: Vector;
    var W: Vector;
}
struct PersistentRef
{
    var entityHandle: EntityHandle;
    var position: Vector;
    var rotation: EulerAngles;
}
struct SAbilityAttributeValue
{
    var valueAdditive: float;
    var valueMultiplicative: float;
    var valueBase: float;
}
struct SActionMatchToSettings
{
    var animation: name;
    var slotName: name;
    var blendIn: float;
    var blendOut: float;
    var useGameTimeScale: bool;
    var useRotationDeltaPolicy: bool;
    var matchBoneName: name;
    var matchEventName: name;
}
struct SActionMatchToTarget
{
}
struct SActionPointId
{
    var component: CGUID;
    var entity: CGUID;
}
struct SAnimatedComponentSlotAnimationSettings
{
    var blendIn: float;
    var blendOut: float;
}
struct SAnimatedComponentSyncSettings
{
    var instanceName: name;
    var syncAllInstances: bool;
    var syncEngineValueSpeed: bool;
}
struct SAnimatedSlideSettings
{
    var animation: name;
    var slotName: name;
    var blendIn: float;
    var blendOut: float;
    var useGameTimeScale: bool;
    var useRotationDeltaPolicy: bool;
}
struct SAnimationEventAnimInfo
{
}
struct SAnimationSequenceDefinition
{
    var entity: CEntity;
    var manualSlotName: name;
    var parts: array<SAnimationSequencePartDefinition>;
    var freezeAtEnd: bool;
    var startForceEvent: name;
    var raiseEventOnEnd: name;
    var raiseForceEventOnEnd: name;
}
struct SAnimationSequencePartDefinition
{
    var animation: name;
    var syncType: EAnimationManualSyncType;
    var syncEventName: name;
    var shouldSlide: bool;
    var shouldRotate: bool;
    var useRefBone: name;
    var rotationTypeUsingRefBone: ESyncRotationUsingRefBoneType;
    var finalPosition: Vector;
    var finalHeading: float;
    var blendTransitionTime: float;
    var blendInTime: float;
    var blendOutTime: float;
    var allowBreakAtStart: float;
    var allowBreakAtStartBeforeEventsEnd: name;
    var allowBreakBeforeEnd: float;
    var allowBreakBeforeAtAfterEventsStart: name;
    var sequenceIndex: int;
    var disableProxyCollisions: bool;
}
struct SAnimationTrajectoryPlayerInput
{
    var localToWorld: Matrix;
    var pointWS: Vector;
    var directionWS: Vector;
    var tagId: name;
    var selectorType: EAnimationTrajectorySelectorType;
    var proxySyncType: EActionMoveAnimationSyncType;
    var proxy: CActionMoveAnimationProxy;
}
struct SAnimationTrajectoryPlayerToken
{
    var isValid: bool;
    var pointWS: Vector;
    var syncPointMS: Vector;
    var timeFactor: float;
    var syncPointDuration: float;
    var blendIn: float;
    var blendOut: float;
    var duration: float;
    var syncTime: float;
}
struct SAppearanceAttachment
{
    var parentClass: name;
    var parentName: name;
    var childClass: name;
    var childName: name;
}
struct SAppearanceAttachments
{
    var appearance: name;
    var attachments: array<SAppearanceAttachment>;
}
struct SAreaMapPinInfo
{
    var areaType: int;
    var position: Vector;
    var worldPath: string;
    var requiredChunk: name;
    var localisationName: name;
    var localisationDescription: name;
}
struct SAttachmentReplacement
{
    var oldName: name;
    var oldClass: name;
    var newName: name;
    var newClass: name;
}
struct SAttachmentReplacements
{
    var replacements: array<SAttachmentReplacement>;
}
struct SBaseStat
{
    var current: float;
    var max: float;
    var type: EBaseCharacterStats;
}
struct SBehTreeExternalListenerPtr
{
}
struct SBehaviorComboAttack
{
    var level: int;
    var type: int;
    var direction: EAttackDirection;
    var distance: EAttackDistance;
    var attackTime: float;
    var parryTime: float;
    var attackAnimation: name;
    var parryAnimation: name;
    var attackHitTime: float;
    var parryHitTime: float;
    var attackHitLevel: float;
    var parryHitLevel: float;
    var attackHitTime1: float;
    var parryHitTime1: float;
    var attackHitLevel1: float;
    var parryHitLevel1: float;
    var attackHitTime2: float;
    var parryHitTime2: float;
    var attackHitLevel2: float;
    var parryHitLevel2: float;
    var attackHitTime3: float;
    var parryHitTime3: float;
    var attackHitLevel3: float;
    var parryHitLevel3: float;
}
struct SBehaviorGraphInstanceSlot
{
    var instanceName: name;
    var graph: CBehaviorGraph;
    var alwaysOnTopOfStack: bool;
}
struct SBlockDesc
{
    //var ock: ?;
    var putName: name;
}
struct SBlockedAbility
{
    var abilityName: name;
    var timeWhenEnabledd: float;
    var count: int;
}
struct SBoatDestructionVolume
{
    var areaHealth: float;
    var volumeCorners: Vector;
    var volumeLocalPosition: Vector;
}
struct SBokehDofParams
{
    var enabled: bool;
    var hexToCircleScale: float;
    var usePhysicalSetup: bool;
    var planeInFocus: float;
    var fStops: EApertureValue;
    var bokehSizeMuliplier: float;
}
struct SBoneIndiceMapping
{
    var startingIndex: Uint32;
    var endingIndex: Uint32;
    var chunkIndex: Uint32;
    var boneIndex: Uint32;
}
struct SCachedConnections
{
    var socketId: name;
    var blocks: array<SBlockDesc>;
}
struct SCameraAnimationDefinition
{
    var animation: name;
    var priority: int;
    var blendIn: float;
    var blendOut: float;
    var weight: float;
    var speed: float;
    var loop: bool;
    var reset: bool;
    var additive: bool;
    var exclusive: bool;
}
struct SCameraMovementData
{
    var camera: CCustomCamera;
    var pivotPositionController: ICustomCameraPivotPositionController;
    var pivotRotationController: ICustomCameraPivotRotationController;
    var pivotDistanceController: ICustomCameraPivotDistanceController;
    var pivotPositionValue: Vector;
    var pivotPositionVelocity: Vector;
    var pivotRotationValue: EulerAngles;
    var pivotRotationVelocity: EulerAngles;
    var pivotDistanceValue: float;
    var pivotDistanceVelocity: float;
    var cameraLocalSpaceOffset: Vector;
    var cameraLocalSpaceOffsetVel: Vector;
    var cameraOffset: Vector;
}
struct SCardDefinition
{
    var index: int;
    var title: string;
    var description: string;
    var power: int;
    var picture: string;
    var faction: eGwintFaction;
    var typeFlags: int;
    var effectFlags: array<eGwintEffect>;
    var summonFlags: array<int>;
    var dlcPictureFlag: name;
    var dlcPicture: string;
}
struct SCollisionData
{
    var entity: CEntity;
    var point: Vector;
    var normal: Vector;
}
struct SComboAnimationData
{
    var animationName: name;
    var blendIn: float;
    var blendOut: float;
}
struct SComboAttackCallbackInfo
{
    var outDirection: EAttackDirection;
    var outDistance: EAttackDistance;
    var outAttackType: EComboAttackType;
    var inAspectName: name;
    var inGlobalAttackCounter: int;
    var inStringAttackCounter: int;
    var inAttackId: int;
    var prevDirAttack: bool;
    var outRotateToEnemyAngle: float;
    var outSlideToPosition: Vector;
    var outShouldTranslate: bool;
    var outShouldRotate: bool;
    var outLeftString: bool;
}
struct SCommonMapPinInstance
{
    var id: int;
    var tag: name;
    var customNameId: int;
    var extraTag: name;
    var type: name;
    var visibleType: name;
    var alternateVersion: int;
    var position: Vector;
    var radius: float;
    var visibleRadius: float;
    var guid: CGUID;
    var entities: array<CEntity>;
    var isDynamic: bool;
    var isKnown: bool;
    var isDiscovered: bool;
    var isDisabled: bool;
    var isHighlightable: bool;
    var isHighlighted: bool;
    var canBePointedByArrow: bool;
    var canBeAddedToMinimap: bool;
    var isAddedToMinimap: bool;
    var invalidated: bool;
}
struct SComponentInstancePropertyEntry
{
    var component: name;
    var property: name;
}
struct SCreatureDefinitionWrapper
{
    var creatureDefinition: name;
}
struct SCreatureEntryEntryGeneratorNodeParam
{
    var comment: string;
    var qualityMin: int;
    var qualityMax: int;
    var spawnWayPointTag: TagList;
    var creatureDefinition: SCreatureDefinitionWrapper;
    var appearanceName: name;
    var tagToAssign: name;
    var group: int;
}
struct SCurveData
{
    //var `Curve Values`: ?;
    //var `value type`: ?;
    var type: ECurveBaseType;
    //var `is looped`: ?;
}
struct SCurveDataEntry
{
    var me: float;
    var ntrolPoint: Vector;
    var lue: float;
    var rveTypeL: Uint16;
    var rveTypeR: Uint16;
}
struct SCurveEaseParam
{
    var easeIn: float;
    var easeOut: float;
}
struct SCustomCameraPreset
{
    var pressetName: name;
    var distance: float;
    var offset: Vector;
}
struct SCustomMapPinDefinition
{
    var tag: name;
    var type: name;
}
struct SCustomNode
{
    var nodeName: name;
    var attributes: array<SCustomNodeAttribute>;
    var values: array<name>;
    var subNodes: array<SCustomNode>;
}
struct SCustomNodeAttribute
{
    var attributeName: name;
    var attributeValue: string;
    var attributeValueAsCName: name;
}
struct SDLCLanguagePack
{
    var textLanguages: array<string>;
    var speechLanguages: array<string>;
}
struct SDeckDefinition
{
    var cardIndices: array<int>;
    var leaderIndex: int;
    var unlocked: bool;
    var specialCard: int;
    var dynamicCardRequirements: array<int>;
    var dynamicCards: array<int>;
}
struct SDropPhysicsCurves
{
    var trajectory: SMultiCurve;
    var rotation: SMultiCurve;
}
struct SEncounterActionPointSelectorPair
{
    //var `name`: ?;
    var chance: float;
}
struct SEntityActionsRouterEntry
{
    var eventName: name;
    //var actionsToPerform: ?;
}
struct SEntityMapPinInfo
{
    var entityName: name;
    var entityCustomNameId: Uint32;
    var entityTags: TagList;
    var entityPosition: Vector;
    var entityRadius: float;
    var entityType: name;
    var alternateVersion: int;
    var fastTravelSpotName: name;
    var fastTravelGroupName: name;
    var fastTravelTeleportWayPointTag: name;
    var fastTravelTeleportWayPointPosition: Vector;
    var fastTravelTeleportWayPointRotation: EulerAngles;
}
struct SEntitySpawnData
{
    var restored: bool;
}
struct SEntityTemplateColoringEntry
{
    var appearance: name;
    var componentName: name;
    var colorShift1: CColorShift;
    var colorShift2: CColorShift;
}
struct SEntityTemplateOverride
{
    var componentName: name;
    var className: name;
    var overriddenProperties: array<name>;
}
struct SEnumVariant
{
    var enumType: name;
    var enumValue: int;
}
struct SExplorationQueryContext
{
    var inputDirectionInWorldSpace: Vector;
    var maxAngleToCheck: float;
    var forJumping: bool;
    var forDynamic: bool;
    var dontDoZAndDistChecks: bool;
    var laddersOnly: bool;
    var forAutoTraverseSmall: bool;
    var forAutoTraverseBig: bool;
}
struct SExplorationQueryToken
{
    var valid: bool;
    var type: EExplorationType;
    var pointOnEdge: Vector;
    var normal: Vector;
    var usesHands: bool;
}
struct SFlashArg
{
}
struct SGlobalSpeedTreeParameters
{
    var alphaScalar3d: float;
    var alphaScalarGrassNear: float;
    var alphaScalarGrass: float;
    var alphaScalarGrassDistNear: float;
    var alphaScalarGrassDistFar: float;
    var alphaScalarBillboards: float;
    var billboardsGrainBias: float;
    var billboardsGrainAlbedoScale: float;
    var billboardsGrainNormalScale: float;
    var billboardsGrainClipScale: float;
    var billboardsGrainClipBias: float;
    var billboardsGrainClipDamping: float;
    var grassNormalsVariation: float;
}
struct SGuiEnhancementInfo
{
    var enhancedItem: name;
    var enhancement: name;
    var oilItem: name;
    var oil: name;
    var dyeItem: name;
    var dye: name;
    var dyeColor: int;
}
struct SInputAction
{
    var aName: name;
    var value: float;
    var lastFrameValue: float;
}
struct SInventoryItem
{
    //var `name`: ?;
    var itemQuantity: Uint32;
    var uniqueId: SItemUniqueId;
    var flags: Uint64;
    var staticRandomSeed: Uint16;
    var uiData: SInventoryItemUIData;
    var craftedAbilities: array<name>;
    var enchantmentName: name;
    var enchantmentStats: name;
    var dyeColorName: name;
    var dyeColorAbilityName: name;
    var dyePreviewColorName: name;
}
struct SInventoryItemUIData
{
    var gridPosition: int;
    var gridSize: int;
    var isNew: bool;
}
struct SItemChangedData
{
    var itemName: name;
    var quantity: int;
    var ids: array<SItemUniqueId>;
    var informGui: bool;
}
struct SItemNameProperty
{
    var itemName: name;
}
struct SItemParts
{
    var itemName: name;
    var quantity: int;
}
struct SItemReward
{
    var item: name;
    var amount: int;
}
struct SItemUniqueId
{
    var value: Uint32;
}
struct SJobTreeSettings
{
    var leftRemoveAtEnd: bool;
    var leftDropOnBreak: bool;
    var rightRemoveAtEnd: bool;
    var rightDropOnBreak: bool;
    var ignoreHardReactions: bool;
    var needsPrecision: bool;
    var isConscious: bool;
    var altJobTreeRes: CJobTree;
    var globalBreakingBlendOutTime: float;
    var forceKeepIKactive: bool;
    var jobTreeType: int;
}
struct SJournalCreatureParams
{
    var abilities: array<name>;
    var autoEffects: array<name>;
    var buffImmunity: CBuffImmunity;
    var monsterCategory: int;
    var isTeleporting: bool;
    var droppedItems: array<name>;
}
struct SJournalQuestObjectiveData
{
    var status: EJournalStatus;
    var objectiveEntry: CJournalQuestObjective;
}
struct SLensFlareElementParameters
{
    var material: CMaterialInstance;
    var isConstRadius: bool;
    var isAligned: bool;
    var centerFadeStart: float;
    var centerFadeRange: float;
    var colorGroupParamsIndex: Uint32;
    var alpha: float;
    var size: float;
    var aspect: float;
    var shift: float;
    var pivot: float;
    var color: Color;
}
struct SLensFlareGroupsParameters
{
    //var `default`: ?;
    var sun: SLensFlareParameters;
    var moon: SLensFlareParameters;
    var custom0: SLensFlareParameters;
    var custom1: SLensFlareParameters;
    var custom2: SLensFlareParameters;
    var custom3: SLensFlareParameters;
    var custom4: SLensFlareParameters;
    var custom5: SLensFlareParameters;
}
struct SLensFlareParameters
{
    var nearDistance: float;
    var nearRange: float;
    var farDistance: float;
    var farRange: float;
    var elements: array<SLensFlareElementParameters>;
}
struct SMapPathInstance
{
    var id: int;
    var position: Vector;
    var splinePoints: array<Vector>;
    var color: int;
    var lineWidth: float;
    var isAddedToMinimap: bool;
}
struct SMeshChunkPacked
{
    var vertexType: EMeshVertexType;
    var materialID: Uint32;
    var numBonesPerVertex: byte;
    var numVertices: Uint32;
    var numIndices: Uint32;
    var firstVertex: Uint32;
    var firstIndex: Uint32;
    //var renderMask: ?;
    var useForShadowmesh: bool;
}
struct SMeshCookedData
{
    var collisionInitPositionOffset: Vector;
    var dropOffset: Vector;
    var bonePositions: array<Vector>;
    var renderLODs: array<float>;
    var renderChunks: array<byte>;
    var renderBuffer: DeferredDataBuffer;
    var quantizationScale: Vector;
    var quantizationOffset: Vector;
    var vertexBufferSize: Uint32;
    var indexBufferSize: Uint32;
    var indexBufferOffset: Uint32;
    var blasLODsOffsets: array<Uint32>;
    var blasBuffer: DeferredDataBuffer;
}
struct SMoveLocomotionGoal
{
}
struct SMovementAdjustmentRequestTicket
{
}
struct SMultiCurve
{
    var type: ECurveType;
    var color: Color;
    var showFlags: EShowFlags;
    var positionInterpolationMode: ECurveInterpolationMode;
    var positionManualMode: ECurveManualMode;
    var rotationInterpolationMode: ECurveInterpolationMode;
    var totalTime: float;
    var automaticPositionInterpolationSmoothness: float;
    var automaticRotationInterpolationSmoothness: float;
    var enableConsistentNumberOfControlPoints: bool;
    var enableAutomaticTimeByDistanceRecalculation: bool;
    var enableAutomaticTimeRecalculation: bool;
    var enableAutomaticRotationFromDirectionRecalculation: bool;
    var curves: array<SCurveData>;
    var leftTangents: array<Vector>;
    var rightTangents: array<Vector>;
    var easeParams: array<SCurveEaseParam>;
    var translationRelativeMode: ECurveRelativeMode;
    var rotationRelativeMode: ECurveRelativeMode;
    var scaleRelativeMode: ECurveRelativeMode;
    var initialParentTransform: EngineTransform;
    var hasInitialParentTransform: bool;
}
struct SMultiValue
{
	var floats: array<float>;
	var bools: array<bool>;
	var enums: array<SEnumVariant>;
	var names: array<name>;
}
struct SPredictionInfo
{
    var distanceToCollision: float;
    var normalYaw: float;
    var turnAngle: float;
    var leftGroundLevel: float;
    var frontGroundLevel: float;
    var rightGroundLevel: float;
}
struct SProcessedDamage
{
    var vitalityDamage: float;
    var essenceDamage: float;
    var moraleDamage: float;
    var staminaDamage: float;
}
struct SR4LootNameProperty
{
    var lootName: name;
}
struct SR4PlayerTargetingConsts
{
    var softLockDistance: float;
    var softLockFrameSize: float;
}
struct SR4PlayerTargetingIn
{
    var canFindTarget: bool;
    var playerHasBlockingBuffs: bool;
    var isHardLockedToTarget: bool;
    var isActorLockedToTarget: bool;
    var isCameraLockedToTarget: bool;
    var actionCheck: bool;
    var actionInput: bool;
    var isInCombatAction: bool;
    var isLAxisReleased: bool;
    var isLAxisReleasedAfterCounter: bool;
    var isLAxisReleasedAfterCounterNoCA: bool;
    var lastAxisInputIsMovement: bool;
    var isAiming: bool;
    var isSwimming: bool;
    var isDiving: bool;
    var isThreatened: bool;
    var isCombatMusicEnabled: bool;
    var isPcModeEnabled: bool;
    var shouldUsePcModeTargeting: bool;
    var isInParryOrCounter: bool;
    var bufferActionType: EBufferActionType;
    var orientationTarget: EOrientationTarget;
    var coneDist: float;
    var findMoveTargetDist: float;
    var cachedRawPlayerHeading: float;
    var combatActionHeading: float;
    var rawPlayerHeadingVector: Vector;
    var lookAtDirection: Vector;
    var moveTarget: CActor;
    var aimingTarget: CActor;
    var displayTarget: CActor;
    var finishableEnemies: array<CActor>;
    var hostileEnemies: array<CActor>;
    var defaultSelectionWeights: STargetSelectionWeights;
}
struct SR4PlayerTargetingOut
{
    var target: CActor;
    var result: bool;
    var confirmNewTarget: bool;
    var forceDisableUpdatePosition: bool;
}
struct SR4PlayerTargetingPrecalcs
{
    var playerPosition: Vector;
    var playerHeading: float;
    var playerHeadingVector: Vector;
    var playerRadius: float;
    var cameraPosition: Vector;
    var cameraDirection: Vector;
    var cameraHeading: float;
    var cameraHeadingVector: Vector;
}
struct SRaycastHitResult
{
    var position: Vector;
    var normal: Vector;
    var distance: float;
    var component: CComponent;
}
struct SResistanceValue
{
    var points: SAbilityAttributeValue;
    var percents: SAbilityAttributeValue;
    var type: ECharacterDefenseStats;
}
struct SReverbDefinition
{
    var reverbName: StringAnsi;
    var enabled: bool;
}
struct SReward
{
    //var `name`: ?;
    var experience: int;
    var level: int;
    var gold: int;
    var items: array<SItemReward>;
    var achievement: int;
    var script: name;
    var comment: string;
}
struct SSavegameInfo
{
    var filename: string;
    var slotType: ESaveGameType;
    var slotIndex: int;
    var comboStatus: ESaveComboStatus;
}
struct SSceneChoice
{
    var description: string;
    var emphasised: bool;
    var previouslyChoosen: bool;
    var disabled: bool;
    var dialogAction: EDialogActionIcon;
    var playGoChunk: name;
}
struct SScriptRaycastId
{
}
struct SScriptSweepId
{
}
struct SSimpleCurve
{
    var CurveType: ESimpleCurveType;
    var ScalarEditScale: float;
    var ScalarEditOrigin: float;
    var dataCurveValues: array<SCurveDataEntry>;
    var dataBaseType: ECurveBaseType;
}
struct SSimplexTreeStruct
{
    //var `parent`: ?;
    var positiveStruct: int;
    var negativeStruct: int;
    var positiveID: int;
    var negativeID: int;
    var normalX: float;
    var normalY: float;
    var offset: float;
}
struct SSkeletonBone
{
    //var `name`: ?;
    var nameAsCName: name;
    //var flags: ?;
}
struct SSkeletonTrack
{
    //var `name`: ?;
    var nameAsCName: name;
}
struct SSlideToTargetEventProps
{
    var minSlideDist: float;
    var maxSlideDist: float;
    var slideToMaxDistIfTargetSeen: bool;
    var slideToMaxDistIfNoTarget: bool;
}
struct SSoundAmbientDynamicSoundEvents
{
    var eventName: StringAnsi;
    var repeatTime: float;
    var repeatTimeVariance: float;
    var triggerOnActivation: bool;
}
struct SSoundGameParameterValue
{
    var gameParameterName: StringAnsi;
    var gameParameterValue: float;
}
struct SSoundInfoMapping
{
    var soundTypeIdentification: name;
    var soundSizeIdentification: name;
    var boneIndexes: array<int>;
    var isDefault: bool;
}
struct SSoundParameterCullSettings
{
    var gameParameterName: StringAnsi;
    var gameParameterCullValue: float;
    var invertCullCheck: bool;
}
struct SSpawnTreeDespawnConfiguration
{
    var canDespawnOnSight: bool;
    var minDespawnRange: float;
    var forceDespawnRange: float;
    var despawnDelayMin: float;
    var despawnDelayMax: float;
    var despawnTime: float;
}
struct SStorySceneGameplayActionCallbackInfo
{
    var outChangeItems: bool;
    var outDontUseSceneTeleport: bool;
    var inActorPosition: Vector;
    var inActorHeading: Vector;
    var inGameplayAction: int;
    var inActor: CActor;
}
struct SStreamedAttachment
{
    var parentName: name;
    var parentClass: name;
    var childName: name;
    var childClass: name;
    var data: array<byte>;
}
struct SSweepHitResult
{
    var position: Vector;
    var normal: Vector;
    var distance: float;
    var component: CComponent;
}
struct SSwitchableFoliageEntry
{
    //var `name`: ?;
    //var tree: ?;
}
struct STargetSelectionData
{
    var sourcePosition: Vector;
    var headingVector: Vector;
    var closeDistance: float;
    var softLockDistance: float;
}
struct STargetSelectionWeights
{
    var angleWeight: float;
    var distanceWeight: float;
    var distanceRingWeight: float;
}
struct STargetingInfo
{
    var source: CActor;
    var targetEntity: CGameplayEntity;
    var canBeTargetedCheck: bool;
    var coneCheck: bool;
    var coneHalfAngleCos: float;
    var coneDist: float;
    var coneHeadingVector: Vector;
    var distCheck: bool;
    var invisibleCheck: bool;
    var navMeshCheck: bool;
    var inFrameCheck: bool;
    var frameScaleX: float;
    var frameScaleY: float;
    var knockDownCheck: bool;
    var knockDownCheckDist: float;
    var rsHeadingCheck: bool;
    var rsHeadingLimitCos: float;
}
struct SWanderAndWorkEntryGeneratorParams
{
    var creatureEntry: SCreatureEntryEntryGeneratorNodeParam;
    var wander: SWanderHistoryEntryGeneratorParams;
    var work: SWorkWanderSmartAIEntryGeneratorParam;
}
struct SWanderHistoryEntryGeneratorParams
{
    var wanderPointsGroupTag: name;
}
struct SWorkCategoriesWrapper
{
    var categories: array<name>;
}
struct SWorkCategoryWrapper
{
    var category: name;
}
struct SWorkEntryGeneratorParam
{
    var creatureEntry: SCreatureEntryEntryGeneratorNodeParam;
    var work: SWorkSmartAIEntryGeneratorNodeParam;
}
struct SWorkSmartAIEntryGeneratorNodeParam
{
    var apTag: TagList;
    var areaTags: TagList;
    var apAreaTag: name;
    var keepActionPointOnceSelected: bool;
    var actionPointMoveType: EMoveType;
}
struct SWorkWanderSmartAIEntryGeneratorParam
{
    var apTag: TagList;
    var areaTags: TagList;
    var apAreaTag: name;
}
struct SWorldEnvironmentParameters
{
    var vignetteTexture: CBitmapTexture;
    var cameraDirtTexture: CBitmapTexture;
    var interiorFallbackAmbientTexture: CCubeTexture;
    var interiorFallbackReflectionTexture: CCubeTexture;
    var cameraDirtNumVerticalTiles: float;
    var globalLightingTrajectory: CGlobalLightingTrajectory;
    var toneMappingAdaptationSpeedUp: float;
    var toneMappingAdaptationSpeedDown: float;
    var environmentDefinition: CEnvironmentDefinition;
    var scenesEnvironmentDefinition: CEnvironmentDefinition;
    var speedTreeParameters: SGlobalSpeedTreeParameters;
    var weatherTemplate: C2dArray;
    var disableWaterShaders: bool;
    var skybox: SWorldSkyboxParameters;
    var lensFlare: SLensFlareGroupsParameters;
    var renderSettings: SWorldRenderSettings;
    var localWindDampers: CResourceSimplexTree;
    var localWaterVisibility: CResourceSimplexTree;
}
struct SWorldMotionBlurSettings
{
    var isPostTonemapping: bool;
    var distanceNear: float;
    var distanceRange: float;
    var strengthNear: float;
    var strengthFar: float;
    var fullBlendOverPixels: float;
    var standoutDistanceNear: float;
    var standoutDistanceRange: float;
    var standoutAmountNear: float;
    var standoutAmountFar: float;
    var sharpenAmount: float;
}
struct SWorldRenderSettings
{
    var cameraNearPlane: float;
    var cameraFarPlane: float;
    var ssaoBlurEnable: bool;
    var ssaoNormalsEnable: bool;
    var envProbeSecondAmbientFilterSize: float;
    var fakeCloudsShadowSize: float;
    var fakeCloudsShadowSpeed: float;
    var fakeCloudsShadowTexture: CTextureArray;
    var bloomLevelsRange: Uint32;
    var bloomLevelsOffset: Uint32;
    var bloomScaleConst: float;
    var bloomDownscaleDivBase: float;
    var bloomDownscaleDivExp: float;
    var bloomLevelScale0: float;
    var bloomLevelScale1: float;
    var bloomLevelScale2: float;
    var bloomLevelScale3: float;
    var bloomLevelScale4: float;
    var bloomLevelScale5: float;
    var bloomLevelScale6: float;
    var bloomLevelScale7: float;
    var bloomPrecision: float;
    var shaftsLevelIndex: Uint32;
    var shaftsIntensity: float;
    var shaftsThresholdsScale: float;
    var fresnelScaleLights: float;
    var fresnelScaleEnvProbes: float;
    var fresnelRoughnessShape: float;
    var interiorDimmerAmbientLevel: float;
    var interiorVolumeSmoothExtent: float;
    var interiorVolumeSmoothRemovalRange: float;
    var interiorVolumesFadeStartDist: float;
    var interiorVolumesFadeRange: float;
    var interiorVolumesFadeEncodeRange: float;
    var distantLightStartDistance: float;
    var distantLightFadeDistance: float;
    var globalFlaresTransparencyThreshold: float;
    var globalFlaresTransparencyRange: float;
    var motionBlurSettings: SWorldMotionBlurSettings;
    var chromaticAberrationStart: float;
    var chromaticAberrationRange: float;
    var interiorFallbackReflectionThresholdLow: float;
    var interiorFallbackReflectionThresholdHigh: float;
    var interiorFallbackReflectionBlendLow: float;
    var interiorFallbackReflectionBlendHigh: float;
    var enableEnvProbeLights: bool;
}
struct SWorldSkyboxParameters
{
    var sunMesh: CMesh;
    var sunMaterial: CMaterialInstance;
    var moonMesh: CMesh;
    var moonMaterial: CMaterialInstance;
    var skyboxMesh: CMesh;
    var skyboxMaterial: CMaterialInstance;
    var cloudsMesh: CMesh;
    var cloudsMaterial: CMaterialInstance;
}
struct Sphere
{
    var CenterRadius2: Vector;
}
struct StorySceneCameraDefinition
{
    var cameraName: name;
    var cameraTransform: EngineTransform;
    var cameraZoom: float;
    var cameraFov: float;
    var enableCameraNoise: bool;
    var dofFocusDistFar: float;
    var dofBlurDistFar: float;
    var dofIntensity: float;
    var dofFocusDistNear: float;
    var dofBlurDistNear: float;
    var sourceSlotName: name;
    var targetSlotName: name;
    var sourceEyesHeigth: float;
    var targetEyesLS: Vector;
    var dof: ApertureDofParams;
    var bokehDofParams: SBokehDofParams;
    var genParam: CEventGeneratorCameraParams;
    var cameraAdjustVersion: byte;
}
struct Vector
{
    var X: float;
    var Y: float;
    var Z: float;
    var W: float;
}
struct Vector3
{
    var X: float;
    var Y: float;
    var Z: float;
}
struct VoicetagAppearancePair
{
    var voicetag: name;
    var appearance: name;
}
