state Base in CNewNPC
{
}
state ReactingBase in CNewNPC extends Base
{
}
state Base in CPlayer
{
    function CreateNoSaveLock();
}
state Movable in CPlayer extends Base
{
    var agent: CMovingAgentComponent;
}
state PostUseVehicle in CPlayer extends Base
{
    function HACK_DeactivatePhysicsRepresentation();
    function HACK_ActivatePhysicsRepresentation();
}
state UseVehicle in CPlayer extends Base
{
}