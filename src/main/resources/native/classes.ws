abstract class ISerializable extends IReferencable
{
}
class CJournalPath extends ISerializable
{
}
class CSectorData extends ISerializable
{
}
class IScriptable extends ISerializable
{
    function PushState(stateName: name);
    function PopState(optional popAll: bool);
    function GotoState(optional newState: name, optional keepStack: bool, optional forceEvents: bool);
    function GotoStateAuto();
    function GetState(stateName: name): CScriptableState;
    function GetCurrentState(): CScriptableState;
    function GetCurrentStateName(): name;
    function IsInState(stateName: name): bool;
    function LockEntryFunction(lock: bool);
    function SetCleanupFunction(functionName: name);
    function ClearCleanupFunction();
    function DebugDumpEntryFunctionCalls(enabled: bool);
    function LogStates();
    function ToString(): string;
}
class CAIStorageAnimalData extends IScriptable
{
}
class CAIStorageHorseData extends IScriptable
{
}
class CAIStorageRiderData extends IScriptable
{
    var sharedParams: CHorseRiderSharedParams;
    var ridingManagerMountError: bool;
    var ridingManagerCurrentTask: ERidingManagerTask;
    var horseScriptedActionTree: IAIActionTree;
    var ridingManagerDismountType: EDismountType;
    var ridingManagerInstantMount: bool;

    function PairWithTaggedHorse(actor: CActor, preferedHorseTag: name, range: float): bool;
    function OnInstantDismount(riderActor: CActor);
}
class CActionPointSelector extends IScriptable
{
}
class CCommunityActionPointSelector extends CActionPointSelector
{
}
class CHorseParkingActionPointSelector extends CActionPointSelector
{
    var apTags: TagList;
    var radius: float;
}
class CSimpleActionPointSelector extends CActionPointSelector
{
    var categories: array<name>;
    var apTags: TagList;
    var areaTags: TagList;
    var apAreaTag: name;
    var keepActionPointOnceSelected: bool;
    var useNearestMatchingAP: bool;
}
class CWanderActionPointSelector extends CActionPointSelector
{
    var categories: array<SEncounterActionPointSelectorPair>;
    var delay: float;
    var radius: float;
    var apTags: TagList;
    var areaTags: TagList;
    var apAreaTag: name;
    var chooseClosestAP: bool;
}
class CRainActionPointSelector extends CWanderActionPointSelector
{
}
class CHorseRiderSharedParams extends IScriptable
{
    var rider: CActor;
    var horse: CActor;
    var mountStatus: EVehicleMountStatus;
    var boat: EntityHandle;
    var vehicleSlot: EVehicleSlot;
}
class CNavigationReachabilityQueryInterface extends IScriptable
{
    function GetLastOutput(optional queryValidTime: float): EAsyncTestResult;
    function GetOutputClosestDistance(): float;
    function GetOutputClosestEntity(): CEntity;
    function TestActorsList(testType: ENavigationReachabilityTestType, originActor: CActor, list: array<CActor>, optional safeSpotTolerance: float, optional pathfindDinstanceLimit: float): EAsyncTestResult;
}
class CObject extends IScriptable
{
    function Clone(newParent: CObject): CObject;
    function GetParent(): CObject;
}
class AnimationTrajectoryPlayerScriptWrapper
{
    function Init(entity: CEntity, optional slotName: name);
    function Deinit();
    function SelectAnimation(input: SAnimationTrajectoryPlayerInput): SAnimationTrajectoryPlayerToken;
    function PlayAnimation(animationToken: SAnimationTrajectoryPlayerToken): bool;
    function Tick(dt: float);
    function UpdateCurrentPoint(pointWS: Vector);
    function UpdateCurrentPointM(l2w: Matrix, pointWS: Vector);
    function GetTime(): float;
    function IsPlayingAnimation(): bool;
    function IsBeforeSyncTime(): bool;
    function WaitForSyncTime(): bool;
    function WaitForFinish(): bool;
}
class CAIAttackRange
{
    //var `name`: ?;
    var rangeMax: float;
    var height: float;
    var angleOffset: float;
    var position: Vector;
    var checkLineOfSight: bool;
    var lineOfSightHeight: float;
    var useHeadOrientation: bool;

    function Test(sourceEntity: CGameplayEntity, targetEntity: CGameplayEntity): bool;
    function GatherEntities(sourceEntity: CGameplayEntity, out entities: array<CGameplayEntity>);
}
class CActionMoveAnimationProxy
{
    var isInitialized: bool;
    var isValid: bool;
    var duration: float;
    var prevTime: float;
    var currTime: float;
    var finished: bool;

    function IsInitialized(): bool;
    function IsValid(): bool;
    function IsFinished(): bool;
    function WillBeFinished(time: float): bool;
}
class CActionPointManager
{
    function HasPreferredNextAPs(currApID: SActionPointId): bool;
    function GetSeqNextActionPoint(currApID: SActionPointId): SActionPointId;
    function GetJobTree(apID: SActionPointId): CJobTree;
    function ResetItems(apID: SActionPointId);
    function GetGoToPosition(apID: SActionPointId, out placePos: Vector, out placeRot: float): bool;
    function GetActionExecutionPosition(apID: SActionPointId, out placePos: Vector, out placeRot: float): bool;
    function GetFriendlyAPName(apID: SActionPointId): string;
    function IsBreakable(apID: SActionPointId): bool;
    function GetPlacementImportance(apID: SActionPointId): EWorkPlacementImportance;
    function IsFireSourceDependent(apID: SActionPointId): bool;
}
class CAnimationManualSlotSyncInstance
{
    function RegisterMaster(definition: SAnimationSequenceDefinition): int;
    function RegisterSlave(definition: SAnimationSequenceDefinition): int;
    function StopSequence(index: int);
    function IsSequenceFinished(index: int): bool;
    function HasEnded(): bool;
    function BreakIfPossible(entity: CEntity): bool;
    function Update(deltaTime: float);
}
class CBaseDamage
{
    var hitLocation: Vector;
    var momentum: Vector;
    var causer: IScriptable;
    var attacker: CGameplayEntity;
    var victim: CGameplayEntity;
    var hitReactionAnimRequested: bool;
}
class CDamageData extends CBaseDamage
{
    var processedDmg: SProcessedDamage;
    var additiveHitReactionAnimRequested: bool;
    var customHitReactionRequested: bool;
    var isDoTDamage: bool;
}
class CBehTreeReactionEventData
{
    var eventName: name;
    var lifetime: float;
    var broadcastInterval: float;
    var distanceRange: float;
    var recipientCount: int;
}
class CBehTreeReactionManager
{
    var reactionScens: array<CReactionScene>;
    //var reactionEvents: ?;

    function CreateReactionEventIfPossible(invoker: CEntity, eventName: name, lifetime: float, distanceRange: float, broadcastInterval: float, recipientCount: int, skipInvoker: bool, optional setActionTargetOnBroadcast: bool, optional customCenter: Vector);
    function CreateReactionEvent(invoker: CEntity, eventName: name, lifetime: float, distanceRange: float, broadcastInterval: float, recipientCount: int, optional skipInvoker: bool, optional setActionTargetOnBroadcast: bool): bool;
    function CreateReactionEventCustomCenter(invoker: CEntity, eventName: name, lifetime: float, distanceRange: float, broadcastInterval: float, recipientCount: int, skipInvoker: bool, setActionTargetOnBroadcast: bool, customCenter: Vector): bool;
    function RemoveReactionEvent(invoker: CEntity, eventName: name): bool;
    function InitReactionScene(invoker: CEntity, eventName: name, lifetime: float, distanceRange: float, broadcastInterval: float, recipientCount: int): bool;
    function AddReactionSceneGroup(voiceset: string, group: name);
    function SuppressReactions(toggle: bool, areaTag: name);
}
class CR4ReactionManager extends CBehTreeReactionManager
{
    var rainReactionsEnabled: bool;
    var rainEventParams: CBehTreeReactionEventData;
}
class CCameraDirector
{
    function ViewCoordsToWorldVector(x: int, y: int, out rayStart: Vector, out rayDirection: Vector);
    function WorldVectorToViewCoords(worldPos: Vector, out x: int, out y: int);
    function WorldVectorToViewRatio(worldPos: Vector, out x: float, out y: float): bool;
    function GetCameraPosition(): Vector;
    function GetCameraRotation(): EulerAngles;
    function GetCameraForward(): Vector;
    function GetCameraRight(): Vector;
    function GetCameraUp(): Vector;
    function GetCameraHeading(): float;
    function GetCameraDirection(): Vector;
    function GetFov(): float;
    function GetTopmostCameraObject(): IScriptable;
    function GetTopmostCamera(): CCustomCamera;
}
class CR4CameraDirector extends CCameraDirector
{
}
class CCarryableItemsRegistry
{
}
class CCharacterStats
{
    function GetAttributeValue(attributeName: name, abilityTags: array<name>, optional withoutTags: bool): SAbilityAttributeValue;
    function GetAbilityAttributeValue(attributeName: name, abilityName: name): SAbilityAttributeValue;
    function AddAbility(abilityName: name, optional allowMultiple: bool): bool;
    function RemoveAbility(abilityName: name): bool;
    function HasAbility(abilityName: name, optional includeInventoryAbl: bool): bool;
    function HasAbilityWithTag(tag: name, optional includeInventoryAbl: bool): bool;
    function IsAbilityAvailableToBuy(abilityName: name): bool;
    function GetAbilities(out abilities: array<name>, optional includeInventoryAbl: bool);
    function GetAllAttributesNames(out attributes: array<name>);
    function GetAllContainedAbilities(out abilities: array<name>);
    function AddAbilityMultiple(abilityName: name, count: int);
    function RemoveAbilityMultiple(abilityName: name, count: int);
    function RemoveAbilityAll(abilityName: name);
    function GetAbilityCount(abilityName: name): int;
    function GetAbilitiesWithTag(tag: name, out abilitiesList: array<name>);
}
class CComboAspect
{
    //var `name`: ?;
    //var strings: ?;

    function CreateComboString(optional leftSide: bool): CComboString;
    function AddLinks(animationName: name, connections: array<name>);
    function AddLink(animationName: name, linkedAnimationName: name);
    function AddHit(animationName: name, hitAnimationName: name);
}
class CComboDefinition
{
    //var aspects: ?;

    function CreateComboAspect(comboAspect: name): CComboAspect;
    function DeleteComboAspect(comboAspect: name): bool;
    function FindComboAspect(comboAspect: name): CComboAspect;
}
class CComboPlayer
{
    function Build(definition: CComboDefinition, entity: CEntity): bool;
    function Init(): bool;
    function Deinit();
    function Update(timeDelta: float): bool;
    function Pause();
    function Unpause();
    function IsPaused(): bool;
    function PauseSlider();
    function UnpauseSlider();
    function IsSliderPaused(): bool;
    function PlayAttack(comboAspect: name): bool;
    function StopAttack();
    function PlayHit(): bool;
    function SetDurationBlend(timeDelta: float);
    function UpdateTarget(attackId: int, pos: Vector, rot: float, optional deltaRotationPolicy: bool, optional useRotationScaling: bool);
}
class CComboString
{
    var attacks: array<SComboAnimationData>;
    var distAttacks: array<array<SComboAnimationData>>;
    var dirAttacks: array<array<array<SComboAnimationData>>>;
    var leftSide: bool;

    function AddAttack(animationName: name, optional distance: EAttackDistance);
    function AddDirAttack(animationName: name, direction: EAttackDirection, distance: EAttackDistance);
    function AddDirAttacks(animationNameFront: name, animationNameBack: name, animationNameLeft: name, animationNameRight: name, distance: EAttackDistance);
}
class CCreateEntityHelper
{
    function SetPostAttachedCallback(caller: IScriptable, funcName: name);
    function IsCreating(): bool;
    function Reset();
    function GetCreatedEntity(): CEntity;
}
class CR4CreateEntityHelper extends CCreateEntityHelper
{
}
class CCurve
{
    var color: Color;
    var dataBaseType: ECurveBaseType;
    //var `data.m_loop`: ?;

    function GetValue(time: float): float;
    function GetDuration(): float;
}
class CDLCManager
{
    var definitions: array<CDLCDefinition>;
    var mountedContent: array<IGameplayDLCMounter>;

    function GetDLCs(names: array<name>);
    function EnableDLC(id: name, isEnabled: bool);
    function IsDLCEnabled(id: name): bool;
    function IsDLCAvailable(id: name): bool;
    function GetDLCName(id: name): string;
    function GetDLCDescription(id: name): string;
    function SimulateDLCsAvailable(shouldSimulate: bool);
}
class CDebugAttributesManager
{
    function AddAttribute(debugName: name, propertyName: name, context: IScriptable, optional groupName: name): bool;
}
class CDefinitionsManagerAccessor
{
    function GetItemAbilitiesWithWeights(itemName: name, playerItem: bool, out abilities: array<name>, out weights: array<float>, out minAbilities: int, out maxAbilities: int);
    function GetItemHoldSlot(itemName: name, playerItem: bool): name;
    function GetItemCategory(itemName: name): name;
    function GetItemPrice(itemName: name): int;
    function GetItemEnhancementSlotCount(itemName: name): int;
    function GetItemUpgradeListName(itemName: name, playerItem: bool): name;
    function GetItemLocalisationKeyName(itemName: name): string;
    function GetItemLocalisationKeyDesc(itemName: name): string;
    function GetItemIconPath(itemName: name): string;
    function ItemHasTag(itemName: name, tag: name): bool;
    function GetItemsWithTag(tag: name): array<name>;
    function GetItemEquipTemplate(itemName: name): string;
    function GetUsableItemType(itemName: name): EUsableItemType;
    function TestWitchcraft();
    function ValidateLootDefinitions(listAllItemDefs: bool);
    function ValidateRecyclingParts(listAllItemDefs: bool);
    function ValidateCraftingDefinitions(listAllItemDefs: bool);
    function AddAllItems(optional category: name, optional depot: string, optional invisibleItems: bool);
    function GetAbilityAttributeValue(abilityName: name, attributeName: name, out valMin: SAbilityAttributeValue, out valMax: SAbilityAttributeValue);
    function GetAbilitiesAttributeValue(abilitiesNames: array<name>, attributeName: name, out valMin: SAbilityAttributeValue, out valMax: SAbilityAttributeValue, optional tags: array<name>);
    function GetAbilityTags(ability: name, out tags: array<name>);
    function GetAbilityAttributes(ability: name, out attrib: array<name>);
    function IsAbilityDefined(abilityName: name): bool;
    function GetContainedAbilities(abilityName: name, out abilities: array<name>);
    function GetUniqueContainedAbilities(abilities: array<name>, out outAbilities: array<name>);
    function AbilityHasTag(ability: name, tag: name): bool;
    function GetCustomDefinition(definition: name): SCustomNode;
    function GetAttributeValueAsInt(out node: SCustomNodeAttribute, out val: int): bool;
    function GetAttributeValueAsFloat(out node: SCustomNodeAttribute, out val: float): bool;
    function GetAttributeValueAsBool(out node: SCustomNodeAttribute, out val: bool): bool;
    function GetAttributeValueAsString(out node: SCustomNodeAttribute): string;
    function GetAttributeName(out node: SCustomNodeAttribute): name;
    function GetAttributeValueAsCName(out node: SCustomNodeAttribute): name;
    function GetSubNodeByAttributeValueAsCName(out node: SCustomNode, rootNodeName: name, attributeName: name, attributeValue: name): bool;
    function GetCustomDefinitionSubNode(out node: SCustomNode, subnode: name): SCustomNode;
    function FindAttributeIndex(out node: SCustomNode, attName: name): int;
    function GetCustomNodeAttributeValueString(out node: SCustomNode, attName: name, out val: string): bool;
    function GetCustomNodeAttributeValueName(out node: SCustomNode, attName: name, out val: name): bool;
    function GetCustomNodeAttributeValueInt(out node: SCustomNode, attName: name, out val: int): bool;
    function GetCustomNodeAttributeValueBool(out node: SCustomNode, attName: name, out val: bool): bool;
    function GetCustomNodeAttributeValueFloat(out node: SCustomNode, attName: name, out val: float): bool;
}
class CDropPhysicsSetup
{
    //var `name`: ?;
    //var particles: ?;
    var curves: array<SDropPhysicsCurves>;
}
class CEntityTemplateParam
{
    var wasIncluded: bool;
}
class CGameplayEntityParam extends CEntityTemplateParam
{
    //var `name`: ?;
    var overrideInherited: bool;
}
class CBloodTrailEffect extends CGameplayEntityParam
{
    var effect: name;

    function GetEffectName(): name;
}
class CFocusSoundParam extends CGameplayEntityParam
{
    var eventStart: name;
    var eventStop: name;
    var hearingAngle: float;
    var visualEffectBoneName: name;

    function GetEventStart(): name;
    function GetEventStop(): name;
    function GetHearingAngle(): float;
    function GetVisualEffectBoneName(): name;
}
class CMonsterParam extends CGameplayEntityParam
{
    var isTeleporting: bool;
    var canBeTargeted: bool;
    var canBeHitByFists: bool;
    var canBeStrafed: bool;
    var monsterCategory: int;
    var soundMonsterName: name;
}
class CFlyingSwarmScriptInput
{
    var groupList: array<CFlyingSwarmGroup>;

    function CreateGroup(toSpawnCount: int, spawnPoiType: name, groupState: name, optional fromOtherGroup_Id: CFlyingGroupId);
    function RemoveGroup(groupId: CFlyingGroupId);
    function MoveBoidToGroup(groupIdA: CFlyingGroupId, count: int, groupIdB: CFlyingGroupId);
}
abstract class CGame
{
    var activeWorld: CWorld;
    //var visualDebug: ?;
    //var inputManager: ?;
    //var timerScriptKeyword: ?;
    var gameResource: CGameResource;

    function DebugActivateContent(contentToActivate: name);
    function IsFinalBuild(): bool;
    function IsActive(): bool;
    function IsPaused(): bool;
    function IsPausedForReason(reason: string): bool;
    function IsStopped(): bool;
    function IsLoadingScreenVideoPlaying(): bool;
    function Pause(reason: string);
    function Unpause(reason: string);
    function PauseCutscenes();
    function UnpauseCutscenes();
    function ExitGame();
    function IsActivelyPaused(): bool;
    function SetActivePause(flag: bool);
    function GetEngineTime(): EngineTime;
    function GetEngineTimeAsSeconds(): float;
    function GetTimeScale(optional forCamera: bool): float;
    function SetTimeScale(timeScale: float, sourceName: name, priority: int, optional affectCamera: bool, optional dontSave: bool);
    function RemoveTimeScale(sourceName: name);
    function RemoveAllTimeScales();
    function SetOrRemoveTimeScale(timeScale: float, sourceName: name, priority: int, optional affectCamera: bool);
    function LogTimeScales();
    function GetLocalTimeAsMilliseconds(): int;
    function GetGameTime(): GameTime;
    function SetGameTime(time: GameTime, callEvents: bool);
    function SetHoursPerMinute(f: float);
    function GetHoursPerMinute(): float;
    function GetDifficultyLevel(): int;
    function SetDifficultyLevel(amount: int);
    function IsVibrationEnabled(): bool;
    function SetVibrationEnabled(enabled: bool);
    function VibrateController(lowFreq: float, highFreq: float, duration: float);
    function StopVibrateController();
    function GetCurrentVibrationFreq(out lowFreq: float, out highFreq: float);
    function RemoveSpecificRumble(lowFreq: float, highFreq: float);
    function IsSpecificRumbleActive(lowFreq: float, highFreq: float): bool;
    function OverrideRumbleDuration(lowFreq: float, highFreq: float, newDuration: float);
    function IsPadConnected(): bool;
    function CreateEntity(entityTemplate: CEntityTemplate, pos: Vector, optional rot: EulerAngles, optional useAppearancesFromIncludes: bool, optional forceBehaviorPose: bool, optional doNotAdjustPlacement: bool, optional persistanceMode: EPersistanceMode, optional tagList: array<name>): CEntity;
    function GetNodeByTag(tag: name): CNode;
    function GetEntityByTag(tag: name): CEntity;
    function GetEntitiesByTag(tag: name, out entities: array<CEntity>);
    function GetNodesByTag(tag: name, out nodes: array<CNode>);
    function GetNodesByTags(tagsList: array<name>, out nodes: array<CNode>, optional matchAll: bool);
    function GetWorld(): CWorld;
    function IsFreeCameraEnabled(): bool;
    function EnableFreeCamera(flag: bool);
    function GetFreeCameraPosition(): Vector;
    function MoveFreeCamera(position: Vector, rotation: EulerAngles);
    function IsShowFlagEnabled(showFlag: EShowFlags): bool;
    function SetShowFlag(showFlag: EShowFlags, enabled: bool);
    function PlayCutsceneAsync(csName: string, actorNames: array<string>, actorEntities: array<CEntity>, csPos: Vector, csRot: EulerAngles, optional cameraNum: int): bool;
    function IsCurrentlyPlayingNonGameplayScene(): bool;
    function IsStreaming(): bool;
    function PlayCutscene(csName: string, actorNames: array<string>, actorEntities: array<CEntity>, csPos: Vector, csRot: EulerAngles, optional cameraNum: int): bool;
    function FadeOut(optional fadeTime: float, optional fadeColor: Color);
    function FadeIn(optional fadeTime: float);
    function FadeOutAsync(optional fadeTime: float, optional fadeColor: Color);
    function FadeInAsync(optional fadeTime: float);
    function IsFading(): bool;
    function IsBlackscreen(): bool;
    function HasBlackscreenRequested(): bool;
    function SetFadeLock(lockName: string);
    function ResetFadeLock(lockName: string);
    function UnlockAchievement(achName: name): bool;
    function LockAchievement(achName: name): bool;
    function NoticeAchievementProgress(achName: name, countNew: int, optional towards: int): bool;
    function GetUnlockedAchievements(out unlockedAchievments: array<name>);
    function GetAllAchievements(out unlockedAchievments: array<name>);
    function IsAchievementUnlocked(achievement: name): bool;
    function ToggleUserProfileManagerInputProcessing(enabled: bool);
    function IsCheatEnabled(cheatFeature: ECheats): bool;
    function ReloadGameplayConfig();
    function GetGameplayChoice(): bool;
    function GetGameplayConfigFloatValue(propName: name): float;
    function GetGameplayConfigBoolValue(propName: name): bool;
    function GetGameplayConfigIntValue(propName: name): int;
    function GetGameplayConfigEnumValue(propName: name): int;
    function SetAIObjectsLooseTime(time: float);
    function AddInitialFact(factName: string);
    function RemoveInitialFact(faceName: string);
    function ClearInitialFacts();
    function GetCurrentViewportResolution(out width: int, out height: int);
    function SetSingleShotLoadingScreen(contextName: name, optional initString: string, optional videoToPlay: string);
    function ToggleRTEnabled();
    function GetHDRSupported(): bool;
    function SetHDRMenuActive(hdrMenuActive: bool);
    function IsIntelGPU(): bool;
    function GetRTEnabled(): bool;
    function GetRTSupported(): bool;
    function GetHairWorksEnabled(): bool;
    function GetDLSSEnabled(): bool;
    function GetFSREnabled(): bool;
    function GetTAAEnabled(): bool;
    function GetXESSEnabled(): bool;
    function GetRTAOEnabled(): bool;
    function GetRTREnabled(): bool;
    function GetDLSSGEnabled(): bool;
    function GetDLSSGSupported(): bool;
    function GetReflexEnabled(): bool;
    function GetReflexSupported(): bool;
    function GetMotionBlurEnabled(): bool;
    function GetGameResource(): CGameResource;
    function GetPhotomodeEnabled(): bool;
    function SetPhotomodeEnabled(photomodeEnabled: bool);
    function IsHdrSupported(): bool;
    function IsRayTracingSupported(): bool;
}
abstract class CCommonGame extends CGame
{
    var player: CPlayer;
    //var dlcManager: ?;

    function EnableSubtitles(enable: bool);
    function AreSubtitlesEnabled(): bool;
    function GetCommunitySystem(): CCommunitySystem;
    function GetAttackRangeForEntity(sourceEntity: CEntity, optional attackName: name): CAIAttackRange;
    function GetReactionsMgr(): CReactionsManager;
    function GetBehTreeReactionManager(): CBehTreeReactionManager;
    function GetIngredientCategoryElements(catName: name, out names: array<name>, out priorities: array<int>);
    function IsIngredientCategorySpecified(catName: name): bool;
    function GetIngredientCathegories(): array<name>;
    function GetSetItems(setName: name): array<name>;
    function GetItemSetAbilities(itemName: name): array<name>;
    function GetDefinitionsManager(): CDefinitionsManagerAccessor;
    function QueryExplorationSync(entity: CEntity, optional queryContext: SExplorationQueryContext): SExplorationQueryToken;
    function QueryExplorationFromObjectSync(entity: CEntity, object: CEntity, optional queryContext: SExplorationQueryContext): SExplorationQueryToken;
    function GetGlobalAttitude(srcGroup: name, dstGroup: name): EAIAttitude;
    function SetGlobalAttitude(srcGroup: name, dstGroup: name, attitude: EAIAttitude): bool;
    function GetReward(rewardName: name, out rewrd: SReward): bool;
    function GiveReward(rewardName: name, targetEntity: CEntity);
    function ConvertToStrayActor(actor: CActor): bool;
    function CreateEntityAsync(createEntityHelper: CCreateEntityHelper, entityTemplate: CEntityTemplate, pos: Vector, optional rot: EulerAngles, optional useAppearancesFromIncludes: bool, optional forceBehaviorPose: bool, optional doNotAdjustPlacement: bool, optional persistanceMode: EPersistanceMode, optional tagList: array<name>): int;
    function LoadLastGameInit(optional isFromDeathScreen: bool);
    function LoadGameInit(info: SSavegameInfo, isFromDeathScreen: bool);
    function CanStartStandaloneDLC(dlc: name): bool;
    function InitStandaloneDLCLoading(dlc: name, difficulty: int): ELoadGameResult;
    function GetLoadGameProgress(): ELoadGameResult;
    function ListSavedGames(out fileNames: array<SSavegameInfo>, optional saveType: int): bool;
    function GetRecentListSG(out fileNames: array<SSavegameInfo>): bool;
    function GetDisplayNameForSavedGame(savegame: SSavegameInfo): string;
    function SaveGame(type: ESaveGameType, slot: int);
    function GetNumSaveSlots(type: ESaveGameType): int;
    function DeleteSavedGame(savegame: SSavegameInfo);
    function GetContentRequiredByLastSave(out content: array<name>);
    function GetSaveInSlot(type: ESaveGameType, slot: int, out info: SSavegameInfo): bool;
    function ShouldShowSaveCompatibilityWarning(): bool;
    function RequestNewGame(gameResourceFilename: string): bool;
    function RequestEndGame();
    function RequestExit();
    function GetGameResourceList(): array<string>;
    function GetGameRelease(): string;
    function GetCurrentLocale(): string;
    function GetAllNPCs(out npcs: array<CNewNPC>);
    function GetAPManager(): CActionPointManager;
    function GetStorySceneSystem(): CStorySceneSystem;
    function GetActorByTag(tag: name): CActor;
    function GetNPCByTag(tag: name): CNewNPC;
    function GetActorsByTag(tag: name, out actors: array<CActor>);
    function GetNPCsByTag(tag: name, out npcs: array<CNewNPC>);
    function GetGameLanguageId(out audioLang: int, out subtitleLang: int);
    function GetGameLanguageName(out audioLang: string, out subtitleLang: string);
    function IsLanguageArabic(): bool;
    function GetGameLanguageIndex(out audioLang: int, out subtitleLang: int);
    function GetAllAvailableLanguages(out textLanguages: array<string>, out speechLanguages: array<string>);
    function SwitchGameLanguageByIndex(audioLang: int, subtitleLang: int);
    function ReloadLanguage();
    function IsGameTimePaused(): bool;
    function AddStateChangeRequest(entityTag: name, modifier: IEntityStateChangeRequest);
    function CreateNoSaveLock(reason: string, out lock: int, optional unique: bool, optional allowCheckpoints: bool);
    function ReleaseNoSaveLock(lock: int);
    function ReleaseNoSaveLockByName(lockName: string);
    function AreSavesLocked(): bool;
    function IsNewGame(): bool;
    function IsNewGameInStandaloneDLCMode(): bool;
    function IsNewGamePlusEnabled(): bool;
    function ConfigSave();
    function AreSavesInitialized(): bool;
    function IsInvertCameraX(): bool;
    function IsInvertCameraY(): bool;
    function SetInvertCameraX(invert: bool);
    function SetInvertCameraY(invert: bool);
    function SetInvertCameraXOnMouse(invert: bool);
    function SetInvertCameraYOnMouse(invert: bool);
    function IsCameraAutoRotX(): bool;
    function IsCameraAutoRotY(): bool;
    function SetCameraAutoRotX(flag: bool);
    function SetCameraAutoRotY(flag: bool);
    function ChangePlayer(playerTemplate: string, optional appearance: name);
    function ScheduleWorldChangeToMapPin(worldPath: string, mapPinName: name);
    function ScheduleWorldChangeToPosition(worldPath: string, position: Vector, rotation: EulerAngles);
    function ForceUIAnalog(value: bool);
    function RequestMenu(menuName: name, optional initData: IScriptable);
    function CloseMenu(menuName: name);
    function RequestPopup(popupName: name, optional initData: IScriptable);
    function ClosePopup(popupName: name);
    function GetHud(): CHud;
    function GetInGameConfigWrapper(): CInGameConfigWrapper;
    function ImportSave(savegameInfo: SSavegameInfo): bool;
    function ListW2SavedGames(out savedGames: array<SSavegameInfo>): bool;
    function TestNoCreaturesOnLocation(pos: Vector, radius: float, optional ignoreActor: CActor): bool;
    function TestNoCreaturesOnLine(pos0: Vector, pos1: Vector, lineWidth: float, optional ignoreActor0: CActor, optional ignoreActor1: CActor, optional ignoreGhostCharacters: bool): bool;
    function RequestAutoSave(reason: string, force: bool);
    function CalculateTimePlayed(): GameTime;
    function RequestScreenshotData(save: SSavegameInfo);
    function IsScreenshotDataReady(): bool;
    function FreeScreenshotData();
    function CenterMouse();
    function MoveMouseTo(xpos: float, ypos: float);
    function GetUIHorizontalPlusFrameScale(): float;
    function GetDLCManager(): CDLCManager;
    function AreConfigResetInThisSession(): bool;
    function HasShownConfigChangedMessage(): bool;
    function SetHasShownConfigChangedMessage(value: bool);
    function GetApplicationVersion(): string;
    function IsSoftwareCursor(): bool;
    function ShowHardwareCursor();
    function HideHardwareCursor();
    function GetAchievementsDisabled(): bool;
    function GetIsDLSSSupported(): bool;
    function GetIsXESSSupported(): bool;
    function VisitWeibo();
    function RequestVoiceLangDownload(lockName: string);
    function GetVoiceLangDownloadStatus(lockName: string): int;
    function UpdateCrossProgressionValue(value: string);
    function RefreshCrossProgressionSavesList();
}
class CR4Game extends CCommonGame
{
    var horseCamera: CCustomCamera;
    //var telemetryScriptProxy: ?;
    //var secondScreenScriptProxy: ?;
    //var kinectSpeechRecognizerListenerScriptProxy: ?;
    //var ticketsDefaultConfiguration: ?;
    //var globalEventsScriptsDispatcher: ?;
    //var worldDLCExtender: ?;
    var photomodeEffects: CR4PhotomodeEffects;
    var globalTicketSource: CGlabalTicketSourceProvider;
    var carryableItemsRegistry: CCarryableItemsRegistry;
    var params: W3GameParams;

    function ShowSteamControllerBindingPanel(): bool;
    function ActivateHorseCamera(activate: bool, blendTime: float, optional instantMount: bool);
    function GetFocusModeController(): CFocusModeController;
    function SetTriggerEffect(t: int, mode: int, param: array<Vector>);
    function ClearTriggerEffect(t: int);
    function HapticStart(t: string);
    function GetSurfacePostFX(): CGameplayFXSurfacePost;
    function GetCommonMapManager(): CCommonMapManager;
    function GetJournalManager(): CWitcherJournalManager;
    function GetLootManager(): CR4LootManager;
    function GetInteractionsManager(): CInteractionsManager;
    function GetCityLightManager(): CCityLightManager;
    function GetSecondScreenManager(): CR4SecondScreenManagerScriptProxy;
    function GetGuiManager(): CR4GuiManager;
    function GetGlobalEventsScriptsDispatcher(): CR4GlobalEventsScriptsDispatcher;
    function GetFastForwardSystem(): CGameFastForwardSystem;
    function NotifyOpeningJournalEntry(jorunalEntry: CJournalBase);
    function StartSepiaEffect(fadeInTime: float): bool;
    function StopSepiaEffect(fadeOutTime: float): bool;
    function GetWindAtPoint(point: Vector): Vector;
    function GetWindAtPointForVisuals(point: Vector): Vector;
    function GetGameCamera(): CCustomCamera;
    function GetBuffImmunitiesForActor(actor: CActor): CBuffImmunity;
    function GetMonsterParamsForActor(actor: CActor, out monsterCategory: EMonsterCategory, out soundMonsterName: name, out isTeleporting: bool, out canBeTargeted: bool, out canBeHitByFists: bool): bool;
    function GetMonsterParamForActor(actor: CActor, out val: CMonsterParam): bool;
    function GetVolumePathManager(): CVolumePathManager;
    function SummonPlayerHorse(teleportToSafeSpot: bool, createEntityHelper: CR4CreateEntityHelper);
    function ToggleMenus();
    function ToggleInput();
    function GetResourceAliases(out aliases: array<string>);
    function GetKinectSpeechRecognizer(): CR4KinectSpeechRecognizerListenerScriptProxy;
    function GetTutorialSystem(): CR4TutorialSystem;
    function DisplaySystemHelp();
    function DisplayStore();
    function IsExpansionPackMenuSupported(): bool;
    function DisplayStoreExpansionPack(dlcName: name);
    function TryDownloadExpansionPack(dlcName: name);
    function DisplayUserProfileSystemDialog();
    function SetRichPresence(presence: name);
    function OnUserDialogCallback(message: int, action: int);
    function SaveUserSettings();
    function GetPhotomodeEffects(): CR4PhotomodeEffects;
    function GetPhotomodeCamera(): CCustomCamera;
    function TakeScreenshot();
    function GetDriveType(): int;
    function PauseGameplayFx(enable: bool);
    function GetPlatform(): int;
    function GetGalaxyPf(): int;
    function UsesRedLauncher(): bool;
    function IsGalaxyUserSignedIn(): bool;
    function HasInternetConnection(): bool;
    function SetActiveUserPromiscuous();
    function ChangeActiveUser();
    function DebugFirstLaunch();
    function DebugForceCrash(crashType: string): bool;
    function GetActiveUserDisplayName(): string;
    function IsContentAvailable(content: name): bool;
    function ProgressToContentAvailable(content: name): int;
    function ShouldForceInstallVideo(): bool;
    function IsDebugQuestMenuEnabled(): bool;
    function EnableNewGamePlus(enable: bool);
    function StartNewGamePlus(save: SSavegameInfo): ENewGamePlusStatus;
    function GetGwintManager(): CR4GwintManager;
    function GetWorldDLCExtender(): CR4WorldDLCExtender;
    function WritePGO(optional counter: int);
}
class CGlabalTicketSourceProvider
{
}
abstract class CGraphBlock
{
}
class CQuestGraphBlock extends CGraphBlock
{
    //var sockets: ?;
    //var `name`: ?;
    var comment: string;
    var forceKeepLoadingScreen: bool;
    var guid: CGUID;
    var cachedConnections: array<SCachedConnections>;
    var hasPatchOutput: bool;
    var hasTerminationInput: bool;
}
class IQuestCombatManagerBaseBlock extends CQuestGraphBlock
{
    var npcTags: TagList;
    var overrideGuardArea: bool;
    var guardAreaTag: name;
    var pursuitAreaTag: name;
    var pursuitRange: float;
}
abstract class CGuiManager
{
}
class CR4GuiManager extends CGuiManager
{
    function IsAnyMenu(): bool;
    function GetRootMenu(): CR4Menu;
    function GetPopup(popupName: name): CR4Popup;
    function GetPopupList(out popupNames: array<name>);
    function SendCustomUIEvent(eventName: name);
    function SetSceneEntityTemplate(template: CEntityTemplate, optional animationName: name);
    function ApplyAppearanceToSceneEntity(appearanceName: name);
    function UpdateSceneEntityItems(items: array<SItemUniqueId>, enhancements: array<SGuiEnhancementInfo>);
    function SetSceneCamera(cameraPosition: Vector, cameraRotation: EulerAngles);
    function SetupSceneCamera(lookAtPos: Vector, cameraRotation: EulerAngles, distance: float, fov: float);
    function SetEntityTransform(position: Vector, rotation: EulerAngles, scale: Vector);
    function SetSceneEnvironmentAndSunPosition(envDef: CEnvironmentDefinition, sunRotation: EulerAngles);
    function EnableScenePhysics(enable: bool);
    function SetBackgroundTexture(texture: CResource);
    function RequestClearScene();
    function VisitSignInPage();
    function GalaxyQRSignInInitiate();
    function GalaxyQRSignInCancel();
    function GalaxyUnlinkAccounts();
    function SyncGalaxySlot(saveListIndex: int);
    function ShowCloudModal();
    function GetGalaxyRewardsList(out fileNames: array<int>): bool;
    function GetGalaxyRewardDesc(rewID: int, out rewTitle: string, out rewDesc: string);
    function ForceProcessFlashStorage();
    function PlayFlashbackVideoAsync(videoFile: string, optional looped: bool);
    function CancelFlashbackVideo();
}
abstract class CGuiObject
{
}
abstract class CHud extends CGuiObject
{
    function GetHudFlash(): CScriptedFlashSprite;
    function GetHudFlashValueStorage(): CScriptedFlashValueStorage;
    function CreateHudModule(moduleName: string, optional userData: int);
    function DiscardHudModule(moduleName: string, optional userData: int);
    function GetHudModule(moduleName: string): CHudModule;
}
class CR4Hud extends CHud
{
    function ShowOneliner(text: string, entity: CEntity);
    function HideOneliner(entity: CEntity);
    function ForceInteractionUpdate();
}
abstract class CHudModule extends CGuiObject
{
    function GetModuleFlash(): CScriptedFlashSprite;
    function GetModuleFlashValueStorage(): CScriptedFlashValueStorage;
}
class CR4HudModule extends CHudModule
{
}
abstract class CMenu extends CGuiObject
{
    function GetMenuFlash(): CScriptedFlashSprite;
    function GetMenuFlashValueStorage(): CScriptedFlashValueStorage;
    function GetMenuInitData(): IScriptable;
    function GetMenuName(): name;
    function RequestSubMenu(menuName: name, optional initData: IScriptable);
    function CloseMenu();
}
class CR4Menu extends CMenu
{
    function GetSubMenu(): CMenu;
    function MakeModal(make: bool): bool;
    function SetRenderGameWorldOverride(override: bool);
}
abstract class CPopup extends CGuiObject
{
    function GetPopupFlash(): CScriptedFlashSprite;
    function GetPopupFlashValueStorage(): CScriptedFlashValueStorage;
    function GetPopupInitData(): IScriptable;
    function GetPopupName(): name;
    function ClosePopup();
}
class CR4Popup extends CPopup
{
    function MakeModal(make: bool): bool;
}
class CGwintMenuInitData
{
    var deckName: name;
    var difficulty: EGwintDifficultyMode;
    var aggression: EGwintAggressionMode;
    var allowMultipleMatches: bool;
    var forceFaction: eGwintFaction;
}
class CHorsePrediction
{
    function CollectPredictionInfo(entity: CNode, testDistance: float, direction: float, checkWater: bool): SPredictionInfo;
}
class CInGameConfigWrapper
{
    function GetGroupDisplayName(groupName: name): string;
    function GetGroupPresetsNum(groupName: name): int;
    function GetGroupPresetDisplayName(groupName: name, presetIdx: int): string;
    function ApplyGroupPreset(groupName: name, presetIdx: int);
    function ResetGroupToDefaults(groupName: name);
    function GetVarDisplayType(groupName: name, varName: name): string;
    function GetVarDisplayName(groupName: name, varName: name): string;
    function GetVarOptionsNum(groupName: name, varName: name): int;
    function GetVarOption(groupName: name, varName: name, optionIdx: int): string;
    function GetVarValue(groupCName: name, varCName: name): string;
    function GetVarValueByStr(groupNameStr: string, varCName: name): string;
    function SetVarValue(groupName: name, varName: name, varValue: string);
    function SetVarValueByStr(groupNameStr: string, varName: name, varValue: string);
    function GetVarNameByGroupName(groupName: name, varIdx: int): name;
    function GetVarsNumByGroupName(groupName: name): int;
    function IsVarVisible(groupName: name, varName: name): bool;
    function DoVarHasTag(groupName: name, varName: name, tag: name): bool;
    function GetGroupsNum(): int;
    function GetGroupName(groupIdx: int): name;
    function GetGroupIdx(groupName: string): int;
    function GetVarsNum(groupIdx: int): int;
    function GetVarName(groupIdx: int, varIdx: int): name;
    function IsGroupVisible(groupName: name): bool;
    function DoGroupHasTag(groupName: name, tag: name): bool;
    function IsTagActive(tag: name): bool;
    function ActivateScriptTag(tag: name);
    function DeactivateScriptTag(tag: name);
    function WriteIniFile(iniFileName: string, key: string, newValue: string);
    function GetEntriesNumForOption(groupName: name, varName: name, optionId: int): int;
    function GetEntryNameForOption(groupName: name, varName: name, optionId: int, entryId: int): name;
    function GetCurrentOptionId(groupName: name, varName: name): int;
}
class CInputManager
{
    //var gestureSystem: ?;

    function GetLastActivationTime(actionName: name): float;
    function GetActionValue(actionName: name): float;
    function GetAction(actionName: name): SInputAction;
    function ClearIgnoredInput();
    function IsInputIgnored(actionName: name): bool;
    function RegisterListener(listener: IScriptable, eventName: name, actionName: name);
    function UnregisterListener(listener: IScriptable, actionName: name);
    function SetContext(contextName: name);
    function GetContext(): name;
    function StoreContext(newContext: name);
    function RestoreContext(storedContext: name, contextCouldChange: bool);
    function EnableLog(val: bool);
    function LastUsedPCInput(): bool;
    function LastUsedGamepad(): bool;
    function GetLastUsedDeviceName(): name;
    function UsesPlaystationPad(): bool;
    function ForceDeactivateAction(actionName: name);
    function GetPCKeysForAction(actionName: name, out outKeys: array<EInputKey>);
    function GetPadKeysForAction(actionName: name, out outKeys: array<EInputKey>);
    function GetCurrentKeysForAction(actionName: name, out outKeys: array<EInputKey>);
    function GetPCKeysForActionStr(actionName: string, out outKeys: array<EInputKey>);
    function GetPadKeysForActionStr(actionName: string, out outKeys: array<EInputKey>);
    function GetCurrentKeysForActionStr(actionName: string, out outKeys: array<EInputKey>);
    function SuppressPropagatingEventAfterAction(actionName: name);
    function SetInvertCamera(invert: bool);
}
class CInterestPoint
{
    var fieldName: name;
    //var potentialField: ?;
}
class CScriptedInterestPoint extends CInterestPoint
{
}
class CInterestPointInstance
{
    var parentNode: CNode;
    var position: Vector;

    function GetParentPoint(): CInterestPoint;
    function GetWorldPosition(): Vector;
    function GetNode(): CNode;
    function GetGeneratedFieldName(): name;
    function GetFieldStrength(position: Vector): float;
    function SetFieldStrengthMultiplier(param: float);
    function GetFieldStrengthMultiplier(): float;
    function SetTestParameter(param: float);
    function GetTestParameter(): float;
}
abstract class CJournalBase
{
    var guid: CGUID;
    var baseName: string;
    var order: Uint32;
    var uniqueScriptIdentifier: name;

    function GetUniqueScriptTag(): name;
    function GetOrder(): int;
}
class CJournalCharacterGroup extends CJournalBase
{
}
abstract class CJournalChildBase extends CJournalBase
{
    var parentGuid: CGUID;
    var linkedParentGuid: CGUID;

    function GetLinkedParentGUID(): CGUID;
}
abstract class CJournalContainerEntry extends CJournalChildBase
{
    var index: byte;
}
class CJournalCharacterDescription extends CJournalContainerEntry
{
    var description: LocalizedString;
    var action: EJournalVisibilityAction;
    var active: bool;

    function GetDescriptionStringId(): int;
}
abstract class CJournalContainer extends CJournalContainerEntry
{
    //var children: ?;

    function GetChild(index: int): CJournalBase;
    function GetNumChildren(): int;
}
class CJournalCharacter extends CJournalContainer
{
    //var `name`: ?;
    var image: string;
    var importance: ECharacterImportance;
    //var entityTemplate: ?;
    var active: bool;

    function GetNameStringId(): int;
    function GetImagePath(): string;
    function GetCharacterImportance(): ECharacterImportance;
    function GetEntityTemplateFilename(): string;
}
class CJournalCreature extends CJournalContainer
{
    //var `name`: ?;
    var image: string;
    //var entityTemplate: ?;
    var itemsUsedAgainstCreature: array<name>;
    var active: bool;

    function GetNameStringId(): int;
    function GetImage(): string;
    function GetEntityTemplateFilename(): string;
    function GetItemsUsedAgainstCreature(): array<name>;
}
class CJournalCreatureDescriptionGroup extends CJournalContainer
{
}
class CJournalCreatureHuntingClueGroup extends CJournalContainer
{
}
class CJournalGlossary extends CJournalContainer
{
    var title: LocalizedString;
    var image: string;
    var active: bool;

    function GetTitleStringId(): int;
    function GetImagePath(): string;
}
class CJournalPlace extends CJournalContainer
{
    //var `name`: ?;
    var image: string;
    var active: bool;

    function GetNameStringId(): int;
    function GetImage(): string;
}
class CJournalQuest extends CJournalContainer
{
    var type: eQuestType;
    var contentType: EJournalContentType;
    var world: Uint32;
    var huntingQuestPath: CJournalPath;
    var title: LocalizedString;
    //var questPhase: ?;

    function GetTitleStringId(): int;
    function GetType(): eQuestType;
    function GetContentType(): EJournalContentType;
    function GetWorld(): int;
    function GetHuntingQuestCreatureTag(): name;
}
class CJournalQuestDescriptionGroup extends CJournalContainer
{
}
class CJournalQuestObjective extends CJournalContainer
{
    var title: LocalizedString;
    var image: string;
    var world: Uint32;
    var counterType: eQuestObjectiveType;
    var count: Uint32;
    var mutuallyExclusive: bool;
    var bookShortcut: name;
    var itemShortcut: name;
    var recipeShortcut: name;
    var monsterShortcut: CJournalPath;

    function GetTitleStringId(): int;
    function GetWorld(): int;
    function GetCount(): int;
    function GetCounterType(): eQuestObjectiveType;
    function IsMutuallyExclusive(): bool;
    function GetBookShortcut(): name;
    function GetItemShortcut(): name;
    function GetRecipeShortcut(): name;
    function GetMonsterShortcut(): CJournalBase;
    function GetParentQuest(): CJournalQuest;
}
class CJournalQuestPhase extends CJournalContainer
{
}
class CJournalStoryBookPage extends CJournalContainer
{
    var title: LocalizedString;
    var world: Uint32;
    var active: bool;

    function GetTitleStringId(): int;
}
class CJournalCreatureDescriptionEntry extends CJournalContainerEntry
{
    var description: LocalizedString;
    var active: bool;

    function GetDescriptionStringId(): int;
}
class CJournalCreatureHuntingClue extends CJournalContainerEntry
{
    var category: name;
    var clue: int;
    var active: bool;
}
class CJournalCreatureVitalSpotEntry extends CJournalContainerEntry
{
    var title: LocalizedString;
    var description: LocalizedString;
    var active: bool;

    function GetTitleStringId(): int;
    function GetDescriptionStringId(): int;
    function GetCreatureEntry(): CJournalCreature;
}
class CJournalGlossaryDescription extends CJournalContainerEntry
{
    var description: LocalizedString;
    var active: bool;

    function GetDescriptionStringId(): int;
}
class CJournalPlaceDescription extends CJournalContainerEntry
{
    var description: LocalizedString;
    var active: bool;

    function GetDescriptionStringId(): int;
}
class CJournalQuestDescriptionEntry extends CJournalContainerEntry
{
    var description: LocalizedString;
    var active: bool;

    function GetDescriptionStringId(): int;
}
class CJournalQuestMapPin extends CJournalContainerEntry
{
    var radius: float;
    var mapPinID: name;
    var type: EJournalMapPinType;
    var enabledAtStartup: bool;

    function GetMapPinID(): name;
    function GetRadius(): float;
}
class CJournalStoryBookPageDescription extends CJournalContainerEntry
{
    var videoFilename: string;
    var description: LocalizedString;
    var isFinal: bool;
    var active: bool;

    function GetVideoFilename(): string;
    function GetDescriptionStringId(): int;
}
class CJournalTutorial extends CJournalChildBase
{
    //var `name`: ?;
    var image: string;
    var video: string;
    var description: LocalizedString;
    var dlcLock: name;
    var active: bool;

    function GetDescriptionStringId(): int;
    function GetNameStringId(): int;
    function GetImagePath(): string;
    function GetVideoPath(): string;
    function GetDLCLock(): name;
}
class CJournalCreatureGroup extends CJournalBase
{
    //var `name`: ?;
    var image: string;

    function GetNameStringId(): int;
    function GetImage(): string;
}
class CJournalGlossaryGroup extends CJournalBase
{
}
class CJournalPlaceGroup extends CJournalBase
{
    //var `name`: ?;
    var image: string;

    function GetNameStringId(): int;
    function GetImage(): string;
}
class CJournalQuestGroup extends CJournalBase
{
    var title: LocalizedString;

    function GetTitleStringId(): int;
}
class CJournalStoryBookChapter extends CJournalBase
{
    var title: LocalizedString;
    var image: string;
    var active: bool;

    function GetTitleStringId(): int;
    function GetImage(): string;
}
class CJournalTutorialGroup extends CJournalBase
{
    //var `name`: ?;
    var image: string;

    function GetNameStringId(): int;
    function GetImage(): string;
}
class CMoveTRGScript
{
    var agent: CMovingAgentComponent;
    var timeDelta: float;

    function SetHeadingGoal(out goal: SMoveLocomotionGoal, heading: Vector);
    function SetOrientationGoal(out goal: SMoveLocomotionGoal, orientation: float, optional alwaysSet: bool);
    function SetSpeedGoal(out goal: SMoveLocomotionGoal, speed: float);
    function SetMaxWaitTime(out goal: SMoveLocomotionGoal, time: float);
    function MatchDirectionWithOrientation(out goal: SMoveLocomotionGoal, enable: bool);
    function SetFulfilled(out goal: SMoveLocomotionGoal, isFulfilled: bool);
    function Seek(pos: Vector): Vector;
    function Flee(pos: Vector): Vector;
    function Pursue(agent: CMovingAgentComponent): Vector;
    function FaceTarget(pos: Vector): Vector;
}
class CMovementAdjustor
{
    function IsRequestActive(ticket: SMovementAdjustmentRequestTicket): bool;
    function HasAnyActiveRequest(): bool;
    function HasAnyActiveRotationRequests(): bool;
    function HasAnyActiveTranslationRequests(): bool;
    function Cancel(ticket: SMovementAdjustmentRequestTicket);
    function CancelByName(requestName: name);
    function CancelAll();
    function CreateNewRequest(optional requestName: name): SMovementAdjustmentRequestTicket;
    function GetRequest(requestName: name): SMovementAdjustmentRequestTicket;
    function AddOneFrameTranslationVelocity(translationVelocity: Vector);
    function AddOneFrameRotationVelocity(rotationVelocity: EulerAngles);
    function BlendIn(ticket: SMovementAdjustmentRequestTicket, blendInTime: float);
    function KeepActiveFor(ticket: SMovementAdjustmentRequestTicket, duration: float);
    function AdjustmentDuration(ticket: SMovementAdjustmentRequestTicket, duration: float);
    function Continuous(ticket: SMovementAdjustmentRequestTicket);
    function DontEnd(ticket: SMovementAdjustmentRequestTicket);
    function BaseOnNode(ticket: SMovementAdjustmentRequestTicket, onNode: CNode);
    function BindToEvent(ticket: SMovementAdjustmentRequestTicket, eventName: name, optional adjustDurationOnNextEvent: bool);
    function BindToEventAnimInfo(ticket: SMovementAdjustmentRequestTicket, animInfo: SAnimationEventAnimInfo, optional bindOnly: bool);
    function ScaleAnimation(ticket: SMovementAdjustmentRequestTicket, optional scaleAnimation: bool, optional scaleLocation: bool, optional scaleRotation: bool);
    function ScaleAnimationLocationVertically(ticket: SMovementAdjustmentRequestTicket, optional scaleAnimationLocationVertically: bool);
    function DontUseSourceAnimation(ticket: SMovementAdjustmentRequestTicket, optional dontUseSourceAnimation: bool);
    function UpdateSourceAnimation(ticket: SMovementAdjustmentRequestTicket, animInfo: SAnimationEventAnimInfo);
    function CancelIfSourceAnimationUpdateIsNotUpdated(ticket: SMovementAdjustmentRequestTicket, optional cancelIfSourceAnimationUpdateIsNotUpdated: bool);
    function SyncPointInAnimation(ticket: SMovementAdjustmentRequestTicket, optional syncPointTime: float);
    function UseBoneForAdjustment(ticket: SMovementAdjustmentRequestTicket, optional boneName: name, optional useContinuously: bool, optional useBoneForLocationAdjustmentWeight: float, optional useBoneForRotationAdjustmentWeight: float, optional useBoneToMatchTargetHeadingWeight: float);
    function MatchEntitySlot(ticket: SMovementAdjustmentRequestTicket, entity: CEntity, slotName: name);
    function KeepLocationAdjustmentActive(ticket: SMovementAdjustmentRequestTicket);
    function ReplaceTranslation(ticket: SMovementAdjustmentRequestTicket, optional replaceTranslation: bool);
    function ShouldStartAt(ticket: SMovementAdjustmentRequestTicket, atLocation: Vector);
    function SlideTo(ticket: SMovementAdjustmentRequestTicket, targetLocation: Vector);
    function SlideBy(ticket: SMovementAdjustmentRequestTicket, byVector: Vector);
    function SlideTowards(ticket: SMovementAdjustmentRequestTicket, node: CNode, optional minDistance: float, optional maxDistance: float);
    function SlideToEntity(ticket: SMovementAdjustmentRequestTicket, entity: CEntity, optional boneName: name, optional minDistance: float, optional maxDistance: float);
    function MaxLocationAdjustmentSpeed(ticket: SMovementAdjustmentRequestTicket, maxSpeed: float, optional maxSpeedZ: float);
    function MaxLocationAdjustmentDistance(ticket: SMovementAdjustmentRequestTicket, optional throughSpeed: bool, optional locationAdjustmentMaxDistanceXY: float, optional locationAdjustmentMaxDistanceZ: float);
    function AdjustLocationVertically(ticket: SMovementAdjustmentRequestTicket, optional adjustLocationVertically: bool);
    function KeepRotationAdjustmentActive(ticket: SMovementAdjustmentRequestTicket);
    function ReplaceRotation(ticket: SMovementAdjustmentRequestTicket, optional replaceRotation: bool);
    function ShouldStartFacing(ticket: SMovementAdjustmentRequestTicket, targetHeading: float);
    function RotateTo(ticket: SMovementAdjustmentRequestTicket, targetHeading: float);
    function RotateBy(ticket: SMovementAdjustmentRequestTicket, byHeading: float);
    function RotateTowards(ticket: SMovementAdjustmentRequestTicket, node: CNode, optional offsetHeading: float);
    function MatchMoveRotation(ticket: SMovementAdjustmentRequestTicket);
    function MaxRotationAdjustmentSpeed(ticket: SMovementAdjustmentRequestTicket, rotationAdjustmentMaxSpeed: float);
    function SteeringMayOverrideMaxRotationAdjustmentSpeed(ticket: SMovementAdjustmentRequestTicket, optional steeringMayOverrideMaxRotationAdjustmentSpeed: bool);
    function LockMovementInDirection(ticket: SMovementAdjustmentRequestTicket, heading: float);
    function RotateExistingDeltaLocation(ticket: SMovementAdjustmentRequestTicket, optional rotateExistingDeltaLocation: bool);
    function NotifyScript(ticket: SMovementAdjustmentRequestTicket, notifyObject: IScriptable, eventName: name, notify: EMovementAdjustmentNotify);
    function DontNotifyScript(ticket: SMovementAdjustmentRequestTicket, notifyObject: IScriptable, eventName: name, notify: EMovementAdjustmentNotify);
}
abstract class CNode
{
    var tags: TagList;
    var transform: EngineTransform;
    //var transformParent: ?;
    var guid: CGUID;

    function GetName(): string;
    function GetLocalPosition(): Vector;
    function GetLocalRotation(): EulerAngles;
    function GetLocalScale(): Vector;
    function GetLocalToWorld(): Matrix;
    function GetWorldPosition(): Vector;
    function GetWorldRotation(): EulerAngles;
    function GetWorldForward(): Vector;
    function GetWorldRight(): Vector;
    function GetWorldUp(): Vector;
    function GetHeading(): float;
    function GetHeadingVector(): Vector;
    function HasTag(tag: name): bool;
    function GetTags(): array<name>;
    function SetTags(tags: array<name>);
    function GetTagsString(): string;
}
abstract class CComponent extends CNode
{
    //var `name`: ?;
    var isStreamed: bool;

    function GetEntity(): CEntity;
    function IsEnabled(): bool;
    function SetEnabled(flag: bool);
    function SetPosition(position: Vector);
    function SetRotation(rotation: EulerAngles);
    function SetScale(scale: Vector);
    function HasDynamicPhysic(): bool;
    function HasCollisionType(collisionTypeName: name, optional actorIndex: int, optional shapeIndex: int): bool;
    function GetPhysicalObjectLinearVelocity(optional actorIndex: int): Vector;
    function GetPhysicalObjectAngularVelocity(optional actorIndex: int): Vector;
    function SetPhysicalObjectLinearVelocity(velocity: Vector, optional actorIndex: int): bool;
    function SetPhysicalObjectAngularVelocity(velocity: Vector, optional actorIndex: int): bool;
    function GetPhysicalObjectMass(optional actorIndex: int): float;
    function ApplyTorqueToPhysicalObject(torque: Vector, optional actorIndex: int);
    function ApplyForceAtPointToPhysicalObject(force: Vector, point: Vector, optional actorIndex: int);
    function ApplyLocalImpulseToPhysicalObject(impulse: Vector, optional actorIndex: int);
    function ApplyTorqueImpulseToPhysicalObject(impulse: Vector, optional actorIndex: int);
    function GetPhysicalObjectBoundingVolume(out box: Box): bool;
    function SetShouldSave(shouldSave: bool);
}
class CAnimatedComponent extends CComponent
{
    var ragdoll: CRagdoll;
    var ragdollCollisionType: CPhysicalCollision;
    var skeleton: CSkeleton;
    //var physicsRepresentation: ?;
    var animationSets: array<CSkeletalAnimationSet>;
    var behaviorInstanceSlots: array<SBehaviorGraphInstanceSlot>;
    var useExtractedMotion: bool;
    var stickRagdollToCapsule: bool;
    var includedInAllAppearances: bool;
    var savable: bool;
    var defaultBehaviorAnimationSlotNode: name;
    var isFrozenOnStart: bool;
    var defaultSpeedConfigKey: name;
    var overrideBudgetedTickDistance: float;
    var overrideDisableTickDistance: float;
    var runtimeBehaviorInstanceSlots: array<SBehaviorGraphInstanceSlot>;

    function ActivateBehaviors(names: array<name>): bool;
    function AttachBehavior(instanceName: name): bool;
    function DetachBehavior(instanceName: name): bool;
    function GetBehaviorVariable(varName: name): float;
    function GetBehaviorVectorVariable(varName: name): Vector;
    function SetBehaviorVariable(varName: name, varValue: float): bool;
    function SetBehaviorVectorVariable(varName: name, varValue: Vector): bool;
    function DisplaySkeleton(bone: bool, optional axis: bool, optional names: bool);
    function GetAnimationTimeMultiplier(): float;
    function DontUpdateByOtherAnimatedComponent();
    function UpdateByOtherAnimatedComponent(slaveComponent: CAnimatedComponent);
    function SetAnimationTimeMultiplier(mult: float);
    function GetMoveSpeedAbs(): float;
    function GetMoveSpeedRel(): float;
    function RaiseBehaviorEvent(eventName: name): bool;
    function RaiseBehaviorForceEvent(eventName: name): bool;
    function FindNearestBoneWS(out position: Vector): int;
    function FindNearestBoneToEdgeWS(a: Vector, b: Vector): int;
    function GetCurrentBehaviorState(optional instanceName: name): string;
    function FreezePose();
    function UnfreezePose();
    function FreezePoseFadeIn(fadeInTime: float);
    function UnfreezePoseFadeOut(fadeOutTime: float);
    function HasFrozenPose(): bool;
    function SyncTo(slaveComponent: CAnimatedComponent, ass: SAnimatedComponentSyncSettings): bool;
    function UseExtractedMotion(): bool;
    function SetUseExtractedMotion(use: bool);
    function HasRagdoll(): bool;
    function GetRagdollBoneName(actorIndex: int): name;
    function StickRagdollToCapsule(stick: bool);
    function PlaySlotAnimationAsync(animation: name, slotName: name, optional settings: SAnimatedComponentSlotAnimationSettings): bool;
    function PlaySkeletalAnimationAsync(animation: name, optional looped: bool): bool;
    function GetBoneMatrixMovementModelSpaceInAnimation(boneIndex: int, animation: name, time: float, deltaTime: float, out boneAtTimeMS: Matrix, out boneWithDeltaTimeMS: Matrix);
}
class CMovingAgentComponent extends CAnimatedComponent
{
    var ragdollRadius: float;
    var steeringBehavior: CMoveSteeringBehavior;
    var steeringControlledMovement: bool;
    var snapToNavigableSpace: bool;
    var physicalRepresentation: bool;
    //var movementAdjustor: ?;
    var triggerAutoActivator: bool;
    var triggerActivatorRadius: float;
    var triggerActivatorHeight: float;
    //var triggerChannels: ?;
    var triggerEnableCCD: bool;
    var triggerMaxSingleFrameDistance: float;

    function SetMaxMoveRotationPerSec(rotSpeed: float);
    function SetMoveType(moveType: EMoveType);
    function GetCurrentMoveSpeedAbs(): float;
    function TeleportBehindCamera(continueMovement: bool): bool;
    function EnableCombatMode(combat: bool): bool;
    function EnableVirtualController(virtualControllerName: name, enabled: bool);
    function SetVirtualRadius(radiusName: name, optional virtualControllerName: name);
    function SetVirtualRadiusImmediately(radiusName: name);
    function ResetVirtualRadius(optional virtualControllerName: name);
    function SetHeight(height: float);
    function ResetHeight();
    function CanGoStraightToDestination(destination: Vector): bool;
    function IsPositionValid(position: Vector): bool;
    function GetEndOfLineNavMeshPosition(pos: Vector, out outPos: Vector): bool;
    function IsEndOfLinePositionValid(position: Vector): bool;
    function GetPathPointInDistance(distance: float, out position: Vector): bool;
    function GetSpeed(): float;
    function GetRelativeMoveSpeed(): float;
    function GetMoveTypeRelativeMoveSpeed(moveType: EMoveType): float;
    function ForceSetRelativeMoveSpeed(relativeMoveSpeed: float);
    function SetGameplayRelativeMoveSpeed(relativeMoveSpeed: float);
    function SetGameplayMoveDirection(actorDirection: float);
    function SetDirectionChangeRate(directionChangeRate: float);
    function GetMaxSpeed(): float;
    function GetVelocity(): Vector;
    function GetVelocityBasedOnRequestedMovement(): Vector;
    function AdjustRequestedMovementDirectionPhysics(out directionWS: Vector, out shouldStop: bool, speed: float, angleToDeflect: float, freeSideDistanceRequired: float, out cornerDetected: bool, out portal: bool): bool;
    function AdjustRequestedMovementDirectionNavMesh(out directionWS: Vector, speed: float, maxAngle: float, maxIteration: int, maxIterationStartSide: int, preferedDirection: Vector, optional checkExploration: bool): bool;
    function StartRoadFollowing(speed: float, maxAngle: float, maxDistance: float, out correctedDirection: Vector): bool;
    function ResetRoadFollowing();
    function GetAgentPosition(): Vector;
    function SnapToNavigableSpace(snap: bool);
    function IsOnNavigableSpace(): bool;
    function IsEntityRepresentationForced(): int;
    function GetLastNavigablePosition(): Vector;
    function GetMovementAdjustor(): CMovementAdjustor;
    function PredictWorldPosition(inTime: float): Vector;
    function SetTriggerActivatorRadius(radius: float);
    function SetTriggerActivatorHeight(height: float);
    function AddTriggerActivatorChannel(channel: ETriggerChannels);
    function RemoveTriggerActivatorChannel(channel: ETriggerChannels);
    function SetEnabledFeetIK(enable: bool, optional blendTime: float);
    function GetEnabledFeetIK(): bool;
    function SetEnabledHandsIK(enable: bool);
    function SetHandsIKOffsets(left: float, right: float);
    function SetEnabledSlidingOnSlopeIK(enable: bool);
    function GetEnabledSlidingOnSlopeIK(): bool;
    function SetUseEntityForPelvisOffset(optional entity: CEntity);
    function GetUseEntityForPelvisOffset(): CEntity;
    function SetAdditionalOffsetWhenAttachingToEntity(optional entity: CEntity, optional time: float);
    function SetAdditionalOffsetToConsumePointWS(transformWS: Matrix, optional time: float);
    function SetAdditionalOffsetToConsumeMS(pos: Vector, rot: EulerAngles, time: float);
}
class CMovingPhysicalAgentComponent extends CMovingAgentComponent
{
    function IsPhysicalMovementEnabled(): bool;
    function GetPhysicalState(): ECharacterPhysicsState;
    function IsAnimatedMovement(): bool;
    function SetAnimatedMovement(enable: bool);
    function SetGravity(flag: bool);
    function SetBehaviorCallbackNeed(flag: bool);
    function SetSwimming(flag: bool);
    function GetWaterLevel(): float;
    function GetSubmergeDepth(): float;
    function SetDiving(diving: bool);
    function IsDiving(): bool;
    function SetEmergeSpeed(value: float);
    function GetEmergeSpeed(): float;
    function SetRagdollPushingMul(value: float);
    function GetRagdollPushingMul(): float;
    function ApplyVelocity(vel: Vector);
    function RegisterEventListener(listener: IScriptable);
    function UnregisterEventListener(listener: IScriptable);
    function SetPushable(pushable: bool);
    function IsOnGround(): bool;
    function IsCollidesWithCeiling(): bool;
    function IsCollidesOnSide(): bool;
    function IsFalling(): bool;
    function IsSliding(): bool;
    function GetSlideCoef(): float;
    function GetSlideDir(): Vector;
    function SetSliding(enable: bool);
    function SetSlidingSpeed(speed: float);
    function SetSlidingLimits(min: float, max: float);
    function EnableAdditionalVerticalSlidingIteration(enable: bool);
    function IsAdditionalVerticalSlidingIterationEnabled(): bool;
    function GetCapsuleHeight(): float;
    function GetCapsuleRadius(): float;
    function SetTerrainLimits(min: float, max: float);
    function SetTerrainInfluence(mul: float);
    function GetSlopePitch(): float;
    function GetTerrainNormal(damped: bool): Vector;
    function GetTerrainNormalWide(out normalAverage: Vector, out normalGlobal: Vector, directionToCheck: Vector, separationH: float, separationF: float, separationB: float);
    function GetCollisionData(index: int): SCollisionData;
    function GetCollisionDataCount(): int;
    function GetCollisionCharacterData(index: int): SCollisionData;
    function GetCollisionCharacterDataCount(): int;
    function GetGroundGridCollisionOn(side: ECollisionSides): bool;
    function GetMaterialName(): name;
    function EnableCollisionPrediction(enable: bool);
    function SetVirtualControllersPitch(pitch: float);
    function EnableVirtualControllerCollisionResponse(virtualControllerName: name, enable: bool);
}
class CAppearanceComponent extends CComponent
{
    var forcedAppearance: name;
    var attachmentReplacements: SAttachmentReplacements;
    var appearanceAttachments: array<SAppearanceAttachments>;

    function IncludeAppearanceTemplate(template: CEntityTemplate);
    function ExcludeAppearanceTemplate(template: CEntityTemplate);
    function ApplyAppearance(appearanceName: string);
}
class CBoatDestructionComponent extends CComponent
{
    var autoGeneratedVolumesX: Uint32;
    var autoGeneratedVolumesY: Uint32;
    var autoGeneratorVolumesResizer: float;
    var destructionVolumes: array<SBoatDestructionVolume>;
}
class CBoidPointOfInterestComponent extends CComponent
{
    //var `params.m_type`: ?;
    //var `params.m_scale`: ?;
    //var `params.m_gravityRangeMin`: ?;
    //var `params.m_gravityRangeMax`: ?;
    //var `params.m_effectorRadius`: ?;
    var acceptor: EZoneAcceptor;
    //var `params.m_shapeType`: ?;
    //var `params.m_useReachCallBack`: ?;
    //var `params.m_closestOnly`: ?;
    //var `params.m_coneMinOpeningAngle`: ?;
    //var `params.m_coneMaxOpeningAngle`: ?;
    //var `params.m_coneEffectorOpeningAngle`: ?;
    var crawlingSwarmDebug: bool;

    function Disable(disable: bool);
}
class CBoidPointOfInterestComponentScript extends CBoidPointOfInterestComponent
{
}
abstract class CBoundedComponent extends CComponent
{
    var boundingBox: Box;

    function GetBoundingBox(): Box;
}
class CAreaComponent extends CBoundedComponent
{
    var height: float;
    var color: Color;
    var terrainSide: EAreaTerrainSide;
    var clippingMode: EAreaClippingMode;
    var clippingAreaTags: TagList;
    var saveShapeToLayer: bool;
    var localPoints: array<Vector>;
    var worldPoints: array<Vector>;

    function TestEntityOverlap(ent: CEntity): bool;
    function TestPointOverlap(point: Vector): bool;
    function GetWorldPoints(out points: array<Vector>);
}
class CDeniedAreaComponent extends CAreaComponent
{
    var collisionType: EPathLibCollision;
    var isEnabled: bool;
    var canBeDisabled: bool;
}
class CTriggerAreaComponent extends CAreaComponent
{
    var isEnabled: bool;
    //var includedChannels: ?;
    //var excludedChannels: ?;
    var triggerPriority: Uint32;
    var enableCCD: bool;

    function SetChannelMask(includedChannels: int, excludedChannes: int);
    function AddIncludedChannel(channel: ETriggerChannels);
    function RemoveIncludedChannel(channel: ETriggerChannels);
    function AddExcludedChannel(channel: ETriggerChannels);
    function RemoveExcludedChannel(channel: ETriggerChannels);
}
abstract class CActionAreaComponent extends CTriggerAreaComponent
{
}
class CR4InteriorAreaComponent extends CTriggerAreaComponent
{
    var entranceTag: name;
    var texture: string;
}
class CSoftTriggerAreaComponent extends CTriggerAreaComponent
{
    var outerClippingAreaTags: TagList;
    //var outerIncludedChannels: ?;
    //var outerExcludedChannels: ?;
    var invertPenetrationFraction: bool;
}
class CSoundAmbientAreaComponent extends CSoftTriggerAreaComponent
{
    var soundEvents: StringAnsi;
    var reverb: SReverbDefinition;
    var customEventOnEnter: StringAnsi;
    var soundEventsOnEnter: array<StringAnsi>;
    var soundEventsOnExit: array<StringAnsi>;
    var enterExitEventsUsePosition: bool;
    var intensityParameter: float;
    var intensityParameterFadeTime: float;
    var maxDistance: float;
    var maxDistanceVertical: float;
    var banksDependency: array<name>;
    var occlusionEnabled: bool;
    var outerListnerReverbRatio: float;
    var priorityParameterMusic: bool;
    var parameterEnteringTime: float;
    var parameterEnteringCurve: ESoundParameterCurveType;
    var parameterExitingTime: float;
    var parameterExitingCurve: ESoundParameterCurveType;
    var useListernerDistance: bool;
    var isGate: bool;
    var gatewayRotation: float;
    var isWalla: bool;
    var wallaSoundEvents: array<StringAnsi>;
    var wallaEmitterSpread: float;
    var wallaOmniFactor: float;
    var wallaMinDistance: float;
    var wallaMaxDistance: float;
    var wallaBoxExtention: float;
    var wallaRotation: float;
    var wallaAfraidRetriggerTime: float;
    var wallaAfraidDecreaseRate: float;
    var parameters: array<SSoundGameParameterValue>;
    var parameterCulling: array<SSoundParameterCullSettings>;
    var fitWaterShore: bool;
    var waterGridCellCount: Uint32;
    var waterLevelOffset: float;
    var fitFoliage: bool;
    var foliageMaxDistance: float;
    var foliageStepNeighbors: Uint32;
    var foliageVitalAreaRadius: float;
    var foliageVitalAreaPoints: Uint32;
    var dynamicParameters: array<ESoundAmbientDynamicParameter>;
    var dynamicEvents: array<SSoundAmbientDynamicSoundEvents>;
}
abstract class CDrawableComponent extends CBoundedComponent
{
    //var drawableFlags: ?;
    //var lightChannels: ?;
    var renderingPlane: ERenderingPlane;

    function IsVisible(): bool;
    function SetVisible(flag: bool);
    function SetCastingShadows(flag: bool);
}
class CDecalComponent extends CDrawableComponent
{
    var diffuseTexture: CBitmapTexture;
    var specularity: float;
    var specularColor: Color;
    var normalThreshold: float;
    var autoHideDistance: float;
    var verticalFlip: bool;
    var horizontalFlip: bool;
    var fadeTime: float;
}
class CDestructionSystemComponent extends CDrawableComponent
{
    var m_resource: CApexResource;
    var targetEntityCollisionScriptName: name;
    var parentEntityCollisionScriptEventName: name;
    //var `parameters.m_materials`: ?;
    var m_physicalCollisionType: CPhysicalCollision;
    var m_fracturedPhysicalCollisionType: CPhysicalCollision;
    //var `dispacher selection`: ?;
    var dynamic: bool;
    var supportDepth: Uint32;
    var useAssetDefinedSupport: bool;
    var debrisDepth: int;
    var essentialDepth: Uint32;
    var debrisTimeout: bool;
    var debrisLifetimeMin: float;
    var debrisLifetimeMax: float;
    var debrisMaxSeparation: bool;
    var debrisMaxSeparationMin: float;
    var debrisMaxSeparationMax: float;
    var fadeOutTime: float;
    var minimumFractureDepth: Uint32;
    var preset: EDestructionPreset;
    var debrisDestructionProbability: float;
    var crumbleSmallestChunks: bool;
    var accumulateDamage: bool;
    var damageCap: float;
    var damageThreshold: float;
    var damageToRadius: float;
    var forceToDamage: float;
    var fractureImpulseScale: float;
    var impactDamageDefaultDepth: int;
    var impactVelocityThreshold: float;
    var materialStrength: float;
    var maxChunkSpeed: float;
    var useWorldSupport: bool;
    var useHardSleeping: bool;
    var useStressSolver: bool;
    var stressSolverTimeDelay: float;
    var stressSolverMassThreshold: float;
    var sleepVelocityFrameDecayConstant: float;
    //var eventOnDestruction: ?;
    var pathLibCollisionType: EPathLibCollision;
    var disableObstacleOnDestruction: bool;
    var shadowDistanceOverride: float;

    function GetFractureRatio(): float;
    function ApplyFracture(): bool;
    function IsDestroyed(): bool;
    function IsObstacleDisabled(): bool;
}
abstract class CMeshTypeComponent extends CDrawableComponent
{
    var forceLODLevel: int;
    var forceAutoHideDistance: Uint16;
    var shadowImportanceBias: EMeshShadowImportanceBias;
    var defaultEffectParams: Vector;
    var defaultEffectColor: Color;
}
class CClothComponent extends CMeshTypeComponent
{
    var resource: CApexResource;
    //var `dispacher selection`: ?;
    var recomputeNormals: bool;
    var correctSimulationNormals: bool;
    var slowStart: bool;
    var useStiffSolver: bool;
    var pressure: float;
    //var `lodWeights.maxDistance`: ?;
    //var `lodWeights.distanceWeight`: ?;
    //var `lodWeights.bias`: ?;
    //var `lodWeights.benefitsBias`: ?;
    var maxDistanceBlendTime: float;
    var uvChannelForTangentUpdate: Uint32;
    //var `maxDistanceScale.Multipliable`: ?;
    //var `maxDistanceScale.Scale`: ?;
    var collisionResponseCoefficient: float;
    var allowAdaptiveTargetFrequency: bool;
    var windScaler: float;
    var triggeringCollisionGroupNames: array<name>;
    var triggerType: ETriggerShape;
    var triggerDimensions: Vector;
    //var `triggerLocalOffset.V[ 3 ]`: ?;
    var shadowDistanceOverride: float;

    function SetSimulated(value: bool);
    function SetMaxDistanceScale(scale: float);
    function SetFrozen(frozen: bool);
}
class CDestructionComponent extends CMeshTypeComponent
{
    var m_baseResource: CPhysicsDestructionResource;
    var m_fracturedResource: CPhysicsDestructionResource;
    //var `parameters.m_pose`: ?;
    var m_physicalCollisionType: CPhysicalCollision;
    var m_fracturedPhysicalCollisionType: CPhysicalCollision;
    var dynamic: bool;
    var kinematic: bool;
    var debrisTimeout: bool;
    var debrisTimeoutMin: float;
    var debrisTimeoutMax: float;
    var initialBaseVelocity: Vector;
    var hasInitialFractureVelocity: bool;
    var maxVelocity: float;
    var maxAngularFractureVelocity: float;
    var debrisMaxSeparationDistance: float;
    var simulationDistance: float;
    var fadeOutTime: float;
    var forceToDamage: float;
    var damageThreshold: float;
    var damageEndurance: float;
    var accumulateDamage: bool;
    var useWorldSupport: bool;
    var fractureSoundEvent: StringAnsi;
    var fxName: name;
    //var eventOnDestruction: ?;
    var pathLibCollisionType: EPathLibCollision;
    var disableObstacleOnDestruction: bool;

    function ApplyFracture(): bool;
    function IsDestroyed(): bool;
    function IsObstacleDisabled(): bool;
}
class CMeshComponent extends CMeshTypeComponent
{
    var mesh: CMesh;
}
class CStaticMeshComponent extends CMeshComponent
{
    var pathLibCollisionType: EPathLibCollision;
    var fadeOnCameraCollision: bool;
    var physicalCollisionType: CPhysicalCollision;
}
class CRigidMeshComponent extends CStaticMeshComponent
{
    var motionType: EMotionType;
    var linearDamping: float;
    var angularDamping: float;
    var linearVelocityClamp: float;

    function EnableBuoyancy(enable: bool): bool;
}
class CBoatBodyComponent extends CRigidMeshComponent
{
    var cutsceneBoneIndex: Int16;

    function TriggerCutsceneStart();
    function TriggerCutsceneEnd();
}
class CScriptedDestroyableComponent extends CRigidMeshComponent
{
    var destroyWay: EDestroyWay;

    function GetDestroyWay(): EDestroyWay;
    function GetDestroyAtTime(): float;
    function GetDestroyTimeDuration(): float;
}
class CCombatDataComponent extends CComponent
{
    function GetAttackersCount(): int;
    function GetTicketSourceOwners(out actors: array<CActor>, ticketName: name);
    function HasAttackersInRange(range: float): bool;
    function TicketSourceOverrideRequest(ticketName: name, ticketsCountMod: int, minimalImportanceMod: float): int;
    function TicketSourceClearRequest(ticketName: name, requestId: int): bool;
    function ForceTicketImmediateImportanceUpdate(ticketName: name);
}
class CDismembermentComponent extends CComponent
{
    function IsWoundDefined(woundName: name): bool;
    function SetVisibleWound(woundName: name, optional spawnEntity: bool, optional createParticles: bool, optional dropEquipment: bool, optional playSound: bool, optional direction: Vector, optional playedEffectsMask: int);
    function ClearVisibleWound();
    function GetVisibleWoundName(): name;
    function CreateWoundParticles(woundName: name): bool;
    function GetNearestWoundName(positionMS: Vector, normalMS: Vector, optional woundTypeFlags: EWoundTypeFlags): name;
    function GetNearestWoundNameForBone(boneIndex: int, normalWS: Vector, optional woundTypeFlags: EWoundTypeFlags): name;
    function GetWoundsNames(out names: array<name>, optional woundTypeFlags: EWoundTypeFlags);
    function IsExplosionWound(woundName: name): bool;
    function IsFrostWound(woundName: name): bool;
    function GetMainCurveName(woundName: name): name;
}
class CDropPhysicsComponent extends CComponent
{
    var dropSetups: array<CDropPhysicsSetup>;

    function DropMeshByName(meshName: string, optional direction: Vector, optional curveName: name): bool;
    function DropMeshByTag(meshTag: name, optional direction: Vector, optional curveName: name): bool;
}
class CFocusActionComponent extends CComponent
{
    var actionName: name;
}
class CGameplayEffectsComponent extends CComponent
{
    function SetGameplayEffectFlag(flag: EEntityGameplayEffectFlags, value: bool);
    function GetGameplayEffectFlag(flag: EEntityGameplayEffectFlags): bool;
    function ResetGameplayEffectFlag(flag: EEntityGameplayEffectFlags): bool;
}
class CInteractionAreaComponent extends CComponent
{
    var rangeMin: float;
    var rangeMax: float;
    var rangeAngle: Uint32;
    var height: float;
    var isPlayerOnly: bool;
    var isEnabled: bool;
    var manualTestingOnly: bool;
    var checkLineOfSight: bool;
    var alwaysVisibleRange: float;
    var lineOfSightOffset: Vector;
    var performScriptedTest: bool;

    function GetRangeMin(): float;
    function GetRangeMax(): float;
    function SetRanges(rangeMin: float, rangeMax: float, height: float);
    function SetRangeAngle(rangeAngle: int);
    function SetCheckLineOfSight(flag: bool);
}
class CInteractionComponent extends CInteractionAreaComponent
{
    var actionName: string;
    var checkCameraVisibility: bool;
    var reportToScript: bool;
    var isEnabledInCombat: bool;
    var shouldIgnoreLocks: bool;

    function GetActionName(): string;
    function SetActionName(actionName: string);
    function GetInteractionFriendlyName(): string;
    function GetInteractionKey(): int;
    function GetInputActionName(): name;
}
class CDoorComponent extends CInteractionComponent
{
    var initialState: EDoorState;
    var isTrapdoor: bool;
    var doorsEnebled: bool;
    var openName: string;
    var closeName: string;

    function Open(force: bool, unlock: bool);
    function Close(force: bool);
    function IsOpen(): bool;
    function IsLocked(): bool;
    function AddForceImpulse(origin: Vector, force: float);
    function InstantClose();
    function InstantOpen(unlock: bool);
    function AddDoorUser(actor: CActor);
    function EnebleDoors(enable: bool);
    function IsInteractive(): bool;
    function IsTrapdoor(): bool;
    function InvertMatrixForDoor(m: Matrix): Matrix;
    function Unsuppress();
}
class CGameplayLightComponent extends CInteractionComponent
{
    var isLightOn: bool;
    var isCityLight: bool;
    var isInteractive: bool;
    var isAffectedByWeather: bool;

    function SetLight(toggle: bool);
    function SetFadeLight(toggle: bool);
    function SetInteractive(toggle: bool);
    function IsLightOn(): bool;
    function IsCityLight(): bool;
    function IsInteractive(): bool;
    function IsAffectedByWeather(): bool;
}
class CInventoryComponent extends CComponent
{
    var containerTemplate: CEntityTemplate;
    var rebalanceEveryNSeconds: Uint32;
    var turnOffSpawnItemsBudgeting: bool;

    function GetItemAbilityAttributeValue(itemId: SItemUniqueId, attributeName: name, abilityName: name): SAbilityAttributeValue;
    function GetItemFromSlot(slotName: name): SItemUniqueId;
    function IsIdValid(itemId: SItemUniqueId): bool;
    function GetItemCount(optional useAssociatedInventory: bool): int;
    function GetItemsNames(): array<name>;
    function GetAllItems(out items: array<SItemUniqueId>);
    function GetItemId(itemName: name): SItemUniqueId;
    function GetItemsIds(itemName: name): array<SItemUniqueId>;
    function GetItemsByTag(tag: name): array<SItemUniqueId>;
    function GetItemsByCategory(category: name): array<SItemUniqueId>;
    function GetSchematicIngredients(itemName: SItemUniqueId, out quantity: array<int>, out names: array<name>);
    function GetSchematicRequiredCraftsmanType(craftName: SItemUniqueId): name;
    function GetSchematicRequiredCraftsmanLevel(craftName: SItemUniqueId): name;
    function GetNumOfStackedItems(itemUniqueId: SItemUniqueId): int;
    function InitInvFromTemplate(resource: CEntityTemplate);
    function SplitItem(itemID: SItemUniqueId, quantity: int): SItemUniqueId;
    function SetItemStackable(itemID: SItemUniqueId, flag: bool);
    function GetCategoryDefaultItem(category: name): name;
    function GetItemLocalizedNameByName(itemName: name): string;
    function GetItemLocalizedDescriptionByName(itemName: name): string;
    function GetItemLocalizedNameByUniqueID(itemUniqueId: SItemUniqueId): string;
    function GetItemLocalizedDescriptionByUniqueID(itemUniqueId: SItemUniqueId): string;
    function GetItemIconPathByUniqueID(itemUniqueId: SItemUniqueId): string;
    function GetItemIconPathByName(itemName: name): string;
    function AddSlot(itemUniqueId: SItemUniqueId): bool;
    function GetSlotItemsLimit(itemUniqueId: SItemUniqueId): int;
    function BalanceItemsWithPlayerLevel(playerLevel: int);
    function GetItemQuantityByName(itemName: name, optional useAssociatedInventory: bool, optional ignoreTags: array<name>): int;
    function GetItemQuantityByCategory(itemCategory: name, optional useAssociatedInventory: bool, optional ignoreTags: array<name>): int;
    function GetItemQuantityByTag(itemTag: name, optional useAssociatedInventory: bool, optional ignoreTags: array<name>): int;
    function GetAllItemsQuantity(optional useAssociatedInventory: bool, optional ignoreTags: array<name>): int;
    function GetItem(itemId: SItemUniqueId): SInventoryItem;
    function GetItemName(itemId: SItemUniqueId): name;
    function GetItemCategory(itemId: SItemUniqueId): name;
    function GetItemClass(itemId: SItemUniqueId): EInventoryItemClass;
    function GetItemTags(itemId: SItemUniqueId, out tags: array<name>): bool;
    function GetCraftedItemName(itemId: SItemUniqueId): name;
    function TotalItemStats(invItem: SInventoryItem): float;
    function GetItemPrice(itemId: SItemUniqueId): int;
    function GetItemPriceModified(itemId: SItemUniqueId, optional playerSellingItem: bool): int;
    function GetInventoryItemPriceModified(invItem: SInventoryItem, optional playerSellingItem: bool): int;
    function GetItemPriceRepair(invItem: SInventoryItem, out costRepairPoint: int, out costRepairTotal: int);
    function GetItemPriceRemoveUpgrade(invItem: SInventoryItem): int;
    function GetItemPriceDisassemble(invItem: SInventoryItem): int;
    function GetItemPriceAddSlot(invItem: SInventoryItem): int;
    function GetItemPriceCrafting(invItem: SInventoryItem): int;
    function GetItemPriceEnchantItem(invItem: SInventoryItem): int;
    function GetItemPriceRemoveEnchantment(invItem: SInventoryItem): int;
    function GetFundsModifier(): float;
    function GetItemQuantity(itemId: SItemUniqueId): int;
    function ItemHasTag(itemId: SItemUniqueId, tag: name): bool;
    function AddItemTag(itemId: SItemUniqueId, tag: name): bool;
    function RemoveItemTag(itemId: SItemUniqueId, tag: name): bool;
    function GetItemByItemEntity(itemEntity: CItemEntity): SItemUniqueId;
    function GetItemAttributeValue(itemId: SItemUniqueId, attributeName: name, optional abilityTags: array<name>, optional withoutTags: bool): SAbilityAttributeValue;
    function GetItemBaseAttributes(itemId: SItemUniqueId, out attributes: array<name>);
    function GetItemAttributes(itemId: SItemUniqueId, out attributes: array<name>);
    function GetItemAbilities(itemId: SItemUniqueId, out abilities: array<name>);
    function GetItemContainedAbilities(itemId: SItemUniqueId, out abilities: array<name>);
    function GiveItem(otherInventory: CInventoryComponent, itemId: SItemUniqueId, optional quantity: int): array<SItemUniqueId>;
    function HasItem(item: name): bool;
    function AddMultiItem(item: name, optional quantity: int, optional informGui: bool, optional markAsNew: bool, optional lootable: bool): array<SItemUniqueId>;
    function AddSingleItem(item: name, optional informGui: bool, optional markAsNew: bool, optional lootable: bool): SItemUniqueId;
    function RemoveItem(itemId: SItemUniqueId, optional quantity: int): bool;
    function RemoveAllItems();
    function GetItemEntityUnsafe(itemId: SItemUniqueId): CItemEntity;
    function GetDeploymentItemEntity(itemId: SItemUniqueId, optional position: Vector, optional rotation: EulerAngles, optional allocateIdTag: bool): CEntity;
    function MountItem(itemId: SItemUniqueId, optional toHand: bool, optional force: bool): bool;
    function UnmountItem(itemId: SItemUniqueId, optional destroyEntity: bool): bool;
    function IsItemMounted(itemId: SItemUniqueId): bool;
    function IsItemHeld(itemId: SItemUniqueId): bool;
    function DropItem(itemId: SItemUniqueId, optional removeFromInv: bool);
    function GetItemHoldSlot(itemId: SItemUniqueId): name;
    function PlayItemEffect(itemId: SItemUniqueId, effectName: name);
    function StopItemEffect(itemId: SItemUniqueId, effectName: name);
    function ThrowAwayItem(itemId: SItemUniqueId, optional quantity: int): bool;
    function ThrowAwayAllItems(): CEntity;
    function ThrowAwayItemsFiltered(excludedTags: array<name>): CEntity;
    function ThrowAwayLootableItems(optional skipNoDropNoShow: bool): CEntity;
    function GetItemRecyclingParts(itemId: SItemUniqueId): array<SItemParts>;
    function GetItemWeight(id: SItemUniqueId): float;
    function HasItemDurability(itemId: SItemUniqueId): bool;
    function GetItemDurability(itemId: SItemUniqueId): float;
    function SetItemDurability(itemId: SItemUniqueId, durability: float);
    function GetItemInitialDurability(itemId: SItemUniqueId): float;
    function GetItemMaxDurability(itemId: SItemUniqueId): float;
    function GetItemGridSize(itemId: SItemUniqueId): int;
    function NotifyItemLooted(item: SItemUniqueId);
    function ResetContainerData();
    function GetItemModifierFloat(itemId: SItemUniqueId, modName: name, optional defValue: float): float;
    function SetItemModifierFloat(itemId: SItemUniqueId, modName: name, val: float);
    function GetItemModifierInt(itemId: SItemUniqueId, modName: name, optional defValue: int): int;
    function SetItemModifierInt(itemId: SItemUniqueId, modName: name, val: int);
    function ActivateQuestBonus();
    function GetItemSetName(itemId: SItemUniqueId): name;
    function AddItemCraftedAbility(itemId: SItemUniqueId, abilityName: name, optional allowDuplicate: bool);
    function RemoveItemCraftedAbility(itemId: SItemUniqueId, abilityName: name);
    function AddItemBaseAbility(item: SItemUniqueId, abilityName: name);
    function RemoveItemBaseAbility(item: SItemUniqueId, abilityName: name);
    function DespawnItem(itemId: SItemUniqueId);
    function GetInventoryItemUIData(item: SItemUniqueId): SInventoryItemUIData;
    function SetInventoryItemUIData(item: SItemUniqueId, data: SInventoryItemUIData);
    function SortInventoryUIData();
    function PrintInfo();
    function EnableLoot(enable: bool);
    function UpdateLoot();
    function AddItemsFromLootDefinition(lootDefinitionName: name);
    function IsLootRenewable(): bool;
    function IsReadyToRenew(): bool;
    function NotifyScriptedListeners(notify: bool);
    function GetItemEnhancementSlotsCount(itemId: SItemUniqueId): int;
    function GetItemEnhancementItems(itemId: SItemUniqueId, out names: array<name>);
    function GetItemEnhancementCount(itemId: SItemUniqueId): int;
    function GetItemColor(itemId: SItemUniqueId): name;
    function IsItemColored(itemId: SItemUniqueId): bool;
    function SetPreviewColor(itemId: SItemUniqueId, colorId: int);
    function ClearPreviewColor(itemId: SItemUniqueId): bool;
    function ColorItem(itemId: SItemUniqueId, dyeId: SItemUniqueId);
    function ClearItemColor(itemId: SItemUniqueId): bool;
    function EnchantItem(enhancedItemId: SItemUniqueId, enchantmentName: name, enchantmentStat: name): bool;
    function GetEnchantment(enhancedItemId: SItemUniqueId): name;
    function IsItemEnchanted(enhancedItemId: SItemUniqueId): bool;
    function UnenchantItem(enhancedItemId: SItemUniqueId): bool;
    function EnhanceItem(enhancedItemId: SItemUniqueId, extensionItemId: SItemUniqueId): bool;
    function RemoveItemEnhancementByIndex(enhancedItemId: SItemUniqueId, slotIndex: int): bool;
    function RemoveItemEnhancementByName(enhancedItemId: SItemUniqueId, extensionItemName: name): bool;
    function PreviewItemAttributeAfterUpgrade(baseItemId: SItemUniqueId, upgradeItemId: SItemUniqueId, attributeName: name, optional baseInventory: CInventoryComponent, optional upgradeInventory: CInventoryComponent): SAbilityAttributeValue;
    function HasEnhancementItemTag(enhancedItemId: SItemUniqueId, slotIndex: int, tag: name): bool;
}
class CMorphedMeshManagerComponent extends CComponent
{
    //var `Default morph ratio`: ?;
    var morphCurve: CCurve;
    var morphRatio: float;

    function SetMorphBlend(morphRatio: float, blendtime: float);
    function GetMorphBlend(): float;
}
class CNormalBlendComponent extends CComponent
{
    //var dataSource: ?;
    var useMainTick: bool;
    var sourceMaterial: IMaterial;
    var sourceNormalTexture: ITexture;
    var normalBlendMaterial: CMaterialInstance;
    var normalBlendAreas: array<Vector>;
}
class CPathComponent extends CComponent
{
    var curve: SMultiCurve;
    var speedCurve: SSimpleCurve;

    function FindClosestEdge(point: Vector): int;
    function GetAlphaOnEdge(point: Vector, edgeIdx: int, optional epsilon: float): float;
    function GetClosestPointOnPath(point: Vector, optional epsilon: float): Vector;
    function GetClosestPointOnPathExt(point: Vector, out edgeIdx: int, out edgeAlpha: float, optional epsilon: float): Vector;
    function GetDistanceToPath(point: Vector, optional epsilon: float): float;
    function GetNextPointOnPath(point: Vector, distance: float, out isEndOfPath: bool, optional epsilon: float): Vector;
    function GetNextPointOnPathExt(out edgeIdx: int, out edgeAlpha: float, distance: float, out isEndOfPath: bool, optional epsilon: float): Vector;
    function GetPointsCount(): int;
    function GetWorldPoint(index: int): Vector;
}
class CPhantomComponent extends CComponent
{
    var collisionGroupName: name;
    var triggeringCollisionGroupNames: array<name>;
    var shapeType: EPhantomShape;
    var shapeDimensions: Vector;
    var onTriggerEnteredScriptEvent: name;
    var onTriggerExitedScriptEvent: name;
    var eventsCalledOnComponent: bool;
    var useInQueries: bool;
    var meshCollision: CMesh;

    function Activate();
    function Deactivate();
    function GetTriggeringCollisionGroupNames(out names: array<name>);
    function GetNumObjectsInside(): int;
}
class CR4EffectComponent extends CComponent
{
    var effectName: name;
    var effectTarget: EntityHandle;
    var targetBone: name;

    function PlayEffect(effectName: name, target: CEntity, targetBone: name);
    function StopEffect();
}
class CR4HumanoidCombatComponent extends CComponent
{
    function UpdateSoundInfo();
    function GetSoundTypeIdentificationForBone(boneIndex: int): name;
    function GetBoneClosestToEdge(a: Vector, b: Vector, optional preciseSearch: bool): int;
    function GetDefaultSoundInfoMapping(): SSoundInfoMapping;
}
class CReactionSceneActorComponent extends CComponent
{
    var cooldownInterval: float;
    var sceneStartedSuccesfully: bool;
}
class CScriptedComponent extends CComponent
{
}
class CSelfUpdatingComponent extends CScriptedComponent
{
    var tickGroup: ETickGroup;
    var tickedByDefault: bool;

    function StartTicking();
    function StopTicking();
    function GetIsTicking(): bool;
}
class CHeadManagerComponent extends CSelfUpdatingComponent
{
    var timePeriod: GameTime;
    var initHeadIndex: int;
    var lastChangeGameTime: GameTime;
    var hasTattoo: bool;
    var hasDemonMark: bool;
    var curIndex: Uint32;
    var heads: array<name>;
    var headsWithTattoo: array<name>;
    var headsDemon: array<name>;
    var headsDemonWithTattoo: array<name>;
    var curHeadId: SItemUniqueId;
    var blockGrowing: bool;

    function SetTattoo(hasTattoo: bool);
    function SetDemonMark(hasDemonMark: bool);
    function SetBeardStage(maxStage: bool, optional stage: int);
    function SetCustomHead(head: name);
    function RemoveCustomHead();
    function BlockGrowing(block: bool);
    function Shave();
    function MimicTest(animName: name);
    function GetCurHeadName(): name;
}
class CSpriteComponent extends CComponent
{
    var isVisible: bool;
    var icon: CBitmapTexture;
}
class CEffectDummyComponent extends CSpriteComponent
{
}
class CWayPointComponent extends CSpriteComponent
{
}
class CSwitchableFoliageComponent extends CComponent
{
    var resource: CSwitchableFoliageResource;
    var minimumStreamingDistance: Uint32;

    function SetEntry(entryName: name);
}
class CTriggerActivatorComponent extends CComponent
{
    var radius: float;
    var height: float;
    //var channels: ?;
    var enableCCD: bool;
    var maxContinousDistance: float;

    function SetRadius(radius: float);
    function SetHeight(height: float);
    function GetRadius(): float;
    function GetHeight(): float;
    function AddTriggerChannel(channel: ETriggerChannels);
    function RemoveTriggerChannel(channel: ETriggerChannels);
}
class CVehicleComponent extends CComponent
{
    var user: CActor;

    function PlaySlotAnimation(slot: name, animation: name, optional blendIn: float, optional blendOut: float): bool;
    function PlaySlotAnimationAsync(slot: name, animation: name, optional blendIn: float, optional blendOut: float): bool;
    function GetSlotTransform(slotName: name, out translation: Vector, out rotQuat: Vector);
    function GetDeepDistance(vel: Vector): float;
    function SetCommandToMountDelayed(ctmd: bool);
    function IsCommandToMountDelayed(): bool;
    function OnDriverMount();
}
class CBoatComponent extends CVehicleComponent
{
    var sailDir: float;
    var mountAnimationFinished: bool;
    var collisionNames: array<name>;

    function GetLinearVelocityXY(): float;
    function TriggerDrowning(globalHitPosition: Vector);
    function IsDrowning(): bool;
    function GetBoatBodyMass(): float;
    function GetCurrentGear(): int;
    function GetCurrentSpeed(): Vector;
    function GetMaxSpeed(): float;
    function GetBuoyancyPointStatus_Front(): Vector;
    function GetBuoyancyPointStatus_Back(): Vector;
    function GetBuoyancyPointStatus_Right(): Vector;
    function GetBuoyancyPointStatus_Left(): Vector;
    function MountStarted();
    function DismountFinished();
    function UseOutOfFrustumTeleportation(enable: bool);
    function GameCameraTick(out fovDistPitch: Vector, out offsetZ: float, out sailOffset: float, dt: float, passenger: bool): bool;
    function StopAndDismountBoat();
    function SetInputValues(accelerate: SInputAction, decelerate: SInputAction, stickTilt: Vector, localSpaceCameraTurnPercent: float);
    function TriggerCutsceneStart();
    function TriggerCutsceneEnd();
}
class W3HorseComponent extends CVehicleComponent
{
    var riderSharedParams: CHorseRiderSharedParams;

    function PairWithRider(inRiderSharedParams: CHorseRiderSharedParams): bool;
    function IsTamed(): bool;
    function Unpair();
    function IsDismounted(): bool;
    function IsFullyMounted(): bool;
}
class CEntity extends CNode
{
    //var components: ?;
    var template: CEntityTemplate;
    var streamingDataBuffer: SharedDataBuffer;
    var streamingDistance: byte;
    //var entityStaticFlags: ?;
    var autoPlayEffectName: name;
    var entityFlags: byte;

    function AddTimer(timerName: name, period: float, optional repeats: bool, optional scatter: bool, optional group: ETickGroup, optional saveable: bool, optional overrideExisting: bool): int;
    function AddGameTimeTimer(timerName: name, period: GameTime, optional repeats: bool, optional scatter: bool, optional group: ETickGroup, optional saveable: bool, optional overrideExisting: bool): int;
    function RemoveTimer(timerName: name, optional group: ETickGroup);
    function RemoveTimerById(id: int, optional group: ETickGroup);
    function RemoveTimers();
    function HasTagInLayer(tag: name): bool;
    function Destroy();
    function Duplicate(optional placeOnLayer: CLayer): CEntity;
    function Teleport(position: Vector);
    function TeleportWithRotation(position: Vector, rotation: EulerAngles);
    function TeleportToNode(node: CNode, optional applyRotation: bool): bool;
    function GetRootAnimatedComponent(): CAnimatedComponent;
    function RaiseEvent(eventName: name): bool;
    function RaiseForceEvent(eventName: name): bool;
    function RaiseEventWithoutTestCheck(eventName: name): bool;
    function RaiseForceEventWithoutTestCheck(eventName: name): bool;
    function WaitForEventProcessing(eventName: name, timeout: float): bool;
    function WaitForBehaviorNodeActivation(activationName: name, timeout: float): bool;
    function WaitForBehaviorNodeDeactivation(deactivationName: name, timeout: float): bool;
    function WaitForAnimationEvent(animEventName: name, timeout: float): bool;
    function BehaviorNodeDeactivationNotificationReceived(deactivationName: name): bool;
    function I_GetDisplayName(): string;
    function CalcBoundingBox(out box: Box);
    function CalcEntitySlotMatrix(slot: name, out slotMatrix: Matrix): bool;
    function GetBoneWorldMatrixByIndex(boneIndex: int): Matrix;
    function GetBoneReferenceMatrixMS(boneIndex: int): Matrix;
    function GetBoneIndex(bone: name): int;
    function GetMoveTarget(): Vector;
    function GetMoveHeading(): float;
    function PreloadBehaviorsToActivate(names: array<name>): bool;
    function ActivateBehaviors(names: array<name>): bool;
    function ActivateBehaviorsSync(names: array<name>): bool;
    function ActivateAndSyncBehaviors(names: array<name>, optional timeout: float): bool;
    function ActivateAndSyncBehavior(names: name, optional timeout: float): bool;
    function AttachBehavior(instanceName: name): bool;
    function AttachBehaviorSync(instanceName: name): bool;
    function DetachBehavior(instanceName: name): bool;
    function GetBehaviorVariable(varName: name, optional defaultValue: float): float;
    function GetBehaviorVectorVariable(varName: name): Vector;
    function SetBehaviorVariable(varName: name, varValue: float, optional inAllInstances: bool): bool;
    function SetBehaviorVectorVariable(varName: name, varValue: Vector, optional inAllInstances: bool): bool;
    function GetBehaviorGraphInstanceName(optional index: int): name;
    function Fade(fadeIn: bool);
    function SetHideInGame(hide: bool);
    function GetComponent(compName: string): CComponent;
    function GetComponentByClassName(className: name): CComponent;
    function GetComponentsByClassName(className: name): array<CComponent>;
    function GetComponentByUsedBoneName(boneIndex: int): array<CComponent>;
    function GetComponentsCountByClassName(className: name): int;
    function GetAutoEffect(): name;
    function SetAutoEffect(effectName: name): bool;
    function PlayEffect(effectName: name, optional target: CNode): bool;
    function PlayEffectOnBone(effectName: name, boneName: name, optional target: CNode): bool;
    function StopEffect(effectName: name): bool;
    function DestroyEffect(effectName: name): bool;
    function StopAllEffects();
    function DestroyAllEffects();
    function IsEffectActive(effectName: name, optional treatStoppingAsActive: bool): bool;
    function SetEffectIntensity(effectName: name, intensity: float, optional specificComponentName: name, optional effectParameterName: name);
    function HasEffect(effectName: name): bool;
    function SoundEvent(eventName: string, optional boneName: name, optional isSlot: bool);
    function TimedSoundEvent(duration: float, optional startEvent: string, optional stopEvent: string, optional shouldUpdateTimeParameter: bool, optional boneName: name);
    function SoundSwitch(swichGroupName: string, optional stateName: string, optional boneName: name, optional isSlot: bool);
    function SoundParameter(parameterName: string, value: float, optional boneName: name, optional duration: float, optional isSlot: bool);
    function SoundIsActiveAny(): bool;
    function SoundIsActiveName(eventName: name): bool;
    function SoundIsActive(boneName: name, optional isSlot: bool): bool;
    function PreloadEffect(effectName: name): bool;
    function PreloadEffectForAnimation(animName: name): bool;
    function SetKinematic(enable: bool);
    function SetStatic();
    function IsRagdolled(): bool;
    function IsStatic(): bool;
    function GetGuidHash(): int;
    function CreateAttachment(parentEntity: CEntity, optional entityTemplateSlot: name, optional relativePosition: Vector, optional relativeRotation: EulerAngles): bool;
    function BreakAttachment(): bool;
    function HasAttachment(): bool;
    function HasSlot(slotName: name, optional recursive: bool): bool;
    function CreateAttachmentAtBoneWS(parentEntity: CEntity, bone: name, worldLocation: Vector, worldRotation: EulerAngles): bool;
    function CreateChildAttachment(child: CNode, optional slot: name): bool;
    function BreakChildAttachment(child: CNode, optional slot: name): bool;
    function HasChildAttachment(child: CNode): bool;
}
class CCamera extends CEntity
{
    function SetActive(blendTime: float);
    function IsActive(): bool;
    function IsOnStack(): bool;
    function GetCameraDirection(): Vector;
    function GetCameraPosition(): Vector;
    function GetCameraMatrixWorldSpace(): Matrix;
    function SetFov(fov: float);
    function GetFov(): float;
    function SetZoom(value: float);
    function GetZoom(): float;
    function Reset();
    function ResetRotation(optional smoothly: bool, optional horizontal: bool, optional vertical: bool, optional duration: float);
    function ResetRotationTo(smoothly: bool, horizontalAngle: float, optional verticalAngle: float, optional duration: float);
    function Rotate(leftRightDelta: float, upDownDelta: float);
    function Follow(dest: CEntity);
    function FollowWithRotation(dest: CEntity);
    function LookAt(target: CNode, optional duration: float, optional activatingTime: float);
    function LookAtStatic(staticTarget: Vector, optional duration: float, optional activatingTime: float);
    function LookAtBone(target: CAnimatedComponent, boneName: string, optional duration: float, optional activatingTime: float);
    function LookAtDeactivation(optional deactivatingTime: float);
    function HasLookAt(): bool;
    function GetLookAtTargetPosition(): Vector;
    function FocusOn(target: CNode, optional duration: float, optional activatingTime: float);
    function FocusOnStatic(staticTarget: Vector, optional duration: float, optional activatingTime: float);
    function FocusOnBone(target: CAnimatedComponent, boneName: string, optional duration: float, optional activatingTime: float);
    function FocusDeactivation(optional deactivatingTime: float);
    function IsFocused(): bool;
    function GetFocusTargetPosition(): Vector;
}
class CStaticCamera extends CCamera
{
    var solver: ECameraSolver;
    var activationDuration: float;
    var deactivationDuration: float;
    var timeout: float;
    var zoom: float;
    var fov: float;
    var animState: int;
    var guiEffect: int;
    var blockPlayer: bool;
    var resetPlayerCamera: bool;
    var fadeStartDuration: float;
    var fadeStartColor: Color;
    var isFadeStartFadeIn: bool;
    var fadeEndDuration: float;
    var fadeEndColor: Color;
    var isFadeEndFadeIn: bool;

    function Run(): bool;
    function IsRunning(): bool;
    function AutoDeactivating(): bool;
    function Stop();
    function RunAndWait(optional timeout: float): bool;
}
class CCustomCamera extends CEntity
{
    //var pivotPositionControllers: ?;
    //var pivotRotationControllers: ?;
    //var pivotDistanceControllers: ?;
    //var activeCameraPositionController: ?;
    //var blendPivotPositionController: ?;
    var allowAutoRotation: bool;
    var manualRotationHorTimeout: float;
    var manualRotationVerTimeout: float;
    var fov: float;
    var animSet: CSkeletalAnimationSet;
    var presets: array<SCustomCameraPreset>;
    //var curveSet: ?;
    var curveNames: array<name>;

    function Activate(optional blendTime: float, optional resetPosition: bool);
    function GetActivePivotPositionController(): ICustomCameraPivotPositionController;
    function GetActivePivotRotationController(): ICustomCameraPivotRotationController;
    function GetActivePivotDistanceController(): ICustomCameraPivotDistanceController;
    function ChangePivotPositionController(_name: name): bool;
    function ChangePivotRotationController(_name: name): bool;
    function ChangePivotDistanceController(_name: name): bool;
    function BlendToPivotPositionController(_name: name, blendTime: float): bool;
    function PlayAnimation(animation: SCameraAnimationDefinition);
    function StopAnimation(animation: name);
    function FindCurve(curveName: name): CCurve;
    function SetManualRotationHorTimeout(timeOut: float);
    function SetManualRotationVerTimeout(timeOut: float);
    function GetManualRotationHorTimeout(): float;
    function GetManualRotationVerTimeout(): float;
    function IsManualControledHor(): bool;
    function IsManualControledVer(): bool;
    function ForceManualControlHorTimeout();
    function ForceManualControlVerTimeout();
    function EnableManualControl(enable: bool);
    function ChangePreset(preset: name);
    function NextPreset();
    function PrevPreset();
    function GetTilt(): float;
    function SetTilt(tilt: float);
    function SetCollisionOffset(offset: Vector);
    function EnableScreenSpaceCorrection(enable: bool);
    function GetActivePreset(): SCustomCameraPreset;
    function SetAllowAutoRotation(allow: bool);
}
class CItemEntity extends CEntity
{
    var timeToDespawn: float;
    var reportToScript: bool;

    function GetParentEntity(): CEntity;
    function GetItemTags(out tags: array<name>);
    function GetMeshComponent(): CComponent;
    function GetItemCategory(): name;
}
class CWitcherSword extends CItemEntity
{
    var swordType: EWitcherSwordType;

    function GetSwordType(): EWitcherSwordType;
}
class RangedWeapon extends CItemEntity
{
}
class CPeristentEntity extends CEntity
{
    var idTag: IdTag;
    var isSaveable: bool;
}
class CGameplayEntity extends CPeristentEntity
{
    //var propertyAnimationSet: ?;
    var displayName: LocalizedString;
    //var stats: ?;
    var isInteractionActivator: bool;
    var aimVector: Vector;
    var gameplayFlags: Uint32;
    var focusModeVisibility: EFocusModeVisibility;

    function GetInventory(): CInventoryComponent;
    function GetCharacterStats(): CCharacterStats;
    function GetDisplayName(optional fallBack: bool): string;
    function PlayPropertyAnimation(animationName: name, optional count: int, optional lengthScale: float, optional mode: EPropertyCurveMode);
    function StopPropertyAnimation(animationName: name, optional restoreInitialValues: bool);
    function RewindPropertyAnimation(animationName: name, time: float);
    function GetPropertyAnimationInstanceTime(propertyName: name, animationName: name): float;
    function GetPropertyAnimationLength(propertyName: name, animationName: name): float;
    function GetPropertyAnimationTransformAt(propertyName: name, animationName: name, time: float): Matrix;
    function GetGameplayEntityParam(className: name): CGameplayEntityParam;
    function AddAnimEventCallback(eventName: name, functionName: name);
    function RemoveAnimEventCallback(eventName: name);
    function AddAnimEventChildCallback(child: CNode, eventName: name, functionName: name);
    function RemoveAnimEventChildCallback(child: CNode, eventName: name);
    function GetSfxTag(): name;
    function GetStorageBounds(out box: Box);
    function GetGameplayInfoCache(type: EGameplayInfoCacheType): bool;
    function GetFocusModeVisibility(): EFocusModeVisibility;
    function SetFocusModeVisibility(focusModeVisibility: EFocusModeVisibility, optional persistent: bool, optional force: bool);
    function EnableVisualDebug(flag: EShowFlags, enable: bool);
}
class CActionPoint extends CGameplayEntity
{
    var events: array<SEntityActionsRouterEntry>;
    var actionBreakable: bool;
    var overrideActionBreakableInComponent: bool;
}
class CActor extends CGameplayEntity
{
    var actorGroups: EPathEngineAgentType;
    var aimOffset: float;
    var barOffset: float;
    var isAttackableByPlayer: bool;
    var losTestBoneIndex: int;
    var attackTarget: CActor;
    var attackTargetSetTime: EngineTime;
    var frontPushAnim: name;
    var backPushAnim: name;
    var isCollidable: bool;
    var isVisibileFromFar: bool;
    var voiceTag: name;
    var voiceToRandomize: array<StringAnsi>;
    //var behTreeMachine: ?;
    var useHiResShadows: bool;
    var keepUseHiResShadows: bool;
    var isInFFMiniGame: bool;
    var pelvisBoneName: name;
    var torsoBoneName: name;
    var headBoneName: name;
    var useAnimationEventFilter: bool;
    var soundListenerOverride: string;
    var encounterGroupUsedToSpawn: int;

    function MuteHeadAudio(mute: bool);
    function CanPush(canPush: bool);
    function ApplyItemAbilities(itemId: SItemUniqueId): bool;
    function RemoveItemAbilities(itemId: SItemUniqueId): bool;
    function GetCharacterStatsParam(abilities: array<name>);
    function ReportDeathToSpawnSystems();
    function ForceSoundAppearanceUpdate();
    function GetAliveFlag(): bool;
    function EnableStaticLookAt(point: Vector, duration: float);
    function EnableDynamicLookAt(node: CNode, duration: float);
    function DisableLookAt();
    function SetLookAtMode(mode: ELookAtMode);
    function ResetLookAtMode(mode: ELookAtMode);
    function GetAutoEffects(out effects: array<name>);
    function SignalGameplayEvent(eventName: name);
    function SignalGameplayEventParamCName(eventName: name, param: name);
    function SignalGameplayEventParamInt(eventName: name, param: int);
    function SignalGameplayEventParamFloat(eventName: name, param: float);
    function SignalGameplayEventParamObject(eventName: name, param: IScriptable);
    function SignalGameplayEventReturnCName(eventName: name, defaultVal: name): name;
    function SignalGameplayEventReturnInt(eventName: name, defaultVal: int): int;
    function SignalGameplayEventReturnFloat(eventName: name, defaultVal: float): float;
    function SignalGameplayDamageEvent(eventName: name, data: CDamageData);
    function GetScriptStorageObject(storageItemName: name): IScriptable;
    function ForceAIUpdate();
    function GetTarget(): CActor;
    function IsDangerous(actor: CActor): bool;
    function GetAttitude(actor: CActor): EAIAttitude;
    function SetAttitude(actor: CActor, attitude: EAIAttitude);
    function ResetAttitude(actor: CActor);
    function HasAttitudeTowards(actor: CActor): bool;
    function ClearAttitudes(hostile: bool, neutral: bool, friendly: bool);
    function GetAttitudeGroup(): name;
    function GetBaseAttitudeGroup(): name;
    function SetBaseAttitudeGroup(groupName: name);
    function ResetBaseAttitudeGroup();
    function SetTemporaryAttitudeGroup(groupName: name, priority: EAttitudeGroupPriority);
    function ResetTemporaryAttitudeGroup(priority: EAttitudeGroupPriority);
    function IsInCombat(): bool;
    function SetDebugAttackRange(rangeName: name);
    function EnableDebugARTraceDraw(enable: bool);
    function IsReadyForNewAction(): bool;
    function ActionCancelAll();
    function IsCurrentActionInProgress(): bool;
    function IsCurrentActionSucceded(): bool;
    function IsCurrentActionFailed(): bool;
    function IsInNonGameplayCutscene(): bool;
    function IsInGameplayScene(): bool;
    function PlayScene(input: string): bool;
    function StopAllScenes();
    function GetCurrentActionPriority(): int;
    function GetCurrentActionType(): EActorActionType;
    function IsDoingSomethingMoreImportant(priority: int): bool;
    function CanPlayQuestScene(): bool;
    function HasInteractionScene(): bool;
    function CanTalk(optional ignoreCurrentSpeech: bool): bool;
    function GetActorAnimState(): int;
    function IsInView(): bool;
    function IsUsingExploration(): bool;
    function GetAnimCombatSlots(animSlotName: name, out outSlots: array<Matrix>, slotsNum: int, mainEnemyMatrix: Matrix): bool;
    function GetHeadBoneIndex(): int;
    function GetTorsoBoneIndex(): int;
    function ActionMoveToNodeAsync(target: CNode, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveToNodeWithHeadingAsync(target: CNode, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveToAsync(target: Vector, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveToWithHeadingAsync(target: Vector, heading: float, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveToDynamicNodeAsync(target: CNode, moveType: EMoveType, absSpeed: float, range: float, optional keepDistance: bool, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveCustomAsync(targeter: CMoveTRGScript): bool;
    function ActionSlideThroughAsync(explorationAreaToUse: CActionAreaComponent): bool;
    function ActionSlideToAsync(target: Vector, duration: float): bool;
    function ActionMoveOnCurveToAsync(target: Vector, duration: float, rightShift: bool): bool;
    function ActionSlideToWithHeadingAsync(target: Vector, heading: float, duration: float, optional rotation: ESlideRotation): bool;
    function ActionMoveAwayFromNodeAsync(position: CNode, distance: float, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveAwayFromLineAsync(positionA: Vector, positionB: Vector, distance: float, makeMinimalMovement: bool, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionRotateToAsync(target: Vector): bool;
    function ActionPlaySlotAnimationAsync(slotName: name, animationName: name, optional blendIn: float, optional blendOut: float, optional continuePlaying: bool): bool;
    function ActionExitWorkAsync(optional fast: bool): bool;
    function ActionAnimatedSlideToStaticAsync(settings: SAnimatedSlideSettings, target: Vector, heading: float, translation: bool, rotation: bool): bool;
    function ActionAnimatedSlideToAsync(settings: SAnimatedSlideSettings, target: CNode, translation: bool, rotation: bool): bool;
    function ActionAnimatedSlideToStaticAsync_P(settings: SAnimatedSlideSettings, target: Vector, heading: float, translation: bool, rotation: bool, out animProxy: CActionMoveAnimationProxy): bool;
    function ActionAnimatedSlideToAsync_P(settings: SAnimatedSlideSettings, target: CNode, translation: bool, rotation: bool, out animProxy: CActionMoveAnimationProxy): bool;
    function ActionMatchToAsync(settings: SActionMatchToSettings, target: SActionMatchToTarget): bool;
    function ActionMatchToAsync_P(settings: SActionMatchToSettings, target: SActionMatchToTarget, out animProxy: CActionMoveAnimationProxy): bool;
    function GetSkeletonType(): ESkeletonType;
    function GetFallTauntEvent(): string;
    function SetErrorState(description: string);
    function GetRadius(): float;
    function GetVisualDebug(): CVisualDebug;
    function PlayVoiceset(priority: int, voiceset: string, optional breakCurrentSpeach: bool): bool;
    function StopAllVoicesets(optional cleanupQueue: bool);
    function HasVoiceset(voiceset: string): EAsyncCheckResult;
    function IsRotatedTowards(node: CNode, optional maxAngle: float): bool;
    function IsRotatedTowardsPoint(point: Vector, optional maxAngle: float): bool;
    function SetAlive(flag: bool);
    function IsExternalyControlled(): bool;
    function IsMoving(): bool;
    function GetMoveDestination(): Vector;
    function GetPositionOrMoveDestination(): Vector;
    function GetVoicetag(): name;
    function EnableCollisions(val: bool);
    function PredictWorldPosition(inTime: float): Vector;
    function GetHeadAngleHorizontal(): float;
    function GetHeadAngleVertical(): float;
    function GetMovingAgentComponent(): CMovingAgentComponent;
    function GetMorphedMeshManagerComponent(): CMorphedMeshManagerComponent;
    function EnablePathEngineAgent(flag: bool);
    function EnableCollisionInfoReportingForItem(itemId: SItemUniqueId, enable: bool);
    function EnablePhysicalMovement(enable: bool): bool;
    function EnableStaticCollisions(enable: bool): bool;
    function EnableDynamicCollisions(enable: bool): bool;
    function EnableCharacterCollisions(enable: bool): bool;
    function PushInDirection(pusherPos: Vector, direction: Vector, optional speed: float, optional playAnimation: bool, optional applyRotation: bool);
    function PushAway(pusher: CMovingAgentComponent, optional strength: float, optional speed: float);
    function IsRagdollObstacle(): bool;
    function ForceAIBehavior(tree: IAITree, forceLevel: EArbitratorPriorities, optional forceEventName: name): int;
    function CancelAIBehavior(forceActionId: int): bool;
    function ClearRotationTarget();
    function SetRotationTarget(node: CNode, optional clamping: bool);
    function SetRotationTargetPos(position: Vector, optional clamping: bool);
    function InAttackRange(target: CGameplayEntity, optional rangeName: name): bool;
    function GetNearestPointInPersonalSpace(position: Vector): Vector;
    function GetNearestPointInPersonalSpaceAt(myPosition: Vector, otherPosition: Vector): Vector;
    function GatherEntitiesInAttackRange(out entities: array<CGameplayEntity>, optional rangeName: name);
    function SetAppearance(appearanceName: name);
    function GetAppearance(): name;
    function GetAnimationTimeMultiplier(): float;
    function CanStealOtherActor(actor: CActor): bool;
    function ResetClothAndDangleSimulation();
    function SetAnimationTimeMultiplier(mult: float);
    function IsAttackableByPlayer(): bool;
    function SetAttackableByPlayerPersistent(flag: bool);
    function SetAttackableByPlayerRuntime(flag: bool, optional timeout: float);
    function SetVisibility(isVisible: bool);
    function GetVisibility(): bool;
    function CalculateHeight(): float;
    function SetBehaviorMimicVariable(varName: name, varValue: float): bool;
    function DrawItems(instant: bool, itemId: SItemUniqueId, optional itemId2: SItemUniqueId, optional itemId3: SItemUniqueId): bool;
    function HolsterItems(instant: bool, itemId: SItemUniqueId, optional itemId2: SItemUniqueId, optional itemId3: SItemUniqueId): bool;
    function IssueRequiredItems(leftItem: name, rightItem: name);
    function SetRequiredItems(leftItem: name, rightItem: name);
    function IssueRequiredItemsGeneric(items: array<name>, slots: array<name>);
    function SetRequiredItemsGeneric(items: array<name>, slots: array<name>);
    function UseItem(itemId: SItemUniqueId): bool;
    function EmptyHands();
    function PlayLine(stringId: int, subtitle: bool);
    function PlayLineByStringKey(stringKey: string, subtitle: bool);
    function EndLine();
    function IsSpeaking(optional stringId: int): bool;
    function PlayMimicAnimationAsync(animation: name): bool;
    function PlayPushAnimation(pushDirection: EPushingDirection);
    function SetInteractionPriority(priority: EInteractionPriority);
    function GetInteractionPriority(): EInteractionPriority;
    function SetUnpushableTarget(target: CActor): CActor;
    function SetOriginalInteractionPriority(priority: EInteractionPriority);
    function RestoreOriginalInteractionPriority();
    function GetOriginalInteractionPriority(): EInteractionPriority;
    function SetGroupShadows(flag: bool);
    function WaitForFinishedAllLatentItemActions(): bool;
    function DrawItemsLatent(itemId: SItemUniqueId, optional itemId2: SItemUniqueId, optional itemId3: SItemUniqueId): bool;
    function HolsterItemsLatent(itemId: SItemUniqueId, optional itemId2: SItemUniqueId, optional itemId3: SItemUniqueId): bool;
    function DrawWeaponAndAttackLatent(itemId: SItemUniqueId): bool;
    function ProcessRequiredItems(optional instant: bool);
    function ActivateAndSyncBehaviorWithItemsParallel(names: name, optional timeout: float): bool;
    function ActivateAndSyncBehaviorWithItemsSequence(names: name, optional timeout: float): bool;
    function ActionMoveToNode(target: CNode, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveToNodeWithHeading(target: CNode, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveTo(target: Vector, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveToWithHeading(target: Vector, heading: float, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveToDynamicNode(target: CNode, moveType: EMoveType, absSpeed: float, range: float, optional keepDistance: bool, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveCustom(targeter: CMoveTRGScript): bool;
    function ActionSlideThrough(explorationAreaToUse: CActionAreaComponent): bool;
    function ActionSlideTo(target: Vector, duration: float): bool;
    function ActionMoveOnCurveTo(target: Vector, duration: float, rightShift: bool): bool;
    function ActionSlideToWithHeading(target: Vector, heading: float, duration: float, optional rotation: ESlideRotation): bool;
    function ActionMoveAwayFromNode(position: CNode, distance: float, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionMoveAwayFromLine(positionA: Vector, positionB: Vector, distance: float, makeMinimalMovement: bool, optional moveType: EMoveType, optional absSpeed: float, optional radius: float, optional failureAction: EMoveFailureAction): bool;
    function ActionRotateTo(target: Vector): bool;
    function ActionSetOrientation(orientation: float): bool;
    function ActionPlaySlotAnimation(slotName: name, animationName: name, optional blendIn: float, optional blendOut: float, optional continuePlaying: bool): bool;
    function ActionExitWork(optional fast: bool): bool;
    function ActionExploration(exploration: SExplorationQueryToken, optional listener: IScriptable, optional steeringGraphTargetNode: CNode): bool;
    function ActionAnimatedSlideToStatic(settings: SAnimatedSlideSettings, target: Vector, heading: float, translation: bool, rotation: bool): bool;
    function ActionAnimatedSlideTo(settings: SAnimatedSlideSettings, target: CNode, translation: bool, rotation: bool): bool;
    function ActionMatchTo(settings: SAnimatedSlideSettings, target: SActionMatchToTarget): bool;
}
class CNewNPC extends CActor
{
    var aiEnabled: bool;
    var suppressBroadcastingReactions: bool;
    var berserkTime: EngineTime;
    var npcGroupType: ENPCGroupType;

    function GetActiveActionPoint(): SActionPointId;
    function IsInInterior(): bool;
    function IsInDanger(): bool;
    function IsSeeingNonFriendlyNPC(): bool;
    function IsAIEnabled(): bool;
    function FindActionPoint(out apID: SActionPointId, out category: name);
    function GetDefaultDespawnPoint(out spawnPoint: Vector): bool;
    function NoticeActor(actor: CActor);
    function ForgetActor(actor: CActor);
    function ForgetAllActors();
    function GetNoticedObject(index: int): CActor;
    function GetPerceptionRange(): float;
    function PlayDialog(optional forceSpawnedActors: bool): bool;
    function GetReactionScript(index: int): CReactionScript;
    function IfCanSeePlayer(): bool;
    function GetGuardArea(): CAreaComponent;
    function SetGuardArea(areaComponent: CAreaComponent);
    function DeriveGuardArea(ncp: CNewNPC): bool;
    function IsConsciousAtWork(): bool;
    function GetCurrentJTType(): int;
    function IsInLeaveAction(): bool;
    function IsSittingAtWork(): bool;
    function IsAtWork(): bool;
    function IsPlayingChatScene(): bool;
    function CanUseChatInCurrentAP(): bool;
    function NoticeActorInGuardArea(actor: CActor);
    function IsMountedByPlayer(isMountedByPlayer: bool);
}
class CPlayer extends CActor
{
    var npcVoicesetCooldown: float;
    //var presenceInterestPoint: ?;
    //var slowMovementInterestPoint: ?;
    //var fastMovementInterestPoint: ?;
    //var weaponDrawnInterestPoint: ?;
    //var weaponDrawMomentInterestPoint: ?;
    //var visionInterestPoint: ?;
    var isMovable: bool;
    var enemyUpscaling: bool;

    function LockButtonInteractions(channel: int);
    function UnlockButtonInteractions(channel: int);
    function GetActiveExplorationEntity(): CEntity;
    function SetEnemyUpscaling(b: bool);
    function GetEnemyUpscaling(): bool;
}
class CR4Player extends CPlayer
{
    var uselessProp: EAsyncCheckResult;
    var horseWithInventory: EntityHandle;

    function GetEnemiesInRange(out enemies: array<CActor>);
    function GetVisibleEnemies(out enemies: array<CActor>);
    function IsEnemyVisible(enemy: CActor): bool;
    function SetupEnemiesCollection(range: float, heightTolerance: float, maxEnemies: int, optional tag: name, optional flags: int);
    function IsInInterior(): bool;
    function IsInSettlement(): bool;
    function EnterSettlement(isEntering: bool);
    function ActionDirectControl(controller: CR4LocomotionDirectController): bool;
    function SetPlayerTarget(target: CActor);
    function SetPlayerCombatTarget(target: CActor);
    function ObtainTicketFromCombatTarget(ticketName: name, ticketsCount: int);
    function FreeTicketAtCombatTarget();
    function SetScriptMoveTarget(target: CActor);
    function GetRiderData(): CAIStorageRiderData;
    function SetIsInCombat(inCombat: bool);
    function SaveLastMountedHorse(mountedHorse: CActor);
    function SetBacklightFromHealth(healthPercentage: float);
    function SetBacklightColor(color: Vector);
    function GetCombatDataComponent(): CCombatDataComponent;
    function GetTemplatePathAndAppearance(out templatePath: string, out appearance: name);
    function HACK_BoatDismountPositionCorrection(slotPos: Vector);
    function HACK_ForceGetBonePosition(boneIndex: int): Vector;
}
class CEncounter extends CGameplayEntity
{
    var enabled: bool;
    var ignoreAreaTrigger: bool;
    var fullRespawnScheduled: bool;
    //var spawnTree: ?;
    //var creatureDefinition: ?;
    var encounterParameters: CEncounterParameters;
    var spawnArea: EntityHandle;
    var fullRespawnDelay: GameTime;
    var isFullRespawnTimeInGameTime: bool;
    var fullRespawnTime: GameTime;
    var wasRaining: bool;
    var conditionRetestTimeout: float;
    var defaultImmediateDespawnConfiguration: SSpawnTreeDespawnConfiguration;
    var spawnTreeType: ESpawnTreeType;

    function GetPlayerDistFromArea(): float;
    function GetEncounterArea(): CTriggerAreaComponent;
    function IsPlayerInEncounterArea(): bool;
    function EnterArea();
    function LeaveArea();
    function IsEnabled(): bool;
    function EnableEncounter(enable: bool);
    function EnableMember(memberName: name, enable: bool);
    function ForceDespawnDetached();
    function SetSpawnPhase(phaseName: name): bool;
}
class CProjectileTrajectory extends CGameplayEntity
{
    var projectileName: name;
    var animatedOffset: Vector;
    var animatedTimeMultiplier: float;
    var bounceOfVelocityPreserve: float;
    var overlapAccuracy: float;
    var doWaterLevelTest: bool;
    var waterTestAccuracy: float;
    var caster: CEntity;
    var realCaster: CEntity;
    var radius: float;

    function Init(caster: CEntity);
    function ShootProjectileAtPosition(angle: float, velocity: float, target: Vector, optional range: float, optional collisionGroups: array<name>);
    function ShootProjectileAtNode(angle: float, velocity: float, target: CNode, optional range: float, optional collisionGroups: array<name>);
    function ShootProjectileAtBone(angle: float, velocity: float, target: CEntity, targetBone: name, optional range: float, optional collisionGroups: array<name>);
    function ShootCakeProjectileAtPosition(cakeAngle: float, cakeHeight: float, shootAngle: float, velocity: float, target: Vector, range: float, optional collisionGroups: array<name>);
    function BounceOff(collisionNormal: Vector, colliisonPosition: Vector);
    function IsBehindWall(testComponent: CComponent, optional collisionGroupsNames: array<name>): bool;
    function StopProjectile();
    function IsStopped(): bool;
    function SphereOverlapTest(radius: float, optional collisionGroups: array<name>);
}
class CR4JournalPlaceEntity extends CGameplayEntity
{
    var placeEntry: CJournalPath;

    function GetJournalPlaceEntry(): CJournalBase;
}
class CR4MapPinEntity extends CGameplayEntity
{
    var entityName: name;
    var customName: LocalizedString;
    var radius: float;
    var ignoreWhenExportingMapPins: bool;
}
class CR4FastTravelEntity extends CR4MapPinEntity
{
    var spotName: name;
    var groupName: name;
    var teleportWayPointTag: name;
    var canBeReachedByBoat: bool;
    var isHubExit: bool;
}
class CStorySceneSpawner extends CGameplayEntity
{
    var storyScene: CStoryScene;
    var inputName: string;
}
abstract class IBoidLairEntity extends CGameplayEntity
{
    var boidSpeciesName: name;
    var spawnFrequency: float;
    var spawnLimit: int;
    var totalLifetimeSpawnLimit: int;
    var lairBoundings: EntityHandle;
    var range: float;
    var visibilityRange: float;
}
abstract class CSwarmLairEntity extends IBoidLairEntity
{
    var defeatedStateFact: string;
    var defeatedStateFactValue: int;
    var lairDisabledAtStartup: bool;

    function Disable(disable: bool);
}
class CFlyingCrittersLairEntity extends CSwarmLairEntity
{
    //var scriptInput: ?;
    //var cellMapResourceFile: ?;
    var cellMapCellSize: float;
}
class CFlyingCrittersLairEntityScript extends CFlyingCrittersLairEntity
{
    function GetPoiCountByType(poiType: name): int;
    function GetSpawnPointArray(out spawnPointArray: array<name>);
}
class W3Boat extends CGameplayEntity
{
    var teleportedFromOtherHUB: bool;

    function HasDrowned(): bool;
    function SetHasDrowned(val: bool);
    function SetTeleportedFromOtherHUB(val: bool);
}
class W3BoatSpawner extends CGameplayEntity
{
}
class W3LockableEntity extends CGameplayEntity
{
    var isEnabledOnSpawn: bool;
    var lockedByKey: bool;
}
class W3Container extends W3LockableEntity
{
    var shouldBeFullySaved: bool;

    function SetIsQuestContainer(isQuest: bool);
}
class CBeehiveEntity extends W3Container
{
}
class W3ToxicCloud extends CGameplayEntity
{
}
class CStoryScenePlayer extends CEntity
{
    var storyScene: CStoryScene;
    var injectedScenes: array<CStoryScene>;
    var isPaused: Uint16;
    var isGameplay: bool;

    function DbFactAdded(factName: string);
    function DbFactRemoved(factName: string);
    function RestartScene();
    function RestartSection();
    function GetSceneWorldPos(): Vector;
}
class CR4FinisherDLC
{
    var finisherAnimName: name;
    var woundName: name;
    var finisherSide: EFinisherSide;
    var leftCameraAnimName: name;
    var rightCameraAnimName: name;
    var frontCameraAnimName: name;
    var backCameraAnimName: name;

    function IsFinisherForAnim(eventAnimInfo: SAnimationEventAnimInfo): bool;
}
class CR4GlobalEventsScriptsDispatcher
{
    function RegisterForCategoryFilterName(eventCategory: EGlobalEventCategory, filter: name): bool;
    function RegisterForCategoryFilterNameArray(eventCategory: EGlobalEventCategory, filter: array<name>): bool;
    function RegisterForCategoryFilterString(eventCategory: EGlobalEventCategory, filter: string): bool;
    function RegisterForCategoryFilterStringArray(eventCategory: EGlobalEventCategory, filter: array<string>): bool;
    function UnregisterFromCategoryFilterName(eventCategory: EGlobalEventCategory, filter: name): bool;
    function UnregisterFromCategoryFilterNameArray(eventCategory: EGlobalEventCategory, filter: array<name>): bool;
    function UnregisterFromCategoryFilterString(eventCategory: EGlobalEventCategory, filter: string): bool;
    function UnregisterFromCategoryFilterStringArray(eventCategory: EGlobalEventCategory, filter: array<string>): bool;
    function AddFilterNameForCategory(eventCategory: EGlobalEventCategory, filter: name): bool;
    function AddFilterNameArrayForCategory(eventCategory: EGlobalEventCategory, filter: array<name>): bool;
    function AddFilterStringForCategory(eventCategory: EGlobalEventCategory, filter: string): bool;
    function AddFilterStringArrayForCategory(eventCategory: EGlobalEventCategory, filter: array<string>): bool;
    function RemoveFilterNameFromCategory(eventCategory: EGlobalEventCategory, filter: name): bool;
    function RemoveFilterNameArrayFromCategory(eventCategory: EGlobalEventCategory, filter: array<name>): bool;
    function RemoveFilterStringFromCategory(eventCategory: EGlobalEventCategory, filter: string): bool;
    function RemoveFilterStringArrayFromCategory(eventCategory: EGlobalEventCategory, filter: array<string>): bool;
}
class CR4KinectSpeechRecognizerListenerScriptProxy
{
    function IsSupported(): bool;
    function IsEnabled(): bool;
    function SetEnabled(enable: bool);
}
abstract class CR4LocomotionDirectController
{
    var agent: CMovingAgentComponent;
    var moveSpeed: float;
    var moveRotation: float;
}
class CR4LocomotionDirectControllerScript extends CR4LocomotionDirectController
{
}
class CR4PhotomodeEffects
{
    function SetEnabled(value: bool);
    function SetDofEnabled(value: bool);
    function SetAperture(value: float);
    function GetAperture(): float;
    function SetFocusDistance(value: float);
    function GetFocusDistance(): float;
    function SetAutoFocus(value: bool);
    function GetAutoFocus(): bool;
    function SetExposure(value: float);
    function GetExposure(): float;
    function SetContrast(value: float);
    function GetContrast(): float;
    function SetHighlights(value: float);
    function GetHighlights(): float;
    function SetSaturation(value: float);
    function GetSaturation(): float;
    function SetTemperature(value: float);
    function GetTemperature(): float;
    function SetChromaticAberration(value: float);
    function GetChromaticAberration(): float;
    function SetFilmGrain(value: float);
    function GetFilmGrain(): float;
    function SetVignette(value: float);
    function GetVignette(): float;
}
class CR4SecondScreenManagerScriptProxy
{
    function SendGlobalMapPins(mappins: array<SCommonMapPinInstance>);
    function SendAreaMapPins(areaType: int, mappins: array<SCommonMapPinInstance>);
    function SendGameMenuOpen();
    function SendGameMenuClose();
    function SendFastTravelEnable();
    function SendFastTravelDisable();
    function PrintJsonObjectsMemoryUsage();
}
class CR4TelemetryScriptProxy
{
    function LogWithName(eventType: ER4TelemetryEvents);
    function LogWithLabel(eventType: ER4TelemetryEvents, label: string);
    function LogWithValue(eventType: ER4TelemetryEvents, value: int);
    function LogWithValueStr(eventType: ER4TelemetryEvents, value: string);
    function LogWithLabelAndValue(eventType: ER4TelemetryEvents, label: string, value: int);
    function LogWithLabelAndValueStr(eventType: ER4TelemetryEvents, label: string, value: string);
    function SetCommonStatFlt(statType: ER4CommonStats, value: float);
    function SetCommonStatI32(statType: ER4CommonStats, value: int);
    function SetGameProgress(value: float);
    function AddSessionTag(tag: string);
    function RemoveSessionTag(tag: string);
    function XDPPrintUserStats(statisticName: string);
    function XDPPrintUserAchievement(achievementName: string);
    function TelemetryConsentChanged(telemetryConsent: bool);
    function WasConsentWindowShown(): bool;
    function MarkShownConsentWindow();
    function NoticeMenuDepth(d: int);
}
class CR4WorldDLCExtender
{
    function AreaTypeToName(areaType: int): string;
    function AreaNameToType(areaName: string): int;
    function GetMiniMapSize(areaType: int): float;
    function GetMiniMapTileCount(areaType: int): int;
    function GetMiniMapExteriorTextureSize(areaType: int): int;
    function GetMiniMapInteriorTextureSize(areaType: int): int;
    function GetMiniMapTextureSize(areaType: int): int;
    function GetMiniMapMinLod(areaType: int): int;
    function GetMiniMapMaxLod(areaType: int): int;
    function GetMiniMapExteriorTextureExtension(areaType: int): string;
    function GetMiniMapInteriorTextureExtension(areaType: int): string;
    function GetMiniMapVminX(areaType: int): int;
    function GetMiniMapVmaxX(areaType: int): int;
    function GetMiniMapVminY(areaType: int): int;
    function GetMiniMapVmaxY(areaType: int): int;
    function GetMiniMapSminX(areaType: int): int;
    function GetMiniMapSmaxX(areaType: int): int;
    function GetMiniMapSminY(areaType: int): int;
    function GetMiniMapSmaxY(areaType: int): int;
    function GetMiniMapMinZoom(areaType: int): float;
    function GetMiniMapMaxZoom(areaType: int): float;
    function GetMiniMapZoom12(areaType: int): float;
    function GetMiniMapZoom23(areaType: int): float;
    function GetMiniMapZoom34(areaType: int): float;
    function GetGradientScale(areaType: int): float;
    function GetPreviewHeight(areaType: int): float;
}
class CReactionScene
{
}
class CReactionsManager
{
    function BroadcastStaticInterestPoint(interestPoint: CInterestPoint, position: Vector, optional timeout: float);
    function BroadcastDynamicInterestPoint(interestPoint: CInterestPoint, node: CNode, optional timeout: float);
    function SendStaticInterestPoint(target: CNewNPC, interestPoint: CInterestPoint, position: Vector, optional timeout: float);
    function SendDynamicInterestPoint(target: CNewNPC, interestPoint: CInterestPoint, node: CNode, optional timeout: float);
}
abstract class CResource
{
    function GetPath(): string;
}
class C2dArray extends CResource
{
    var headers: array<string>;
    var data: array<array<string>>;

    function GetValueAt(column: int, row: int): string;
    function GetValue(header: string, row: int): string;
    function GetValueAtAsName(column: int, row: int): name;
    function GetValueAsName(header: string, row: int): name;
    function GetNumRows(): int;
    function GetRowIndexAt(column: int, value: string): int;
    function GetRowIndex(header: string, value: string): int;
}
class CIndexed2dArray extends C2dArray
{
    function GetRowIndexByKey(key: name): int;
}
class CBehTree extends CResource
{
    var rootNode: IBehTreeNodeDefinition;
}
class CBehaviorGraph extends CResource
{
    //var defaultStateMachine: ?;
    //var stateMachines: ?;
    var sourceDataRemoved: bool;
    var customTrackNames: array<name>;
    var generateEditorFragments: bool;
    //var poseSlots: ?;
    //var animSlots: ?;
}
class CCollisionMesh extends CResource
{
    //var shapes: ?;
    var occlusionAttenuation: float;
    var occlusionDiagonalLimit: float;
    var swimmingRotationAxis: int;
}
class CCubeTexture extends CResource
{
}
class CDLCDefinition extends CResource
{
    var id: name;
    var localizedNameKey: string;
    var localizedDescriptionKey: string;
    //var mounters: ?;
    var languagePacks: array<SDLCLanguagePack>;
    var initiallyEnabled: bool;
    var visibleInDLCMenu: bool;
    var requiredByGameSave: bool;
}
class CEntityTemplate extends CResource
{
    var includes: array<CEntityTemplate>;
    var overrides: array<SEntityTemplateOverride>;
    var properOverrides: bool;
    var backgroundOffset: Vector;
    var dataCompilationTime: CDateTime;
    var entityClass: name;
    //var entityObject: ?;
    var bodyParts: array<CEntityBodyPart>;
    var appearances: array<CEntityAppearance>;
    var usedAppearances: array<name>;
    var voicetagAppearances: array<VoicetagAppearancePair>;
    //var effects: ?;
    var slots: array<EntitySlot>;
    //var templateParams: ?;
    var coloringEntries: array<SEntityTemplateColoringEntry>;
    var instancePropEntries: array<SComponentInstancePropertyEntry>;
    var flatCompiledData: array<byte>;
    var streamedAttachments: array<SStreamedAttachment>;
    var cookedEffects: array<CEntityTemplateCookedEffect>;
    var cookedEffectsVersion: Uint32;
}
class CEnvironmentDefinition extends CResource
{
    var envParams: CAreaEnvironmentParams;
}
class CExtAnimEventsFile extends CResource
{
    var requiredSfxTag: name;
}
class CSkeletalAnimationSet extends CExtAnimEventsFile
{
    //var animations: ?;
    var extAnimEvents: array<CExtAnimEventsFile>;
    var skeleton: CSkeleton;
    //var compressedPoses: ?;
    //var `Streaming option`: ?;
    //var `Number of non-streamable bones`: ?;
}
class CFormation extends CResource
{
    var uniqueFormationName: name;
    //var formationLogic: ?;
    var steeringGraph: CMoveSteeringBehavior;
}
abstract class CGameResource extends CResource
{
    function GetDefaultCameraTemplate(): CEntityTemplate;
}
abstract class CCommonGameResource extends CGameResource
{
}
class CJobTree extends CResource
{
    //var jobTreeRootNode: ?;
    var movementMode: EJobMovementMode;
    var customMovementSpeed: float;
    var settings: SJobTreeSettings;
}
class CJournalResource extends CResource
{
    //var `entry`: ?;

    function GetEntry(): CJournalBase;
}
class CLayer extends CResource
{
    //var entities: ?;
    var sectorData: CSectorData;
    var nameCount: Uint32;
}
abstract class CMeshTypeResource extends CResource
{
}
abstract class CApexResource extends CMeshTypeResource
{
}
class CMesh extends CMeshTypeResource
{
    var materials: array<IMaterial>;
    var boundingBox: Box;
    var autoHideDistance: float;
    var isTwoSided: bool;
    var collisionMesh: CCollisionMesh;
    var useExtraStreams: bool;
    var generalizedMeshRadius: float;
    var mergeInGlobalShadowMesh: bool;
    var isOccluder: bool;
    var smallestHoleOverride: float;
    var chunks: array<SMeshChunkPacked>;
    var rawVertices: DeferredDataBuffer;
    var rawIndices: DeferredDataBuffer;
    var isStatic: bool;
    var entityProxy: bool;
    var cookedData: SMeshCookedData;
    //var soundInfo: ?;
    var internalVersion: byte;
    var chunksBuffer: DeferredDataBuffer;
}
class CPhysicsDestructionResource extends CMesh
{
    var boneIndicesMapping: array<SBoneIndiceMapping>;
    var finalIndices: array<Uint16>;
    var chunkNumber: Uint32;
}
class CMoveSteeringBehavior extends CResource
{
    //var root: ?;
}
class CRagdoll extends CResource
{
    var windScaler: float;
    var densityScaler: float;
    var autoStopDelay: float;
    var autoStopTime: float;
    var autoStopSpeed: float;
    var resetDampingAfterStop: bool;
    var forceWakeUpOnAttach: bool;
    var customDynamicGroup: CPhysicalCollision;
    var disableConstrainsTwistAxis: bool;
    var disableConstrainsSwing1Axis: bool;
    var disableConstrainsSwing2Axis: bool;
    var jointBounce: float;
    var modifyTwistLower: float;
    var modifyTwistUpper: float;
    var modifySwingY: float;
    var modifySwingZ: float;
    var projectionIterations: int;
}
class CResourceSimplexTree extends CResource
{
    var nodes: array<SSimplexTreeStruct>;
}
class CSkeleton extends CResource
{
    var lodBoneNum_1: int;
    var walkSpeed: float;
    var slowRunSpeed: float;
    var fastRunSpeed: float;
    var sprintSpeed: float;
    var walkSpeedRel: float;
    var slowRunSpeedRel: float;
    var fastRunSpeedRel: float;
    var sprintSpeedRel: float;
    //var poseCompression: ?;
    //var bboxGenerator: ?;
    //var controlRigDefinition: ?;
    //var controlRigDefaultPropertySet: ?;
    //var skeletonMappers: ?;
    //var controlRigSettings: ?;
    //var teleportDetectorData: ?;
    var lastNonStreamableBoneName: name;
    var bones: array<SSkeletonBone>;
    var tracks: array<SSkeletonTrack>;
    var parentIndices: array<Int16>;
}
class CSourceTexture extends CResource
{
    var width: Uint32;
    var height: Uint32;
    var pitch: Uint32;
    var format: ETextureRawFormat;
}
class CStoryScene extends CResource
{
    //var controlParts: ?;
    //var sections: ?;
    var elementIDCounter: Uint32;
    var sectionIDCounter: Uint32;
    var sceneId: Uint32;
    //var sceneTemplates: ?;
    //var sceneProps: ?;
    //var sceneEffects: ?;
    //var sceneLights: ?;
    var mayActorsStartWorking: bool;
    var surpassWaterRendering: bool;
    //var dialogsetInstances: ?;
    var cameraDefinitions: array<StorySceneCameraDefinition>;
    var banksDependency: array<name>;
    var blockMusicTriggers: bool;
    var muteSpeechUnderWater: bool;
    var soundListenerOverride: string;
    var soundEventsOnEnd: array<name>;
    var soundEventsOnSkip: array<name>;

    function GetRequiredPositionTags(): array<name>;
}
class CSwitchableFoliageResource extends CResource
{
    var entries: array<SSwitchableFoliageEntry>;
}
class CTextureArray extends CResource
{
    var bitmaps: array<CTextureArrayEntry>;
    var textureGroup: name;
}
class CUmbraScene extends CResource
{
    var distanceMultiplier: float;
    var localUmbraOccThresholdMul: CResourceSimplexTree;
}
class CWorld extends CResource
{
    var startupCameraPosition: Vector;
    var startupCameraRotation: EulerAngles;
    //var terrainClipMap: ?;
    var newLayerGroupFormat: bool;
    var hasEmbeddedLayerInfos: bool;
    var initialyHidenLayerGroups: C2dArray;
    var umbraScene: CUmbraScene;
    //var globalWater: ?;
    //var pathLib: ?;
    var worldDimensions: float;
    var shadowConfig: CWorldShadowConfig;
    var environmentParameters: SWorldEnvironmentParameters;
    var soundBanksDependency: array<name>;
    var soundEventsOnAttach: array<StringAnsi>;
    var soundEventsOnDetach: array<StringAnsi>;
    //var foliageScene: ?;
    var playGoChunks: array<name>;
    var minimapsPath: string;
    var hubmapsPath: string;
    //var mergedGeometry: ?;

    function ShowLayerGroup(layerGroupName: string);
    function HideLayerGroup(layerGroupName: string);
    function PointProjectionTest(point: Vector, normal: EulerAngles, range: float): bool;
    function StaticTrace(pointA: Vector, pointB: Vector, out position: Vector, out normal: Vector, optional collisionGroupsNames: array<name>): bool;
    function StaticTraceWithAdditionalInfo(pointA: Vector, pointB: Vector, out position: Vector, out normal: Vector, out material: name, out component: CComponent, optional collisionGroupsNames: array<name>): bool;
    function SweepTest(pointA: Vector, pointB: Vector, radius: float, out position: Vector, out normal: Vector, optional collisionGroupsNames: array<name>): bool;
    function SphereOverlapTest(out entities: array<CEntity>, position: Vector, radius: float, optional collisionGroupsNames: array<name>): int;
    function GetWaterLevel(point: Vector, optional dontUseApproximation: bool): float;
    function GetWaterDepth(point: Vector, optional dontUseApproximation: bool): float;
    function GetWaterTangent(point: Vector, direction: Vector, optional resolution: float): Vector;
    function ForceUpdateWaterOnPause(force: bool);
    function NavigationLineTest(pos1: Vector, pos2: Vector, radius: float, optional ignoreObstacles: bool, optional noEndpointZ: bool): bool;
    function NavigationCircleTest(position: Vector, radius: float, optional ignoreObstacles: bool): bool;
    function NavigationClosestObstacleToLine(pos1: Vector, pos2: Vector, radius: float, out closestPointOnLine: Vector, out closestPointOnGeometry: Vector, optional ignoreObstacles: bool): float;
    function NavigationClosestObstacleToCircle(position: Vector, radius: float, out closestPointOnGeometry: Vector, optional ignoreObstacles: bool): float;
    function NavigationClearLineInDirection(pos1: Vector, pos2: Vector, radius: float, out closestPointOnLine: Vector): bool;
    function NavigationFindSafeSpot(position: Vector, personalSpace: float, searchRadius: float, out outSafeSpot: Vector): bool;
    function NavigationComputeZ(position: Vector, zMin: float, zMax: float, out z: float): bool;
    function PhysicsCorrectZ(position: Vector, out z: float): bool;
    function GetDepotPath(): string;
    function ForceGraphicalLOD(lodLevel: int);
    function GetTerrainParameters(out terrainSize: float, out tilesCount: int): bool;
    function GetTraceManager(): CScriptBatchQueryAccessor;
    function GetCameraDirector(): CCameraDirector;
}
abstract class IMaterial extends CResource
{
}
class CMaterialInstance extends IMaterial
{
    var baseMaterial: IMaterial;
    var enableMask: bool;
}
abstract class ITexture extends CResource
{
}
class CBitmapTexture extends ITexture
{
    var width: Uint32;
    var height: Uint32;
    var format: ETextureRawFormat;
    var compression: ETextureCompression;
    var sourceData: CSourceTexture;
    var textureGroup: name;
    var pcDownscaleBias: int;
    var xboneDownscaleBias: int;
    var ps4DownscaleBias: int;
    var residentMipIndex: byte;
    var textureCacheKey: Uint32;
}
class CScriptBatchQueryAccessor
{
    function RayCast(start: Vector, end: Vector, out result: array<SRaycastHitResult>, optional collisionGroupsNames: array<name>, optional queryFlags: int): bool;
    function RayCastDir(start: Vector, direction: Vector, distance: float, out result: array<SRaycastHitResult>, optional collisionGroupsNames: array<name>, optional queryFlags: int): bool;
    function RayCastSync(start: Vector, end: Vector, out result: array<SRaycastHitResult>, optional collisionGroupsNames: array<name>): bool;
    function RayCastDirSync(start: Vector, direction: Vector, distance: float, out result: array<SRaycastHitResult>, optional collisionGroupsNames: array<name>): bool;
    function RayCastAsync(start: Vector, end: Vector, optional collisionGroupsNames: array<name>, optional queryFlags: int): SScriptRaycastId;
    function RayCastDirAsync(start: Vector, direction: Vector, distance: float, optional collisionGroupsNames: array<name>, optional queryFlags: int): SScriptRaycastId;
    function GetRayCastState(queryId: SScriptRaycastId, out result: array<SRaycastHitResult>): EBatchQueryState;
    function Sweep(start: Vector, end: Vector, radius: float, out result: array<SSweepHitResult>, optional collisionGroupsNames: array<name>, optional queryFlags: int): bool;
    function SweepDir(start: Vector, direction: Vector, radius: float, distance: float, out result: array<SSweepHitResult>, optional collisionGroupsNames: array<name>, optional queryFlags: int): bool;
    function SweepAsync(start: Vector, end: Vector, radius: float, optional collisionGroupsNames: array<name>, optional queryFlags: int): SScriptSweepId;
    function SweepDirAsync(start: Vector, direction: Vector, radius: float, distance: float, optional collisionGroupsNames: array<name>, optional queryFlags: int): SScriptSweepId;
    function GetSweepState(queryId: SScriptSweepId, out result: array<SSweepHitResult>): EBatchQueryState;
}
class CScriptSoundSystem
{
    function SoundState(stateGroupName: string, optional stateName: string);
    function SoundSwitch(swichGroupName: string, optional stateName: string);
    function SoundEvent(eventName: string);
    function SoundParameter(parameterName: string, value: float, optional duration: float);
    function SoundGlobalParameter(parameterName: string, value: float, optional duration: float);
    function SoundSequence(sequenceName: string, sequence: array<string>);
    function SoundEventAddToSave(eventName: string);
    function SoundEventClearSaved();
    function SoundEnableMusicEvents(how: bool);
    function SoundLoadBank(bankName: string, async: bool);
    function SoundUnloadBank(bankName: string);
    function SoundIsBankLoaded(bankName: string): bool;
    function EnableMusicDebug(enable: bool);
    function TimedSoundEvent(duration: float, optional startEvent: string, optional stopEvent: string, optional shouldUpdateTimeParameter: bool);
    function MuteSpeechUnderwater(mute: bool);
}
class CScriptedRenderFrame
{
    function DrawText(text: string, position: Vector, color: Color);
    function DrawSphere(position: Vector, radius: float, color: Color);
    function DrawLine(start: Vector, end: Vector, color: Color);
}
class CSpawnTreeEntrySubDefinition
{
    var id: Uint64;
    var creatureDefinition: name;
    var partyMemberId: name;
    var creatureCount: Uint32;
    //var initializers: ?;
}
class CStorySceneLinkElement
{
    //var linkedElements: ?;
    //var nextLinkElement: ?;
}
abstract class CStorySceneControlPart extends CStorySceneLinkElement
{
    var comment: string;
}
class CStorySceneOutput extends CStorySceneControlPart
{
    //var `name`: ?;
    var questOutput: bool;
    var endsWithBlackscreen: bool;
    var blackscreenColor: Color;
    var gameplayCameraBlendTime: float;
    var environmentLightsBlendTime: float;
    var gameplayCameraUseFocusTarget: bool;
}
class CTicketsDefaultConfiguration
{
    var tickets: array<CTicketSourceConfiguration>;

    function SetupTicketSource(ticketName: name, ticketPoolSize: int, minimalImportance: float);
}
class CTimerScriptKeyword
{
    var timeDelta: float;
    var timeDeltaUnscaled: float;
}
class CVisualDebug
{
    function AddText(dbgName: name, text: string, optional position: Vector, optional absolutePos: bool, optional line: byte, optional color: Color, optional background: bool, optional timeout: float);
    function AddSphere(dbgName: name, radius: float, optional position: Vector, optional absolutePos: bool, optional color: Color, optional timeout: float);
    function AddBox(dbgName: name, size: Vector, optional position: Vector, optional rotation: EulerAngles, optional absolutePos: bool, optional color: Color, optional timeout: float);
    function AddAxis(dbgName: name, optional scale: float, optional position: Vector, optional rotation: EulerAngles, optional absolutePos: bool, optional timeout: float);
    function AddLine(dbgName: name, optional startPosition: Vector, optional endPosition: Vector, optional absolutePos: bool, optional color: Color, optional timeout: float);
    function AddArrow(dbgName: name, optional start: Vector, optional end: Vector, optional arrowPostionOnLine01: float, optional arrowSizeX: float, optional arrowSizeY: float, optional absolutePos: bool, optional color: Color, optional overlay: bool, optional timeout: float);
    function AddBar(dbgName: name, x: int, y: int, width: int, height: int, progress: float, color: Color, optional text: string, optional timeout: float);
    function AddBarColorSmooth(dbgName: name, x: int, y: int, width: int, height: int, progress: float, color: Color, optional text: string, optional timeout: float);
    function AddBarColorAreas(dbgName: name, x: int, y: int, width: int, height: int, progress: float, optional text: string, optional timeout: float);
    function RemoveText(dbgName: name);
    function RemoveSphere(dbgName: name);
    function RemoveBox(dbgName: name);
    function RemoveAxis(dbgName: name);
    function RemoveLine(dbgName: name);
    function RemoveArrow(dbgName: name);
    function RemoveBar(dbgName: name);
}
abstract class IActorConditionType
{
    var inverted: bool;
}
class CQCActorScriptedCondition extends IActorConditionType
{
}
abstract class IBehTreeNodeDefinition
{
}
abstract class ICustomCameraBaseController
{
    var controllerName: name;
}
abstract class ICustomCameraPivotDistanceController extends ICustomCameraBaseController
{
    var minDist: float;
    var maxDist: float;

    function Update(out currDistance: float, out currVelocity: float, timeDelta: float);
    function SetDesiredDistance(distance: float, optional mult: float);
}
class ICustomCameraScriptedPivotDistanceController extends ICustomCameraPivotDistanceController
{
}
class ICustomCameraScriptedCurveSetPivotDistanceController extends ICustomCameraScriptedPivotDistanceController
{
    //var curveSet: ?;
    var curveNames: array<name>;

    function FindCurve(curveName: name): CCurve;
}
abstract class ICustomCameraPivotPositionController extends ICustomCameraBaseController
{
    var offsetZ: float;
    var pivotZSmoothTime: float;

    function Update(out currPosition: Vector, out currVelocity: Vector, timeDelta: float);
    function SetDesiredPosition(position: Vector, optional mult: float);
}
class CCustomCameraRopePPC extends ICustomCameraPivotPositionController
{
    var dampFactor: float;
    var smoothZ: float;
    var ropeLength: float;
}
class CCustomCameraBoatPPC extends CCustomCameraRopePPC
{
    function SetPivotOffset(offset: Vector);
}
class ICustomCameraScriptedPivotPositionController extends ICustomCameraPivotPositionController
{
}
class ICustomCameraScriptedCurveSetPivotPositionController extends ICustomCameraScriptedPivotPositionController
{
    //var curveSet: ?;
    var curveNames: array<name>;

    function FindCurve(curveName: name): CCurve;
}
abstract class ICustomCameraPivotRotationController extends ICustomCameraBaseController
{
    var minPitch: float;
    var maxPitch: float;
    var sensitivityPreset: EInputSensitivityPreset;

    function Update(out currRotation: EulerAngles, out currVelocity: EulerAngles, timeDelta: float);
    function SetDesiredHeading(heading: float, optional mult: float);
    function SetDesiredPitch(pitch: float, optional mult: float);
    function RotateHorizontal(right: bool, optional mult: float);
    function RotateVertical(up: bool, optional mult: float);
    function StopRotating();
}
class ICustomCameraScriptedPivotRotationController extends ICustomCameraPivotRotationController
{
}
class ICustomCameraScriptedCurveSetPivotRotationController extends ICustomCameraScriptedPivotRotationController
{
    //var curveSet: ?;
    var curveNames: array<name>;

    function FindCurve(curveName: name): CCurve;
}
abstract class ICustomCameraPositionController extends ICustomCameraBaseController
{
    var enableAutoCollisionAvoidance: bool;
    var enableScreenSpaceCorrections: bool;
}
class ICustomCameraScriptedPositionController extends ICustomCameraPositionController
{
}
class ICustomCameraScriptedCurveSetPositionController extends ICustomCameraScriptedPositionController
{
    //var curveSet: ?;
    var curveNames: array<name>;

    function FindCurve(curveName: name): CCurve;
}
abstract class IEntityStateChangeRequest
{
}
class CEnableDeniedAreaRequest extends IEntityStateChangeRequest
{
    var enable: bool;
}
class CPlaySoundOnActorRequest extends IEntityStateChangeRequest
{
    var boneName: name;
    var soundName: StringAnsi;
    var fadeTime: float;

    function Initialize(boneName: name, soundName: string, optional fadeTime: float);
}
class CScriptedEntityStateChangeRequest extends IEntityStateChangeRequest
{
}
abstract class IGameSystem
{
}
class CCityLightManager extends IGameSystem
{
    function SetEnabled(toggle: bool);
    function IsEnabled(): bool;
    function ForceUpdate();
    function SetUpdateEnabled(value: bool);
    function DebugToggleAll(toggle: bool);
}
class CCommonMapManager extends IGameSystem
{
    function InitializeMinimapManager(minimapModule: CR4HudModule);
    function SetHintWaypointParameters(maxRemovalDistance: float, minPlacingDistance: float, refreshInterval: float, pathfindingTolerance: float, maxCount: int);
    function OnChangedMinimapRadius(radius: float, zoom: float);
    function IsFastTravellingEnabled(): bool;
    function EnableFastTravelling(enable: bool);
    function IsEntityMapPinKnown(tag: name): bool;
    function SetEntityMapPinKnown(tag: name, optional set: bool);
    function IsEntityMapPinDiscovered(tag: name): bool;
    function SetEntityMapPinDiscovered(tag: name, optional set: bool);
    function IsEntityMapPinDisabled(tag: name): bool;
    function SetEntityMapPinDisabled(tag: name, optional set: bool);
    function IsQuestPinType(type: name): bool;
    function IsUserPinType(type: name): bool;
    function GetUserPinNames(out names: array<name>);
    function ShowKnownEntities(show: bool);
    function CanShowKnownEntities(): bool;
    function ShowDisabledEntities(show: bool);
    function CanShowDisabledEntities(): bool;
    function ShowFocusClues(optional show: bool);
    function ShowHintWaypoints(optional show: bool);
    function AddQuestLootContainer(container: CEntity);
    function DeleteQuestLootContainer(container: CEntity);
    function CacheMapPins();
    function GetMapPinInstances(worldPath: string): array<SCommonMapPinInstance>;
    function GetMapPinTypeByTag(mapPinTag: name): name;
    function GetHighlightedMapPinTag(): name;
    function TogglePathsInfo(optional toggle: bool);
    function ToggleQuestAgentsInfo(optional toggle: bool);
    function ToggleShopkeepersInfo(optional toggle: bool);
    function ToggleInteriorInfo(optional toggle: bool);
    function ToggleUserPinsInfo(optional toggle: bool);
    function TogglePinsInfo(optional flags: int);
    function ExportGlobalMapPins();
    function ExportEntityMapPins();
    function GetAreaMapPins(): array<SAreaMapPinInfo>;
    function GetEntityMapPins(worldPath: string): array<SEntityMapPinInfo>;
    function UseMapPin(pinTag: name, onStart: bool): bool;
    function UseInteriorsForQuestMapPins(use: bool);
    function EnableShopkeeper(tag: name, enable: bool);
    function EnableMapPath(tag: name, enable: bool, lineWidth: float, segmentLength: float, color: Color);
    function EnableDynamicMappin(tag: name, enable: bool, type: name, optional useAgents: bool);
    function InvalidateStaticMapPin(entityName: name);
    function ToggleUserMapPin(area: EAreaName, position: Vector, type: int, fromSelectionPanel: bool, out indexToAdd: int, out indexToRemove: int): int;
    function GetUserMapPinLimits(out waypointPinLimit: int, out otherPinLimit: int): int;
    function GetUserMapPinCount(): int;
    function GetUserMapPinByIndex(index: int, out id: int, out area: int, out mapPinX: float, out mapPinY: float, out type: int): bool;
    function GetUserMapPinIndexById(id: int): int;
    function GetIdOfFirstUser1MapPin(id: int): bool;
    function GetCurrentArea(): int;
    function NotifyPlayerEnteredBorder(interval: float, position: Vector, rotation: EulerAngles): int;
    function NotifyPlayerExitedBorder(): int;
    function IsWorldAvailable(area: int): bool;
    function GetWorldContentTag(area: int): name;
    function GetWorldPercentCompleted(area: int): int;
    function DisableMapPin(pinName: string, disable: bool): bool;
    function GetDisabledMapPins(): array<string>;
}
class CCommunitySystem extends IGameSystem
{
    var apMan: CActionPointManager;
    var communitySpawnInitializer: ISpawnTreeInitializerAI;
}
class CFocusModeController extends IGameSystem
{
    function SetActive(active: bool);
    function IsActive(): bool;
    function GetIntensity(): float;
    function EnableVisuals(enable: bool, optional desaturation: float, optional highlightBoos: float);
    function SetFadeParameters(NearFadeDistance: float, FadeDistanceRange: float, dimmingTIme: float, dimmingSpeed: float);
    function EnableExtendedVisuals(enable: bool, fadeTime: float);
    function SetDimming(enable: bool);
    function SetSoundClueEventNames(entity: CGameplayEntity, eventStart: name, eventStop: name, effectType: int): bool;
    function ActivateScentClue(entity: CEntity, effectName: name, duration: float);
    function DeactivateScentClue(entity: CEntity);
}
class CGameFastForwardSystem extends IGameSystem
{
    function BeginFastForward(optional dontSpawnHostilesClose: bool, optional coverWithBlackscreen: bool);
    function AllowFastForwardSelfCompletion();
    function RequestFastForwardShutdown(optional coverWithBlackscreen: bool);
    function EndFastForward();
}
class CGameplayFXSurfacePost extends IGameSystem
{
    function AddSurfacePostFXGroup(position: Vector, fadeInTime: float, activeTime: float, fadeOutTime: float, range: float, type: int);
    function IsActive(): bool;
}
class CInteractionsManager extends IGameSystem
{
}
class CJournalManager extends IGameSystem
{
    function ActivateEntry(journalEntry: CJournalBase, optional status: EJournalStatus, optional showInfoOnScreen: bool, optional activateParents: bool);
    function GetEntryStatus(journalEntry: CJournalBase): EJournalStatus;
    function GetEntryIndex(journalEntry: CJournalBase): int;
    function IsEntryUnread(journalEntry: CJournalBase): bool;
    function SetEntryUnread(journalEntry: CJournalBase, isUnread: bool);
    function GetNumberOfActivatedOfType(type: name): int;
    function GetActivatedOfType(type: name, out entries: array<CJournalBase>);
    function GetNumberOfActivatedChildren(parentEntry: CJournalBase): int;
    function GetActivatedChildren(parentEntry: CJournalBase, out entries: array<CJournalBase>);
    function GetNumberOfAllChildren(parentEntry: CJournalBase): int;
    function GetAllChildren(parentEntry: CJournalBase, out entries: array<CJournalBase>);
    function GetEntryByTag(tag: name): CJournalBase;
    function GetEntryByString(str: string): CJournalBase;
    function GetEntryByGuid(guid: CGUID): CJournalBase;
}
class CWitcherJournalManager extends CJournalManager
{
    function SetTrackedQuest(journalEntry: CJournalBase);
    function GetTrackedQuest(): CJournalQuest;
    function GetHighlightedQuest(): CJournalQuest;
    function GetHighlightedObjective(): CJournalQuestObjective;
    function SetHighlightedObjective(journalEntry: CJournalBase): bool;
    function SetPrevNextHighlightedObjective(optional next: bool): bool;
    function GetCreaturesWithHuntingQuestClue(categoryName: name, clueIndex: int, out creatures: array<CJournalCreature>);
    function GetNumberOfCluesFoundForQuest(huntingQuest: CJournalQuest): int;
    function GetAllCluesFoundForQuest(huntingQuest: CJournalQuest, out creatures: array<CJournalCreatureHuntingClue>);
    function SetHuntingClueFoundForQuest(huntingQuest: CJournalQuest, huntingClue: CJournalCreatureHuntingClue);
    function GetTrackedQuestObjectivesData(out objectives: array<SJournalQuestObjectiveData>);
    function GetQuestHasMonsterKnown(journalEntry: CJournalBase): bool;
    function SetQuestHasMonsterKnown(journalEntry: CJournalBase, isKnown: bool);
    function GetEntryHasAdvancedInfo(journalEntry: CJournalBase): bool;
    function SetEntryHasAdvancedInfo(journalEntry: CJournalBase, isKnown: bool);
    function GetQuestObjectiveCount(questGuid: CGUID): int;
    function SetQuestObjectiveCount(questGuid: CGUID, newCount: int);
    function GetCreatureParams(entityFilename: string, out params: SJournalCreatureParams): bool;
    function ToggleDebugInfo(debugInfo: int);
    function ShowLoadingScreenVideo(debugVideo: bool);
    function GetQuestRewards(journalQuest: CJournalQuest): array<name>;
    function GetRegularQuestCount(): int;
    function GetMonsterHuntQuestCount(): int;
    function GetTreasureHuntQuestCount(): int;
    function GetQuestProgress(): int;
    function GetJournalAreasWithQuests(): array<int>;
    function ForceSettingLoadingScreenVideoForWorld(area: int);
    function ForceSettingLoadingScreenContextNameForWorld(contextName: name);
    function ForceUntrackingQuestForEP1Savegame();
}
class CR4GwintManager extends IGameSystem
{
    function GetCardDefs(): array<SCardDefinition>;
    function GetLeaderDefs(): array<SCardDefinition>;
    function GetFactionDeck(faction: eGwintFaction, out deck: SDeckDefinition): bool;
    function SetFactionDeck(faction: eGwintFaction, deck: SDeckDefinition);
    function GetPlayerCollection(): array<CollectionCard>;
    function GetPlayerLeaderCollection(): array<CollectionCard>;
    function GetSelectedPlayerDeck(): eGwintFaction;
    function SetSelectedPlayerDeck(index: eGwintFaction);
    function UnlockDeck(index: eGwintFaction);
    function IsDeckUnlocked(index: eGwintFaction): bool;
    function AddCardToCollection(cardIndex: int);
    function RemoveCardFromCollection(cardIndex: int);
    function HasCardInCollection(cardIndex: int): bool;
    function HasCardsOfFactionInCollection(faction: eGwintFaction, optional includeLeaders: bool): bool;
    function AddCardToDeck(faction: eGwintFaction, cardIndex: int);
    function RemoveCardFromDeck(faction: eGwintFaction, cardIndex: int);
    function GetHasDoneTutorial(): bool;
    function SetHasDoneTutorial(value: bool);
    function GetHasDoneDeckTutorial(): bool;
    function SetHasDoneDeckTutorial(value: bool);
}
class CR4LootManager extends IGameSystem
{
    function SetCurrentArea(areaName: string);
    function GetCurrentArea(): string;
}
class CR4TutorialSystem extends IGameSystem
{
    var needsTickEvent: bool;
}
class CStorySceneSystem extends IGameSystem
{
    var activeScenes: array<CStoryScenePlayer>;
    //var actorMap: ?;

    function IsSkippingLineAllowed(): bool;
    function PlayScene(scene: CStoryScene, input: string);
    function SendSignal(signal: EStorySceneSignalType, value: int);
}
class CVolumePathManager extends IGameSystem
{
    function GetPath(start: Vector, end: Vector, out resultPath: array<Vector>, optional maxHeight: float): bool;
    function GetPointAlongPath(start: Vector, end: Vector, distAlongPath: float, optional maxHeight: float): Vector;
    function IsPathfindingNeeded(start: Vector, end: Vector): bool;
}
abstract class IGameplayDLCMounter
{
}
class CR4FinishersDLCMounter extends IGameplayDLCMounter
{
    var customCameraAnimSet: CSkeletalAnimationSet;
    //var finishers: ?;
}
class CR4QuestDLCMounter extends IGameplayDLCMounter
{
    //var `quest`: ?;
    var taintFact: name;
    var sceneVoiceTagsTableFilePath: string;
    var questLevelsFilePath: string;
}
class IGameplayEffectExecutor
{
}
abstract class IMoveSteeringCondition
{
}
class CMoveSCScriptedCondition extends IMoveSteeringCondition
{
}
abstract class IPerformableAction
{
    function Trigger(parnt: CEntity);
    function TriggerArgNode(parnt: CEntity, node: CNode);
    function TriggerArgFloat(parnt: CEntity, value: float);
}
class CScriptedAction extends IPerformableAction
{
}
abstract class IQuestCondition
{
    //var `name`: ?;
    var active: bool;
}
class CQuestScriptedCondition extends IQuestCondition
{
}
abstract class IReactionAction
{
}
class CReactionScript extends IReactionAction
{
}
abstract class IReactionCondition
{
}
class CReactionScriptedCondition extends IReactionCondition
{
}
abstract class ISpawnTreeBaseNode
{
    var nodeName: name;
    var id: Uint64;
}
abstract class ISpawnTreeBranch extends ISpawnTreeBaseNode
{
}
abstract class ISpawnTreeDecorator extends ISpawnTreeBranch
{
}
class ISpawnTreeScriptedDecorator extends ISpawnTreeDecorator
{
    //var childNode: ?;
}
abstract class ISpawnTreeLeafNode extends ISpawnTreeBaseNode
{
}
abstract class CBaseCreatureEntry extends ISpawnTreeLeafNode
{
    var initializers: array<ISpawnTreeInitializer>;
    var quantityMin: int;
    var quantityMax: int;
    var spawnInterval: float;
    var waveDelay: float;
    var waveCounterHitAtDeathRatio: float;
    var randomizeRotation: bool;
    var group: int;
    var baseSpawner: CSpawnTreeWaypointSpawner;
    var recalculateDelay: GameTime;
}
class CCreatureEntry extends CBaseCreatureEntry
{
    var creatureDefinition: name;
}
class CCreaturePartyEntry extends CBaseCreatureEntry
{
    //var subDefinitions: ?;
    var partySpawnOrganizer: CPartySpawnOrganizer;
    var blockChats: bool;
    var synchronizeWork: bool;

    function AddPartyMember(inEditor: bool): CSpawnTreeEntrySubDefinition;
}
class CSpawnTreeBaseEntryGenerator extends ISpawnTreeLeafNode
{
    var childNodes: array<ISpawnTreeBaseNode>;

    function RemoveChildren();
    function AddNodeToTree(newNode: ISpawnTreeBaseNode, parentNode: ISpawnTreeBaseNode);
    function AddInitializerToNode(newNode: ISpawnTreeInitializer, parentNode: ISpawnTreeBaseNode);
    function SetName(out pair: SEncounterActionPointSelectorPair, catName: name);
}
class CWanderAndWorkEntryGenerator extends CSpawnTreeBaseEntryGenerator
{
    var entries: array<SWanderAndWorkEntryGeneratorParams>;
}
class CWorkEntryGenerator extends CSpawnTreeBaseEntryGenerator
{
    var entries: array<SWorkEntryGeneratorParam>;
}
abstract class ISpawnTreeInitializer
{
    var id: Uint64;
    var overrideDeepInitializers: bool;
}
class CSpawnTreeInitializerAddTag extends ISpawnTreeInitializer
{
    var tag: TagList;
    var onlySetOnSpawnAppearance: bool;

    function AddTag(tag: name);
}
class CSpawnTreeInitializerSetAppearance extends ISpawnTreeInitializer
{
    var appearanceName: name;
    var onlySetOnSpawnAppearance: bool;
}
abstract class ISpawnTreeInitializerAI extends ISpawnTreeInitializer
{
    var dynamicTreeParameterName: name;
}
abstract class CSpawnTreeInitializerBaseStartingBehavior extends ISpawnTreeInitializerAI
{
    var runBehaviorOnSpawn: bool;
    var runBehaviorOnActivation: bool;
    var runBehaviorOnLoading: bool;
    var actionPriority: ETopLevelAIPriorities;
}
class CSpawnTreeInitializerRiderStartingBehavior extends CSpawnTreeInitializerBaseStartingBehavior
{
}
class CSpawnTreeInitializerStartingBehavior extends CSpawnTreeInitializerBaseStartingBehavior
{
}
class CSpawnTreeInitializerIdleAI extends ISpawnTreeInitializerAI
{
}
class ISpawnTreeInitializerIdleSmartAI extends CSpawnTreeInitializerIdleAI
{
    var subInitializer: ISpawnTreeInitializer;
}
class CSpawnTreeInitializerIdleFlightAI extends ISpawnTreeInitializerAI
{
}
class CSpawnTreeInitializerRiderIdleAI extends ISpawnTreeInitializerAI
{
}
class ISpawnTreeInitializerCommunityAI extends ISpawnTreeInitializerAI
{
}
abstract class ISpawnTreeInitializerGuardAreaBase extends ISpawnTreeInitializer
{
    var pursuitRange: float;
}
class CSpawnTreeInitializerGuardArea extends ISpawnTreeInitializerGuardAreaBase
{
    var guardAreaTag: name;
    var pursuitAreaTag: name;
    var findAreasInEncounter: bool;
}
class CSpawnTreeInitializerGuardAreaByHandle extends ISpawnTreeInitializerGuardAreaBase
{
    var guardArea: EntityHandle;
    var pursuitArea: EntityHandle;
}
class ISpawnTreeInitializerToggleBehavior extends ISpawnTreeInitializer
{
    var behaviorSwitchName: name;
    var enableBehavior: bool;
}
class ISpawnTreeScriptedInitializer extends ISpawnTreeInitializer
{
}
abstract class ISpawnTreeSpawnMonitorBaseInitializer extends ISpawnTreeInitializer
{
}
class ISpawnTreeSpawnMonitorInitializer extends ISpawnTreeSpawnMonitorBaseInitializer
{
    function GetNumCreaturesSpawned(): int;
    function GetNumCreaturesToSpawn(): int;
    function GetNumCreaturesDead(): int;
}
abstract class ISpawnTreeSpawnerInitializer extends ISpawnTreeInitializer
{
}
class CSpawnTreeInitializerActionpointSpawner extends ISpawnTreeSpawnerInitializer
{
    var spawner: CSpawnTreeActionPointSpawner;
}
class CSpawnTreeInitializerWaypointSpawner extends ISpawnTreeSpawnerInitializer
{
    var spawner: CSpawnTreeWaypointSpawner;
}
class IStorySceneChoiceLineAction
{
}
class CStorySceneChoiceLineActionScripted extends IStorySceneChoiceLineAction
{
}
class CStorySceneChoiceLineActionScriptedContentGuard extends CStorySceneChoiceLineActionScripted
{
    var playGoChunk: name;
}
class W3GameParams
{
}
abstract class CPartySpawnOrganizer extends IScriptable
{
}
class CInstantMountPartySpawnOrganizer extends CPartySpawnOrganizer
{
}
class CR4PlayerTargeting extends IScriptable
{
    function SetConsts(out consts: SR4PlayerTargetingConsts);
    function BeginFindTarget(out inValues: SR4PlayerTargetingIn);
    function EndFindTarget(out outValues: SR4PlayerTargetingOut);
    function FindTarget();
    function WasVisibleInScaledFrame(entity: CEntity, frameSizeX: float, frameSizeY: float): bool;
}
class CScriptableState extends IScriptable
{
    function IsActive(): bool;
    function GetStateName(): name;
    function CanEnterState(prevStateName: name): bool;
    function CanLeaveState(nextStateName: name): bool;
    function BeginState(prevStateName: name);
    function EndState(nextStateName: name);
    function ContinuedState();
    function PausedState();
}
class CScriptedExplorationTraverser extends IScriptable
{
    function Update(deltaTime: float);
    function GetExplorationType(out expType: EExplorationType): bool;
}
class IAIParameters extends IScriptable
{
    function LoadSteeringGraph(fileName: string): CMoveSteeringBehavior;
    function OnManualRuntimeCreation();
}
class CAIDefaults extends IAIParameters
{
}
class CAIParameters extends IAIParameters
{
}
class IAIActionParameters extends CAIParameters
{
}
class CAIDespawnParameters extends IAIActionParameters
{
    var despawnAction: IAIActionTree;
}
class IRiderActionParameters extends CAIParameters
{
}
class CAIRedefinitionParameters extends IAIParameters
{
}
class ICustomValAIParameters extends CAIRedefinitionParameters
{
}
abstract class IAISpawnTreeParameters extends IAIParameters
{
}
class CEncounterParameters extends IAISpawnTreeParameters
{
    var encounter: CEncounter;
    var globalDefaults: array<IAISpawnTreeSubParameters>;
}
abstract class IAISpawnTreeSubParameters extends IAISpawnTreeParameters
{
}
class IAITree extends IAIParameters
{
    var aiTreeName: string;
    var tree: CBehTree;

    function OnCreated();
}
class CAIBaseTree extends IAITree
{
}
class CAIQuestScriptedActionsTree extends IAITree
{
    var actionTree: IAITree;
    var listener: SBehTreeExternalListenerPtr;
}
class CAITree extends IAITree
{
}
class IAIActionTree extends CAITree
{
}
class CAIActionSequence extends IAIActionTree
{
    var actions: array<IAIActionTree>;
}
class CAIDespawnTree extends IAIActionTree
{
    var params: CAIDespawnParameters;
}
class IRiderActionTree extends CAITree
{
}
class IAIExplorationTree extends IAITree
{
    var interactionPoint: Vector3;
    var destinationPoint: Vector3;
    var metalinkComponent: CComponent;
}
class IActorLatentAction extends IAIParameters
{
}
class IPresetActorLatentAction extends IActorLatentAction
{
    var res: CBehTree;
    //var `def`: ?;
    var resName: string;
}
class IBehTreeObjectDefinition extends IScriptable
{
    var instanceClass: name;

    function GetValFloat(v: CBehTreeValFloat): float;
    function GetValInt(v: CBehTreeValInt): int;
    function GetValEnum(v: IBehTreeValueEnum): int;
    function GetValString(v: CBehTreeValString): string;
    function GetValCName(v: CBehTreeValCName): name;
    function GetValBool(v: CBehTreeValBool): bool;
    function GetObjectByVar(varName: name): IScriptable;
    function GetAIParametersByClassName(className: name): IAIParameters;
    function SetValFloat(v: CBehTreeValFloat, n: float);
    function SetValInt(v: CBehTreeValInt, n: int);
    function SetValString(v: CBehTreeValString, n: string);
    function SetValCName(v: CBehTreeValCName, n: name);
    function SetValBool(v: CBehTreeValBool, n: bool);
}
class IBehTreeOnSpawnEffector extends IBehTreeObjectDefinition
{
    function GetActor(): CActor;
    function GetObjectFromAIStorage(varName: name): IScriptable;
}
class IBehTreeTaskDefinition extends IBehTreeObjectDefinition
{
    var listenToGameplayEvents: array<name>;
    var listenToAnimEvents: array<name>;

    function ListenToAnimEvent(eventName: name);
    function ListenToGameplayEvent(eventName: name);
}
class IBehTreeConditionalTaskDefinition extends IBehTreeTaskDefinition
{
}
class ITicketAlgorithmScriptDefinition extends IBehTreeObjectDefinition
{
}
class IBehTreeTask extends IScriptable
{
    var isActive: bool;

    function GetActor(): CActor;
    function GetNPC(): CNewNPC;
    function GetLocalTime(): float;
    function GetActionTarget(): CNode;
    function SetActionTarget(node: CNode);
    function GetCombatTarget(): CActor;
    function SetCombatTarget(target: CActor);
    function SetCustomTarget(target: Vector, heading: float): bool;
    function GetCustomTarget(out target: Vector, out heading: float): bool;
    function SetNamedTarget(targetName: name, node: CNode);
    function GetNamedTarget(targetName: name): CNode;
    function RunMain();
    function Complete(success: bool);
    function GetEventParamCName(defaultVal: name): name;
    function GetEventParamFloat(defaultVal: float): float;
    function GetEventParamInt(defaultVal: int): int;
    function GetEventParamObject(): IScriptable;
    function UnregisterFromAnimEvent(eventId: name);
    function UnregisterFromGameplayEvent(eventId: name);
    function SetIsInCombat(inCombat: bool);
    function SetEventRetvalCName(val: name): bool;
    function SetEventRetvalFloat(val: float): bool;
    function SetEventRetvalInt(val: int): bool;
    function GetEventParamBaseDamage(): CBaseDamage;
    function GetReactionEventInvoker(): CEntity;
    function RequestStorageItem(itemName: name, itemClass: name): IScriptable;
    function FindStorageItem(itemName: name, itemClass: name): IScriptable;
}
class IBehTreeValueEnum extends IScriptable
{
    var varName: name;
}
class IScriptedFlash extends IScriptable
{
}
class CScriptedFlashArray extends IScriptedFlash
{
    function ClearElements();
    function GetLength(): int;
    function SetLength(length: int);
    function GetElementFlashBool(index: int): bool;
    function GetElementFlashInt(index: int): int;
    function GetElementFlashUInt(index: int): int;
    function GetElementFlashNumber(index: int): float;
    function GetElementFlashString(index: int): string;
    function GetElementFlashObject(index: int): CScriptedFlashObject;
    function PopBack();
    function SetElementFlashObject(index: int, value: CScriptedFlashObject);
    function SetElementFlashString(index: int, value: string);
    function SetElementFlashBool(index: int, value: bool);
    function SetElementFlashInt(index: int, value: int);
    function SetElementFlashUInt(index: int, value: int);
    function SetElementFlashNumber(index: int, value: float);
    function PushBackFlashObject(value: CScriptedFlashObject);
    function PushBackFlashString(value: string);
    function PushBackFlashBool(value: bool);
    function PushBackFlashInt(value: int);
    function PushBackFlashUInt(value: int);
    function PushBackFlashNumber(value: float);
    function RemoveElement(index: int);
    function RemoveElements(index: int, optional count: int);
}
class CScriptedFlashFunction extends IScriptedFlash
{
    function InvokeSelf();
    function InvokeSelfOneArg(arg0: SFlashArg);
    function InvokeSelfTwoArgs(arg0: SFlashArg, arg1: SFlashArg);
    function InvokeSelfThreeArgs(arg0: SFlashArg, arg1: SFlashArg, arg2: SFlashArg);
    function InvokeSelfFourArgs(arg0: SFlashArg, arg1: SFlashArg, arg2: SFlashArg, arg3: SFlashArg);
    function InvokeSelfFiveArgs(arg0: SFlashArg, arg1: SFlashArg, arg2: SFlashArg, arg3: SFlashArg, arg4: SFlashArg);
    function InvokeSelfSixArgs(arg0: SFlashArg, arg1: SFlashArg, arg2: SFlashArg, arg3: SFlashArg, arg4: SFlashArg, arg5: SFlashArg);
    function InvokeSelfSevenArgs(arg0: SFlashArg, arg1: SFlashArg, arg2: SFlashArg, arg3: SFlashArg, arg4: SFlashArg, arg5: SFlashArg, arg6: SFlashArg);
    function InvokeSelfEightArgs(arg0: SFlashArg, arg1: SFlashArg, arg2: SFlashArg, arg3: SFlashArg, arg4: SFlashArg, arg5: SFlashArg, arg6: SFlashArg, arg7: SFlashArg);
    function InvokeSelfNineArgs(arg0: SFlashArg, arg1: SFlashArg, arg2: SFlashArg, arg3: SFlashArg, arg4: SFlashArg, arg5: SFlashArg, arg6: SFlashArg, arg7: SFlashArg, arg8: SFlashArg);
}
class CScriptedFlashObject extends IScriptedFlash
{
    function CreateFlashObject(optional flashClassName: string): CScriptedFlashObject;
    function CreateFlashArray(): CScriptedFlashArray;
    function GetMemberFlashObject(memberName: string): CScriptedFlashObject;
    function GetMemberFlashArray(memberName: string): CScriptedFlashArray;
    function GetMemberFlashFunction(memberName: string): CScriptedFlashFunction;
    function GetMemberFlashString(memberName: string): string;
    function GetMemberFlashBool(memberName: string): bool;
    function GetMemberFlashInt(memberName: string): int;
    function GetMemberFlashUInt(memberName: string): int;
    function GetMemberFlashNumber(memberName: string): float;
    function SetMemberFlashObject(memberName: string, value: CScriptedFlashObject);
    function SetMemberFlashArray(memberName: string, value: CScriptedFlashArray);
    function SetMemberFlashFunction(memberName: string, value: CScriptedFlashFunction);
    function SetMemberFlashString(memberName: string, value: string);
    function SetMemberFlashBool(memberName: string, value: bool);
    function SetMemberFlashInt(memberName: string, value: int);
    function SetMemberFlashUInt(memberName: string, value: int);
    function SetMemberFlashNumber(memberName: string, value: float);
}
class CScriptedFlashSprite extends CScriptedFlashObject
{
    function GetChildFlashSprite(memberName: string): CScriptedFlashSprite;
    function GetChildFlashTextField(memberName: string): CScriptedFlashTextField;
    function GotoAndPlayFrameNumber(frame: int);
    function GotoAndPlayFrameLabel(frame: string);
    function GotoAndStopFrameNumber(frame: int);
    function GotoAndStopFrameLabel(frame: string);
    function GetAlpha(): float;
    function GetRotation(): float;
    function GetVisible(): bool;
    function GetX(): float;
    function GetY(): float;
    function GetZ(): float;
    function GetXRotation(): float;
    function GetYRotation(): float;
    function GetXScale(): float;
    function GetYScale(): float;
    function GetZScale(): float;
    function SetAlpha(alpha: float);
    function SetRotation(degrees: float);
    function SetVisible(visible: bool);
    function SetPosition(x: float, y: float);
    function SetScale(xscale: float);
    function SetX(x: float);
    function SetY(y: float);
    function SetZ(z: float);
    function SetXRotation(degrees: float);
    function SetYRotation(degrees: float);
    function SetXScale(xscale: float);
    function SetYScale(yscale: float);
    function SetZScale(zscale: float);
}
class CScriptedFlashTextField extends IScriptedFlash
{
    function GetText(): string;
    function GetTextHtml(): string;
    function SetText(text: string);
    function SetTextHtml(htmlText: string);
}
class CScriptedFlashValueStorage extends IScriptedFlash
{
    function SetFlashObject(key: string, value: CScriptedFlashObject, optional index: int);
    function SetFlashArray(key: string, value: CScriptedFlashArray);
    function SetFlashString(key: string, value: string, optional index: int);
    function SetFlashBool(key: string, value: bool, optional index: int);
    function SetFlashInt(key: string, value: int, optional index: int);
    function SetFlashUInt(key: string, value: int, optional index: int);
    function SetFlashNumber(key: string, value: float, optional index: int);
    function CreateTempFlashObject(optional flashClassName: string): CScriptedFlashObject;
    function CreateTempFlashArray(): CScriptedFlashArray;
}
abstract class ISpawnCondition extends IScriptable
{
}
class ISpawnScriptCondition extends ISpawnCondition
{
}
class ITicketAlgorithmScript extends IScriptable
{
    var overrideTicketsCount: int;

    function GetActor(): CActor;
    function GetNPC(): CNewNPC;
    function GetLocalTime(): float;
    function GetActionTarget(): CNode;
    function GetCombatTarget(): CActor;
    function GetTimeSinceMyAcquisition(): float;
    function GetTimeSinceAnyAcquisition(): float;
    function IsActive(): bool;
    function HasTicket(): bool;
}
class W3AbilityManager extends IScriptable
{
    var statPoints: array<SBaseStat>;
    var resistStats: array<SResistanceValue>;
    var blockedAbilities: array<SBlockedAbility>;
    var owner: CActor;
    var charStats: CCharacterStats;
    var usedDifficultyMode: EDifficultyMode;
    var usedHealthType: EBaseCharacterStats;
    var difficultyAbilities: array<array<name>>;
    var ignoresDifficultySettings: bool;

    function CacheStaticScriptData();
    function SetInitialStats(diff: EDifficultyMode): bool;
    function FixInitialStats(diff: EDifficultyMode): bool;
    function HasStat(stat: EBaseCharacterStats): bool;
    function StatAddNew(stat: EBaseCharacterStats, optional max: float);
    function RestoreStat(stat: EBaseCharacterStats);
    function RestoreStats();
    function GetStat(stat: EBaseCharacterStats, optional skipLock: bool): float;
    function GetStatMax(stat: EBaseCharacterStats): float;
    function GetStatPercents(stat: EBaseCharacterStats): float;
    function GetStats(stat: EBaseCharacterStats, out current: float, out max: float): bool;
    function SetStatPointCurrent(stat: EBaseCharacterStats, val: float);
    function SetStatPointMax(stat: EBaseCharacterStats, val: float);
    function UpdateStatMax(stat: EBaseCharacterStats);
    function HasResistStat(stat: ECharacterDefenseStats): bool;
    function GetResistStat(stat: ECharacterDefenseStats, out resistStat: SResistanceValue): bool;
    function SetResistStat(stat: ECharacterDefenseStats, out resistStat: SResistanceValue);
    function ResistStatAddNew(stat: ECharacterDefenseStats);
    function RecalcResistStat(stat: ECharacterDefenseStats);
    function GetAttributeValueInternal(attributeName: name, optional tags: array<name>): SAbilityAttributeValue;
    function CacheDifficultyAbilities();
    function UpdateStatsForDifficultyLevel(diff: EDifficultyMode);
    function UpdateDifficultyAbilities(diff: EDifficultyMode);
    function GetAllStats_Debug(out stats: array<SBaseStat>): bool;
    function GetAllResistStats_Debug(out stats: array<SResistanceValue>): bool;
    function SetCharacterStats(cStats: CCharacterStats);
    function IsAbilityBlocked(abilityName: name): bool;
}
